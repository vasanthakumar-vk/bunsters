function testfunciton() {
  var JsonList;
  if (1) {
    JsonList = getOutOfStock("shippingTree");
  } else if (0) {
    JsonList = getOutOfStock("Pikpak");
  } else if (0) {
    JsonList = getBasicDetails();
  } else if (0) {
    JsonList = getOutOfStockAmazon();
    v = 0;
  }
}

function getInventorySheet(typeOfSite) {
  var inventorySheet;
  if (typeOfSite == "Pikpak") {
    inventorySheet = SpreadsheetApp.getActive().getSheetByName("Inventory");
  } else {
    inventorySheet = SpreadsheetApp.openByUrl(SHIPPINGTREE_SHEET_URL).getSheetByName("Inventory");
  }
  return inventorySheet;
}

function getSOSheet(typeOfSite) {
  var soSheet;
  if (typeOfSite == "Pikpak") {
    soSheet = SpreadsheetApp.getActive().getSheetByName("Created SO");
  } else {
    soSheet = SpreadsheetApp.openByUrl(SHIPPINGTREE_SHEET_URL).getSheetByName("Created SO");
  }
  return soSheet;
}

function getLogSheet(typeOfSite) {
  var logSheet;
  if (typeOfSite == "Pikpak") {
    logSheet = SpreadsheetApp.openByUrl(LOGSHEET_URL).getSheetByName("Pikpak");
  } else {
    logSheet = SpreadsheetApp.openByUrl(LOGSHEET_URL).getSheetByName("ShippingTree");
  }
  return logSheet;
}

//Reorder function
function getOutOfStock(typeOfSite) {
  var inventorySheet = getInventorySheet(typeOfSite);
  var soSheet = getSOSheet(typeOfSite);
  var skus = inventorySheet.getRange(2, 1, inventorySheet.getLastRow() - 1).getValues();
  var qtys = inventorySheet.getRange(2, inventorySheet.getLastColumn(), inventorySheet.getLastRow() - 1).getValues();

  // Get Event based SKU and Qty
  var minStock = eventFunction(typeOfSite, null);
  //Get outof Stock
  var outOfStock = [];
  for (var i = 0; i < skus.length; i++) {
    for (var j = 0; j < minStock.length; j++) {
      var stock = minStock[j]
      if (skus[i] == stock[0] && qtys[i][0] < stock[1]) {// stock[0] == "BU-HS-16-AU"
        outOfStock.push([stock[0], qtys[i][0], stock[1], stock[2], stock[3], stock[4], stock[5]]);
      }
    }
  }
  //Get Already Fullfilled
  var discardSKUs;
  if (soSheet.getLastRow() >= 2) {
    var pastOutOfStock = soSheet.getRange(2, 1, soSheet.getLastRow() - 1, 5).getValues().filter(function (row) {
      var now = new Date();
      var lessThan7Days = ((now.getTime() - row[1].getTime()) < (7 * 24 * 60 * 60 * 1000));
      var notFilfilled = !row[4];
      return lessThan7Days && notFilfilled;
    });
    discardSKUs = pastOutOfStock.map(function (row) {
      return row[0];
    });
  } else {
    discardSKUs = [];
  }
  // Filter New SKU
  var validOutOfStocks = outOfStock.filter(function (row) {
    return discardSKUs.indexOf(row[0]) == -1;
  });

  var rows = validOutOfStocks.map(function (row) {
    return [row[0], Utilities.formatDate(new Date(), "GMT+10", "yyyy-MM-dd"), row[1], row[3], false, row[4], row[5], row[6], row[2]];
  });

  var finalList = CheckFulFilment(rows, typeOfSite);
  finalList = checkBundle(finalList, typeOfSite);
  finalList = CheckvalidReorderNum(finalList);

  if (typeOfSite == "shippingTree") {
    storeShippingSO(finalList);
  }
  var JsonList = [];
  for (var i = 0; i < finalList.length; i++) {
    var arrayLen = finalList[i].length;
    var componentsList = '';
    if (arrayLen > 8) {
      for (var c = arrayLen - 1; c > 8; c--) {
        if (c == 9) {
          componentsList += finalList[i][c];
        } else {
          componentsList += finalList[i][c] + "---";
        }
      }
    }
    JsonList.push({ "sku": finalList[i][0], "date": finalList[i][1], "currentQty": finalList[i][2], "reorderQty": finalList[i][3], "fulFillment": finalList[i][4], "rate": finalList[i][5], "instruction": finalList[i][6], "component": componentsList, "QtyInCarton": finalList[i][7] })
  }

  if (typeOfSite == "shippingTree") {
    createProjectEmail(JsonList);
  }
  return JsonList;
}

function CheckvalidReorderNum(stock) {
  if (stock.length == 0)
    return stock;
  var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL);
  var dateSheet = sheet.getSheetByName("Date bound Inventory");
  var finalData = [];
  var notValidSku = '';
  stock.forEach(function (row) {
    if (Number(row[3]).toString() == 'NaN') {
      notValidSku += '\n*' + row[0] + '* \n>Reorder Qty is ' + row[2] + '\n'
    } else {
      finalData.push(row);
    }
  });

  if (notValidSku != "") {
    sendNotification("", 'Failed to order due to spreadsheet #NA Error', "", "", notValidSku, "slack", sheet.getUrl() + "#gid=" + dateSheet.getSheetId())
  }
  return finalData;
}


// Update FulFillment
function updateFulfilmentStatus(stock, typeOfSite) {
  var soSheet = getSOSheet(typeOfSite);

  if (soSheet.getLastRow() < 2) {
    return;
  }

  soSheet.getRange(2, 1, soSheet.getLastRow() - 1, 5).getValues().forEach(function (row, index) {
    var now = new Date();
    //var lessThan7Days = ((now.getTime() - row[1].getTime()) < (7 * 24 * 60 * 60 * 1000));
    var notFilfilled = !row[4];
    if (notFilfilled) {
      for (var i = 0; i < stock.length; i++) {
        if (row[0] == stock[i][0]
          && stock[i][1] > row[2]) {
          soSheet.getRange(index + 2, 5).setValue(true);
        }
      }
    }
  });
}

// EVENT FUNCTION
function eventFunction(typeOfSite, AMZcurrentDate) {
  var inventorySheet = getInventorySheet(typeOfSite);
  var warehouse = (typeOfSite == "Pikpak") ? 'au-1' : (typeOfSite == "shippingTree") ? 'us-1' : 'amz';
  var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL);
  var eventSheet = sheet.getSheetByName("Events");
  var dateSheet = sheet.getSheetByName("Date bound Inventory");

  var currentYear;
  var currentDate;
  if (typeOfSite == 'amazon') {
    currentYear = AMZcurrentDate.getYear();
  } else {
    currentDate = inventorySheet.getRange(1, inventorySheet.getLastColumn(), 1, 1).getValue();
    currentYear = currentDate.getYear();
  }

  var dateSheetHeader = dateSheet.getRange(1, 1, 1, dateSheet.getLastColumn()).getValues()[0];
  var dateSheetDatas = dateSheet.getRange(2, 1, dateSheet.getLastRow() - 1, dateSheet.getLastColumn()).getValues();

  var WarehouseIndex = dateSheetHeader.indexOf("Warehouse");
  var SKUIndex = dateSheetHeader.indexOf("SKU");
  var eventIndex = dateSheetHeader.indexOf("Event");
  var flagIndex = dateSheetHeader.indexOf("Auto Reorder Flag");
  var reorderAtIndex = dateSheetHeader.indexOf("Min Stock Levels");//Re-order when inventory falls below
  var reorderQtyIndex = dateSheetHeader.indexOf("Order Quantity");//Re-order Quantity
  var rateIndex = dateSheetHeader.indexOf("Rate");
  var instructionIndex = dateSheetHeader.indexOf("Link to Instructions");
  var QtyOfMasterIndex = dateSheetHeader.indexOf("MOQ (Minimum Order Quantity)");//Qty in Master Carton

  var dateSheetData = [];
  dateSheetDatas.forEach(function (row) {
    var reOrderAt = 0;
    if (Number(row[reorderAtIndex]).toString() != 'NaN') {
      reOrderAt = row[reorderAtIndex];
    }
    dateSheetData.push([row[WarehouseIndex], row[SKUIndex], row[eventIndex], row[flagIndex], reOrderAt, row[reorderQtyIndex], row[rateIndex], row[instructionIndex], row[QtyOfMasterIndex]])
  });

  var skus = dateSheetData.filter(function (row) {
    if (row[0].toLowerCase().indexOf(warehouse) > -1 && row[4] >= 0 && row[3]) {
      return row;
    }
  }).map(function (item) {
    return [item[1], item[2], item[4], item[5], item[6], item[7], item[8], item[0]]
  });

  var eventDateRange = eventSheet.getRange(2, 1, eventSheet.getLastRow() - 1, 3).getValues()
  var DefaultEventName = "";
  eventDateRange.forEach(function (row) {
    if (row[1] == "Default") {
      DefaultEventName = row[0];
    }
  });
  var val = [];
  var startingDate;
  var endingDate;
  var selectedArray = [];

  for (var i = 0; i < skus.length; i++) {
    var sku = skus[i];
    var event = sku[1];
    for (var j = 0; j < eventDateRange.length; j++) {
      var theDate;
      if (event == eventDateRange[j][0]) {
        theDate = [eventDateRange[j][1], eventDateRange[j][2]]
      } else {
        continue;
      }
      if (theDate[0] != "Default") {
        startingDate = new Date(currentYear, theDate[0].getMonth(), theDate[0].getDate());//.getTime();
        endingDate = new Date(currentYear, theDate[1].getMonth(), theDate[1].getDate());//.getTime(); 
        if (currentDate >= startingDate) {
          if (currentDate <= endingDate) {
            val.push([sku[0], sku[2], sku[3], sku[4], sku[5], sku[6], sku[7]]);
            selectedArray.push(sku[0]);
          }
        }
      }
    }
  }
  skus.filter(function (row) {
    if (row[1] == DefaultEventName) {
      return row;
    }
  }).forEach(function (row) {
    if (selectedArray.indexOf(row[0]) == -1) {
      val.push([row[0], row[2], row[3], row[4], row[5], row[6], row[7]])
    }
  });
  return val;
}

// Check FullFillMent 
function CheckFulFilment(stocks, typeOfSite) {
  var soSheet = getSOSheet(typeOfSite);
  if (soSheet.getLastRow() < 2) {
    return stocks;
  }

  var stock = stocks.map(function (row) { return row[0]; });
  var UnFulfillMentSku = [];

  var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");
  var inventorySheet = getInventorySheet(typeOfSite);
  var logSheet = getLogSheet(typeOfSite);

  var emailAddress = (typeOfSite == "Pikpak") ? sheet.getRange("B5").getValue() : sheet.getRange("B9").getValue();
  var subject = (typeOfSite == "Pikpak") ? sheet.getRange("B7").getValue() : sheet.getRange("B11").getValue();
  var ccMail = (typeOfSite == "Pikpak") ? sheet.getRange("B6").getValue() : sheet.getRange("B10").getValue();
  var replyToEmail = sheet.getRange("B21").getValue();

  var inventorySheetData = inventorySheet.getRange(2, 1, inventorySheet.getLastRow() - 1, inventorySheet.getLastColumn()).getValues().filter(function (row) {
    if (stock.indexOf(row[0]) > -1) {
      return row;
    }
  });
  var message = 'Requested';
  soSheet.getRange(2, 1, soSheet.getLastRow() - 1, 6).getValues().forEach(function (row, index) {
    var msg = message;
    var now = new Date();
    var moreThan7Days = ((now.getTime() - row[1].getTime()) > (7 * 24 * 60 * 60 * 1000));
    var notFilfilled = !row[4];
    if (moreThan7Days && notFilfilled) {
      for (var i = 0; i < inventorySheetData.length; i++) {
        if (row[0] == inventorySheetData[i][0] && inventorySheetData[i][inventorySheetData[i].length - 1] <= row[2]) {
          if (typeOfSite == "Pikpak") {
            msg += " an SO (" + row[5] + ")"
          } else if (typeOfSite == 'shippingTree') {
            msg += " a Project (" + row[3] + " x " + row[0] + ")"
          }
          msg += " on " + Utilities.formatDate(new Date(row[1]), "GMT+10", "dd-MM-yyyy") + ". This hasn’t been created yet."
          var details = "\n>SKU: " + row[0] + "\n>Qty Requested: " + row[3] + "\n>Date requested: " + Utilities.formatDate(new Date(row[1]), "GMT+10", "dd-MM-yyyy") + "\n>Current qty : " + inventorySheetData[i][inventorySheetData[i].length - 1]
          sendNotification(emailAddress, subject.replace("{SKU}", row[0]), ccMail, replyToEmail, msg + details, "slack", soSheet.getParent().getUrl() + "#gid=" + soSheet.getSheetId())
          logSheet.appendRow(["Request for SO update", msg + " on " + Utilities.formatDate(new Date(row[1]), "GMT+10", "dd-MM-yyyy") + details + "<br><br>Thank you, Bunsters Bot <br>"])
          UnFulfillMentSku.push(row[0]);
        }
      }
    }
  });

  var finalList = stocks.filter(function (row) {
    return UnFulfillMentSku.indexOf(row[0]) == -1;
  });
  return finalList;
}

function Comparator(a, b) {
  if (a[2] < b[2]) return -1;
  if (a[2] > b[2]) return 1;
  return 0;
}

// CHECK BUNDLE
function checkBundle(OutOfStockList, typeOfSite) {
  //  OutOfStockList = [["BU-BUNDLE-19-US", "2020-10-07", 0, 70, false, "", "https://tinyurl.com/BU-BUNDLE-19-US-MC", 5, 45]]
  //  typeOfSite = "shippingTree"
  if (OutOfStockList.length == 0)
    return OutOfStockList;

  //Get Expiration Date
  var expirationSheet;
  if (typeOfSite == 'Pikpak') {
    expirationSheet = SpreadsheetApp.openByUrl(PICPAK_EXPIREDAETSHEET_URL).getSheetByName("Data");
  } else {
    expirationSheet = SpreadsheetApp.openByUrl(EXPIREDAETSHEET_URL).getSheetByName("Data");
  }
  var expirationDetails = {};
  var expirationSkuArray = [];
  expirationSheet.getRange(2, 1, expirationSheet.getLastRow() - 1, expirationSheet.getLastColumn()).getValues().forEach(function (row) {
    if (row[3] != "") {
      if (expirationSkuArray.indexOf(row[1]) == -1) {
        expirationSkuArray.push(row[1]);
        expirationDetails[row[1]] = [];
      }
      expirationDetails[row[1]].push([row[3]]);
    }
  });
  for (var key in expirationDetails) {
    var val = expirationDetails[key];
    var min = val.reduce(function (a, b) { return a < b ? a : b; });
    var expire = new Date(min).getTime();
    var now = new Date().getTime();
    var diff = Math.floor((expire - now) / (24 * 3600 * 1000));
    expirationDetails[key] = diff > 100;
  }

  // Get bundle checkbox
  var checkBoxSheet = SpreadsheetApp.openByUrl(SHIPPINGTREE_SHEET_URL).getSheetByName("SKU Bundle Check Flag");
  var checkBoxData = [];
  if (checkBoxSheet.getLastRow() > 1) {
    checkBoxData = checkBoxSheet.getRange(2, 1, checkBoxSheet.getLastRow() - 1, 2).getValues();
  }
  var SKUjson = {};
  checkBoxData.forEach(function (row) {
    SKUjson[row[0]] = row[1];
  });

  //Current Stock of all SKU
  var inventorySheet = getInventorySheet(typeOfSite);
  var inventoryData = {};
  inventorySheet.getRange(1, 1, inventorySheet.getLastRow(), inventorySheet.getLastColumn()).getValues().forEach(function (row) {
    var currentQty = row[row.length - 1];
    var sku = row[0];
    inventoryData[sku] = currentQty;
  });

  //Get Type of material
  var SKUType = {};
  var sheet = SpreadsheetApp.openByUrl(SKU_DETAILS_URL).getSheetByName("SKU Master list");
  var sheetHeader = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
  var typeIndex = sheetHeader.indexOf("Type");
  var data = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues();
  data.forEach(function (row) {
    SKUType[row[0]] = row[typeIndex]
  });

  // Values
  var logSheet = getLogSheet(typeOfSite);
  var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL);
  var emailSheet = sheet.getSheetByName("Email Config");
  var emailAddress = emailSheet.getRange("B1").getValue();
  var replyToEmail = emailSheet.getRange("B21").getValue();

  var BundleBreakdownSheet = sheet.getSheetByName("Bundle breakdown");
  var alternativeSKUSheet = sheet.getSheetByName("Bundle Breakdown - Backup");
  var BundleBreakdownHeader = BundleBreakdownSheet.getRange(1, 1, 1, BundleBreakdownSheet.getLastColumn()).getValues()[0];
  var BundleBreakdownDatas = BundleBreakdownSheet.getRange(2, 1, BundleBreakdownSheet.getLastRow() - 1, BundleBreakdownSheet.getLastColumn()).getValues();
  var alternativeSKUData = alternativeSKUSheet.getRange(2, 1, alternativeSKUSheet.getLastRow() - 1, alternativeSKUSheet.getLastColumn()).getValues();
  var WarehouseIndex = BundleBreakdownHeader.indexOf("Home Warehouse");
  var BundleIndex = BundleBreakdownHeader.indexOf("Bundle / Master Carton SKU");
  var ComponentIndex = BundleBreakdownHeader.indexOf("Component SKU");
  var QtyIndex = BundleBreakdownHeader.indexOf("Component Qty");
  var StockIndex = BundleBreakdownHeader.indexOf("Bunsters Hold Stock?");

  // Bundle Breakdown Data alternative SKUs
  var alternativeSKUs = {};
  var alternativeSKUsArray = [];
  alternativeSKUData.forEach(function (row) {
    if (alternativeSKUsArray.indexOf(row[0]) == -1) {
      alternativeSKUsArray.push(row[0]);
      alternativeSKUs[row[0]] = [];
    }
    alternativeSKUs[row[0]].push([row[0], row[1], row[2]]);
  });
  for (var key in alternativeSKUs) {
    var vals = alternativeSKUs[key];
    alternativeSKUs[key] = vals.sort(Comparator);
  }

  // Bundle Breakdown Data
  var BundleBreakdownData = {};
  var BundleBreakdownDataArray = [];
  BundleBreakdownDatas.forEach(function (row) {
    if (BundleBreakdownDataArray.indexOf(row[BundleIndex]) == -1) {
      BundleBreakdownDataArray.push(row[BundleIndex]);
      BundleBreakdownData[row[BundleIndex]] = [];
    }
    BundleBreakdownData[row[BundleIndex]].push([row[WarehouseIndex], row[BundleIndex], row[ComponentIndex], row[QtyIndex], row[StockIndex]]);
  });

  var array = [];
  var array2 = [];
  var array3 = [];
  var message = "";
  var type = (typeOfSite == "Pikpak") ? "an SO" : "a Project";
  var projectOrSO = (typeOfSite == "Pikpak") ? "SO" : "project";
  var finalOutOfStock = [];
  OutOfStockList.forEach(function (row) {
    var missingSku = false;
    var tempArray = [];
    var bundle = BundleBreakdownData[row[0]];
    bundle.forEach(function (items) {
      var sku = items[2];
      var qty = items[3] * row[3];
      if ((items[4] == 0 && typeOfSite != 'shippingTree') || ((items[4] == 0 || SKUjson[items[2]] == true) && typeOfSite == 'shippingTree')) {
        tempArray.push(qty + " X " + sku);
      }
      if ((items[4] != 0 && typeOfSite != 'shippingTree') || (items[4] != 0 && typeOfSite == 'shippingTree' && SKUjson[items[2]] == false)) {
        var checkavailable = false;
        if (inventoryData[sku] != undefined) {
          checkavailable = true;
          var currentQty = inventoryData[sku];
          if (qty <= currentQty) {
            array.push([sku, qty, currentQty]);
            tempArray.push(qty + " X " + sku);
          } else {
            var alternative_sku_available = false;
            var alternativeSKU = alternativeSKUs[sku];
            var SKUALL_alternatives = [];
            if (alternativeSKU) {
              for (var a = 0; a < alternativeSKU.length; a++) {
                var alternative_sku = alternativeSKU[a][1];
                if (inventoryData[alternative_sku] && qty <= inventoryData[alternative_sku]) {
                  alternative_sku_available = true;
                  checkavailable = true;
                  array.push([alternative_sku, qty, inventoryData[alternative_sku]]);
                  tempArray.push(qty + " X " + alternative_sku);
                  break;
                } else {
                  SKUALL_alternatives.push([alternative_sku, inventoryData[alternative_sku]]);
                  continue;
                }
              }
            }
            if (!alternative_sku_available) {
              array2.push([sku, qty, currentQty, SKUALL_alternatives]);
              array3.push([sku, qty, currentQty]);
            }
          }
        }
        if (!checkavailable) {
          sendNotification(emailAddress, 'Missing SKU', "", "", '( ' + sku + ' ) is Missing in the ( ' + items[1] + " ) Bundle", "slack");
          logSheet.appendRow(["MISSING EMAIL", 'I’m the bunsters bot. <br> <br> ( ' + sku + ' ) is Missing in the ( ' + items[1] + " ) Bundle"]);
          missingSku = true;
        }
      }
    });

    if (array2.length == 0 && !missingSku) {
      array = [];
      array2 = [];
      row = row.concat(tempArray);
      var compomentSKUFlag = false;
      tempArray.forEach(function (compomentSKU) {
        var split = compomentSKU.split(" X ");
        if (SKUType[split[1]].toLowerCase().indexOf("packaging") > -1 || SKUType[split[1]].toLowerCase().indexOf("promo") > -1) {
          return
        }
        var exDate = expirationDetails[split[1]];
        if (exDate == undefined) {
          var msg = "Create " + type + " failed \n" + row[3] + " x " + row[0] + " - Can't request a " + typeOfSite + " " + projectOrSO + " because a component (" + split[0] + " x " + split[1] + ") is missing an expiry date.\nDownload an inventory report here : https://app.shippingtree.co/product/";
          sendNotification(emailAddress, ' ACTION NEEDED - Expiration Date Missing', "", replyToEmail, msg, "slack");
          compomentSKUFlag = true;
        } else if (!exDate) {
          var msg = "Create  " + type + " failed \n" + row[3] + " x " + row[0] + " - Can't request a " + typeOfSite + " " + projectOrSO + " because a component (" + split[0] + " x " + split[1] + ") is expired.\nDownload an inventory report here : https://app.shippingtree.co/product/";
          sendNotification(emailAddress, ' ACTION NEEDED - Expiration Date out of date', "", replyToEmail, msg, "slack");
          compomentSKUFlag = true;
        }
      });
      if (!compomentSKUFlag) {
        finalOutOfStock.push(row);
      }
    } else if (array2.length > 0) {
      var html = createMessage("*_" + row[0] + "_* \n(Current Qty: " + row[2] + ", Reorder below: " + row[8] + ", Reorder Qty: " + row[3] + ")", array, array2);
      array = [];
      array2 = [];
      message += html;
      console.log(message);
    } else if (missingSku) {
      array = [];
      array2 = [];
    }
  });

  if (array3.length > 0) {
    console.log("sending Email");
    var subject = sheet.getSheetByName("Email Config").getRange("B2").getValue();
    sendNotification(emailAddress, subject, "", "", message, "slack");
    logSheet.appendRow([subject, message.toString()]);
  }
  return finalOutOfStock;
}

// CREATE MESSAGE HTML
function createMessage(sku, arr, arr2) {
  var message = "\n\n" + sku
  arr.forEach(function (val) {
    message += "\n>" + val[0] + "  X  " + val[1] + " (Current Qty - " + val[2] + ")";
  });
  arr2.forEach(function (val) {
    message += "\n>:mailbox_with_no_mail: " + val[0] + "  X  " + val[1] + " (Current Qty - " + val[2] + ")";
    if (val[3].length > 0) {
      message += "\n> (" + val[1] + " alternative SKU are also Insufficient";
      val[3].forEach(function (item) {
        message += "\n>>" + val[0] + "  X  " + item[0] + " (Current Qty - " + item[1] + ")";
      });
    }
  });
  return message;
}

function sendNotification(email, subject, ccMail, replyToEmail, message, type, extra) {
  var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");

  var CHANNEL_NAME = sheet.getRange("B23").getValue();
  var isTestEmail = sheet.getRange("B24").getValue();
  var issendSlack = sheet.getRange("B25").getValue();
  var issendEmail = sheet.getRange("B26").getValue();

  if (isTestEmail) {
    email = "sid@bunstersworldwide.com";
    ccMail = "sid@bunstersworldwide.com";
    replyToEmail = "";
    CHANNEL_NAME = 'C011DT06BK7';
    Authorization = "Bearer xoxp-647241452166-633657186034-776659978080-3e184af328ee7b787cb0740513fd8667";
  }

  if (type == "slack") {
    if (issendSlack) {
      return;
    }
    createSlackPostEmails(true, CHANNEL_NAME, subject, message, extra)
  } else {
    if (issendEmail) {
      return;
    }
    MailApp.sendEmail({
      to: email,
      subject: subject,
      htmlBody: message,
      cc: ccMail,
      replyTo: replyToEmail
    });
  }
}

function toSendGmail(email, subject, ccMail, replyToEmail, message) {
  var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");
  var isTestEmail = sheet.getRange("B24").getValue();
  var issendEmail = sheet.getRange("B26").getValue();

  if (issendEmail) {
    return;
  }
  if (isTestEmail) {
    email = "sid@bunstersworldwide.com";
    ccMail = "sid@bunstersworldwide.com";
    replyToEmail = "";
  }

  GmailApp.sendEmail(email, subject, null, {
    htmlBody: message,
    cc: ccMail,
    replyTo: replyToEmail
  });
}

function sendErrorMail(data) {
  var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");
  var emailAddress = sheet.getRange("B1").getValue();
  var shipmentDetails = data.allData;
  var message
  try {
    message = shipmentDetails.skuQty + " X " + shipmentDetails.sku + " (Current Qty - " + shipmentDetails.currentQty + ")";
    message += "\n" + data.error.replace("Reason:", "\n*Reason*:");
  } catch (e) {
    var err = data.errors ? data.errors.Message ? data.errors.Message : data.errors: data.errors;
    var err2 = data.error ? data.error: "";
    message = err2 + "\n" + err;
  }
  sendNotification(emailAddress, "Amazon Inbound Shipment Failed", "", "", message, "slack");
  //sendErrNorification(message);
}

var Authorization = "Bearer xoxb-268255044727-946978712406-nwyClIRO3VnDeoEKBh4zWKVf";

function getChannelIdFromName(CHANNEL_NAME) {
  var options = {
    'method': 'get',
    'contentType': 'application/json',
    'headers': {
      "Authorization": Authorization
    }
  };
  var response = UrlFetchApp.fetch('https://slack.com/api/conversations.list', options);
  response = JSON.parse(response);
  var CHANNEL_ID;
  response.channels.forEach(function (row) {
    var names = row.name
    if(names.indexOf("bunst") > -1){
     v= 0;
    }
    if (row.name.toLowerCase() == CHANNEL_NAME.toLowerCase() || row.id.toLowerCase() == CHANNEL_NAME.toLowerCase()) {
      CHANNEL_ID = row.id;
    }
  });
  return CHANNEL_ID;
}

function createSlackPostEmails(isNew, CHANNEL_NAME, subject, messageText, extra) {
  var CHANNEL_ID = getChannelIdFromName(CHANNEL_NAME)
  var message = {
    "channel": CHANNEL_ID,
    "blocks": [
      {
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": "*" + subject.trim() + "*"
        }
      },
      {
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": messageText.replace(/<\/?[^>]+>/ig, " ")
        },
      }
    ]
  };
  if (extra) {
    message.blocks.push({
      "type": "actions",
      "elements": [
        {
          "type": "button",
          "text": {
            "type": "plain_text",
            "text": "Sheet",
          },
          "url": extra
        }
      ]
    })
  }

  var url = "https://slack.com/api/chat.postMessage";

  var slackMessageTS = PropertiesService.getScriptProperties().getProperty("slack_message_email");

  console.log("New required " + isNew);

  if (!isNew && slackMessageTS && slackMessageTS.length > 0) {
    message["ts"] = slackMessageTS;
    message["as_user"] = true;
    url = "https://slack.com/api/chat.update"
  }

  var options = {
    'method': 'post',
    'contentType': 'application/json',
    'headers': {
      "Authorization": Authorization
    },
    'payload': JSON.stringify(message)
  };
  var response = UrlFetchApp.fetch(url, options);
  response = JSON.parse(response);

  if (response.error) {
    throw response.error
  }
  var ts = response.ts;
  PropertiesService.getScriptProperties().setProperty("slack_message_email", ts);
}

function sendErrNorification(messageText) {
  var message = {
    "channel": 'C011DT06BK7',
    "blocks": [
      {
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": "*Bunstres Error*"
        }
      },
      {
        "type": "section",
        "text": {
          "type": "mrkdwn",
          "text": messageText.toString()
        },
      }
    ]
  };

  var response = UrlFetchApp.fetch("https://slack.com/api/chat.postMessage", {
    'method': 'post',
    'contentType': 'application/json',
    'headers': {
      "Authorization": "Bearer xoxp-647241452166-633657186034-776659978080-3e184af328ee7b787cb0740513fd8667"
    },
    'payload': JSON.stringify(message)
  });
  response = JSON.parse(response);
  if (response.error) {
    throw response.error
  }
}


// var emailAddress = eventSheet.getRange("B1").getValue();
// var replyToEmail = eventSheet.getRange("B21").getValue();

// finalList = finalList.filter(function (item) {
//     var dateArray = []
//     data.forEach(function (row) {
//         if (row[1] == item[0] && row[3] && row[3].getTime()) {
//             dateArray.push(row[3]);
//         }
//     });
//     var min;
//     if (dateArray.length > 0) {
//         min = dateArray.reduce(function (a, b) { return a < b ? a : b; });
//         var date = Utilities.formatDate(new Date(min), "GMT+10", "yyyy-MM-dd")
//         var expire = new Date(min).getTime()
//         var now = new Date().getTime()
//         var diff = Math.floor((expire - now) / (24 * 3600 * 1000));
//         console.log(diff)
//         if (diff > 120) {
//             return item
//         } else {
//             var msg = "Create  " + type + " is failed \n" + item[3] + " x " + item[0] + "\nThis SKU is expired."
//             sendNotification(emailAddress, ' ACTION NEEDED - Expiration Date out of date', "", replyToEmail, msg, "slack")
//         }
//     } else {
//         var msg = "Create " + type + " is failed \n" + item[3] + " x " + item[0] + "\n Missing expiration date."
//         sendNotification(emailAddress, ' ACTION NEEDED - Expiration Date Missing', "", replyToEmail, msg, "slack")
//     }
// });