//Get SKUsfunction
function storeShippingTreeInventory(data) {
    // Sheet Two
    var sheet2 = SpreadsheetApp.openByUrl(SHIPPINGTREE_SHEET_URL).getSheetByName("Inventory");
    var sheetData2 = sheet2.getDataRange().getValues()
    if (sheetData2.length <= 1) {
        sheet2.appendRow(["SKU", Utilities.formatDate(new Date(), "GMT+10", "YYYY-MM-dd")]);
        var newSkuArray2 = []
        data.forEach(function (row) {
            newSkuArray2.push([[row[0].sku], [row[0].inStock]])
        })
        sheet2.getRange(2, 1, newSkuArray2.length, newSkuArray2[0].length).setValues(newSkuArray2);
    } else {
        var skuList = sheet2.getRange("A2:A").getDisplayValues().filter(String);
        var skus = skuList.map(function (row) { return row[0] });
        console.log(skus)
        var lastCol = sheet2.getLastColumn();
        var newSKUs = [];
        var newQtys = [];
        var qtys = skuList.map(function (row) { return [""] });
        data.forEach(function (item) {
            if (skus.indexOf(item[0].sku) > -1) {
                qtys.splice(skus.indexOf(item[0].sku), 1, [item[0].inStock])
            } else {
                newSKUs.push([item[0].sku]);
                newQtys.push([item[0].inStock]);
            }
        });
        sheet2.getRange(1, lastCol + 1).setValue(Utilities.formatDate(new Date(), "GMT+10", "YYYY-MM-dd"))

        if (qtys.length > 0) {
            sheet2.getRange(2, lastCol + 1, qtys.length).setValues(qtys);
        }
        if (newSKUs.length > 0) {
            sheet2.getRange(skuList.length + 2, 1, newSKUs.length).setValues(newSKUs);
            sheet2.getRange(skuList.length + 2, lastCol + 1, newSKUs.length).setValues(newQtys);
        }
    }
    data = data.map(function (row) {
        return [row[0].sku, row[0].inStock]
    });
    updateFulfilmentStatus(data, 'false');
}

function storeShippingSO(data) {
    var soSheet = SpreadsheetApp.openByUrl(SHIPPINGTREE_SHEET_URL).getSheetByName("Created SO");
    for (var i = 0; i < data.length; i++) {
        soSheet.getRange(soSheet.getLastRow() + 1, 1, 1, 6).setValues([[data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], ""]]);
    }
}

function changeData(dataArray) {
    sendBigquery(dataArray, "shippingTree")
    var sheet = SpreadsheetApp.openByUrl(EXPIREDAETSHEET_URL).getSheetByName('Data')
    var header = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
    var data = []
    dataArray.forEach(function (row) {
        var tempArray = []
        header.forEach(function (key) {
            tempArray.push(row[key])
        })
        data.push(tempArray)
    });
    var array = []
    data.forEach(function (row) {
        var now = new Date();
        now = now.setHours(23)
        now = new Date(now).setMinutes(59)
        now = new Date(now).getTime()
        if (row[4] && now > new Date(row[4]).getTime()) {
            array.push(row)
        }
    });
    sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).clearContent()
    sheet.getRange(sheet.getLastRow() + 1, 1, array.length, array[0].length).setValues(array);
    //sheet.getRange(2, 1,sheet.getLastRow()-1,sheet.getLastColumn()).sort(1);
    console.log("finish")
}

function createProjectEmail(JsonList) {
    var logSheet = SpreadsheetApp.openByUrl(LOGSHEET_URL).getSheetByName("ShippingTree");
    var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");
    var email = sheet.getRange("B17").getValue();
    var ccMail = sheet.getRange("B18").getValue();
    var replyToEmail = sheet.getRange("B21").getValue();
    var subjectTemplate = sheet.getRange("B19").getValue();
    var messageDefault = "Hi<br><br>Can you please create a new Prep project for Bunsters<br><br>";
    var reOrderRequest = JsonList

    if (reOrderRequest.length > 0) {
        if (reOrderRequest[0].sku) {
            for (var i = 0; i < reOrderRequest.length; i++) {
                var messageText = "";
                var total = reOrderRequest[i].rate * reOrderRequest[i].reorderQty;
                var merg = "<ul>";
                var componentsText = ""
                if (reOrderRequest[i].component.length > 0) {
                    var splitComponent = reOrderRequest[i].component.split('---');
                    splitComponent.forEach(function (row) {
                        merg += "<li>" + row + "</li>";
                        componentsText += "\n>" + row;
                    });
                    merg += "</ul>"
                }
                var message = messageDefault;
                var subject = subjectTemplate.replace("{SKU}", reOrderRequest[i].sku).replace("{QTY}", reOrderRequest[i].reorderQty);
                message += reOrderRequest[i].reorderQty + " x " + reOrderRequest[i].sku + " (That’s equal to " + reOrderRequest[i].reorderQty / reOrderRequest[i].QtyInCarton + " Master Cartons each with " + reOrderRequest[i].QtyInCarton + " units)<br>Components: " + merg + "Instructions : " + reOrderRequest[i].instruction
                if (total > 0) {
                    message += "Can you please make sure I'm charged the agreed time (" + reOrderRequest[i][5] + "x" + reOrderRequest[i].reorderQty + "=" + total + ")"
                }
                message += "<br><br>Beep Beep<br>Bunsters Bot";
                messageText += "Components: " + componentsText;
                toSendGmail(email, subject, ccMail, replyToEmail, message)
                sendNotification(email, "Requested New project Shipping tree: " + reOrderRequest[i].reorderQty + " x " + reOrderRequest[i].sku, ccMail, replyToEmail, messageText, "slack")
                console.log("email send !")
                logSheet.appendRow([subject, message])
            }
        }
    }
}
function skuBundleCheck(source) {
    var sheet = SpreadsheetApp.openByUrl(SHIPPINGTREE_SHEET_URL).getSheetByName("SKU Bundle Check Flag");
    sheet.clear();
    sheet.appendRow(["SKU Name", "Status"]);
    var array = [];
    source.forEach(function (data) {
        array.push([data.sku, data.Checked]);
    });
    if(array && array.length > 0 && array[0].length > 0){
      sheet.getRange(2, 1, array.length, array[0].length).setValues(array);
    }
}