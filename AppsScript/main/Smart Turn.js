//Get SKUsfunction
function storeSmartTurnInventory(data) {
    var sheet = SpreadsheetApp.getActive().getSheetByName("Inventory");
    var existingSKUs = [];
    if (sheet.getLastRow() >= 2) {
        existingSKUs = sheet.getRange(2, 1, sheet.getLastRow() - 1, 1)
            .getValues()
            .map(function (row) {
                return row[0]
            });
    }

    var stock = data;
    var newSKUs = stock.filter(function (item) {
        var sku = item[0];
        return existingSKUs.indexOf(sku) == -1
    }).map(function (sku) {
        return [sku]
    });

    if (newSKUs.length > 0) {
        sheet.getRange(sheet.getLastRow() + 1, 1, newSKUs.length).setValues(newSKUs);
    }

    var SKUs = sheet.getRange(2, 1, sheet.getLastRow() - 1, 1).getValues();
    var colNum = sheet.getLastColumn() + 1;
    sheet.getRange(1, colNum, 1, 1)
        .setValue(new Date())
        .setNumberFormat('yyyy-MM-dd');

    for (var k = 0; k < SKUs.length; k++) {
        var SKU = SKUs[k];
        var isPresent = false;
        var qty;
        for (var l = 0; l < stock.length; l++) {
            var newSKU = stock[l][0];
            if (SKU[0] === newSKU) {
                isPresent = true;
                qty = stock[l][1];
                break;
            }
        }
        if (isPresent) {
            sheet.getRange(k + 2, colNum).setValue(qty);
        }
    }
    updateFulfilmentStatus(stock, 'true');
}

function picpakExpireDate(data) {
    sendBigquery(data, "Pikpak");
    var sheet = SpreadsheetApp.openByUrl(PICPAK_EXPIREDAETSHEET_URL).getSheetByName('Data')
    var array = []
    data.forEach(function (row) {
        var now = new Date();
        now = now.setHours(23)
        now = new Date(now).setMinutes(59)
        now = new Date(now).getTime()
        if (row[4] && now > new Date(row[4]).getTime()) {
            array.push(row)
        }
    });
    if (sheet.getLastRow() > 1) {
        sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).clearContent()
    }
    sheet.getRange(sheet.getLastRow() + 1, 1, array.length, array[0].length).setValues(array);
    console.log("finish")
}

//Create SO
function storeSmartTurnSO(data) {
    var soSheet = SpreadsheetApp.getActive().getSheetByName("Created SO");
    soSheet.getRange(soSheet.getLastRow() + 1, 1, 1, 6).setValues([data]);
}

function sendEmailOrderId(data, ORDERID) {
    var logSheet = SpreadsheetApp.openByUrl(LOGSHEET_URL).getSheetByName("Pikpak");
    var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");
    var email = sheet.getRange("B13").getValue();
    var ccMail = sheet.getRange("B14").getValue();
    var subject = sheet.getRange("B15").getValue().replace("{ORDERID}", ORDERID);
    var replyToEmail = sheet.getRange("B21").getValue();
    var message = "SKU Produced : " + data.sku;
    var slackMessageText = "SO : " + ORDERID + "\nComponents: ";
    if (data.componets.length > 0) {
        message += '<br><br>' + "Component SKUs consumed :"
        var split = data.componets.split('---');
        split.forEach(function (row) {
            message += "<br>" + row;
            slackMessageText += "\n>" + row
        });
    }
    message += "<br><br>Instructions: " + data.instruction;
    toSendGmail(email, subject, ccMail, replyToEmail, message)
    sendNotification(email, "Created SO in Pickpack : " + data.sku, ccMail, replyToEmail, slackMessageText, "slack")
    logSheet.appendRow([subject, message])
}
