function getOutOfStockAmazon() {
    var sheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Inventory");
    var Header = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues();
    var lastColumn = sheet.getLastColumn() - 1;

    var uniqSku = [];
    var listItems = {};
    Header.forEach(function (row) {
        if (row[1].indexOf('CA') == -1) {
            if (uniqSku.indexOf(row[1]) == -1) {
                listItems[row[1]] = {};
                listItems[row[1]][row[0]] = row[lastColumn];
                uniqSku.push(row[1]);
            }
            else {
                listItems[row[1]][row[0]] = row[lastColumn];
            }
        }
    });

    var currentDate = sheet.getRange(1, sheet.getLastColumn(), 1, 1).getValue();
    var minStock = eventFunction("amazon", currentDate);

    var skus = [];
    var qtys = [];
    for (var sku in listItems) {
        var listDetails = listItems[sku];
        var qty = 0;
        for (var status in listDetails) {
            if (status != "Reserved") {
                var q = (Number(listDetails[status]).toString() != 'NaN') ? Number(listDetails[status]) : 0
                qty = qty + q;
            }
        }
        qty = Number(qty);
        skus.push(sku);
        qtys.push([qty]);
    }

    var outOfStock = [];
    for (var i = 0; i < skus.length; i++) {
        for (var j = 0; j < minStock.length; j++) {
            var stock = minStock[j];
            if (skus[i] == stock[0]
                && qtys[i][0] < stock[1]) {
                outOfStock.push([stock[0], qtys[i][0], stock[1], stock[2], stock[3], stock[4], stock[5], stock[6]]);
            }
        }
    }

    var soSheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Created Order");

    var discardSKUs;
    if (soSheet.getLastRow() >= 2) {
        var pastOutOfStock = soSheet
            .getRange(2, 1, soSheet.getLastRow() - 1, 5)
            .getValues()
            .filter(function (row) {
                var now = new Date();
                var lessThan7Days = ((now.getTime() - row[1].getTime()) < (7 * 24 * 60 * 60 * 1000));
                //var notFilfilled = !row[4];
                return lessThan7Days;
            });
        discardSKUs = pastOutOfStock.map(function (row) {
            return row[0];
        });
    } else {
        discardSKUs = [];
    }

    var validOutOfStocks = outOfStock.filter(function (row) {

        return discardSKUs.indexOf(row[0]) == -1
    });

    var SPANDLTLsheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Amazon Choose SPD or LTL");
    var SPANDLTLdata = SPANDLTLsheet.getRange(2, 1, SPANDLTLsheet.getLastRow() - 1, SPANDLTLsheet.getLastColumn()).getValues();
    var SPANDLTLJson = {};
    SPANDLTLdata.forEach(function (row) {
        SPANDLTLJson[row[0]] = row[1];
    });

    var rows = [];
    validOutOfStocks.forEach(function (row) {
        var QtyInCarton = Number(row[6]);
        var reOrderOty = Number(row[3]);
        var CortonCount = reOrderOty / QtyInCarton;
        var check = reOrderOty % QtyInCarton;
        var palletCount = CortonCount / 12;
        if (check == 0) {
            if (palletCount >= 1) {          
                if(palletCount % 1 == 0){
                   var newPalletQty = (palletCount >= SPANDLTLJson[row[7]]) ?  palletCount : CortonCount;
                   rows.push([row[0] + "-PLT", newPalletQty, palletCount, row[0], row[1], row[2], row[3], row[6], row[7], FreightReadyDate(new Date())]);
                }else{
                   var msg = "The following Amazon order is failed \nThis is Not enough SKU Qty for " + row[3] + " x " + row[0]
                   sendNotification("", ' ACTION NEEDED - Not enough SKU Qty ', "", "", msg, "slack")
                }
            } else {
                rows.push([row[0] + "-MC", CortonCount, palletCount, row[0], row[1], row[2], row[3], row[6], row[7], FreightReadyDate(new Date())]);
            }
        }
    });

    var eventSheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");
    var finalList = checkHomeWarehouse(rows, eventSheet);
    finalList = getDimentions(finalList, eventSheet);
    finalList = checkExpireDate(finalList, eventSheet);

    var JsonList = [];
    for (var i = 0; i < finalList.length; i++) {
        var arrayLen = finalList[i].length;
        var componentsList = '';
        JsonList.push({
            "Qty": finalList[i][1], "sku": finalList[i][3], "skuQty": finalList[i][6], "warehouse": finalList[i][8], "palletQty": finalList[i][2],
            "parcel": finalList[i][10], "weight": finalList[i][11], "Boxlength": finalList[i][12], "width": finalList[i][13], "heigth": finalList[i][14],
            "expire": finalList[i][15], "FreightReadyDate": finalList[i][9], "QuantityInCase": finalList[i][7], "shipmentByName": finalList[i][0], "currentQty": finalList[i][4], "reoderBelow": finalList[i][5], "minPallet" : SPANDLTLJson[finalList[i][8]]
        });
    }
    return JsonList;
}

function manageJson(reOrderRequest){
  var newReOrderRequest = [];
  for (var a = 0; a < reOrderRequest.length; a++) {
    var req = reOrderRequest[a];
    if (req.parcel == "SP") {
      newReOrderRequest.push(req);
    } else {
      if ((req.palletQty % 1) == 0) {
        newReOrderRequest.push(req);
      } else {
        var split = req.palletQty.toString().split(".");
        var newPalletQty = split[0];
        var newSPQty = Math.round(Number(('0.' + split[1]) * 12).toFixed(0));
        for (var b = 1; b <= 2; b++) {
          var newReq = JSON.parse(JSON.stringify(req));
          if (b == 1) {
            newReq.palletQty = Number(newPalletQty);
            newReq.skuQty = Number(newPalletQty) * 12 * newReq.QuantityInCase;
            newReq.Qty = Number(newPalletQty);
          } else {
            newReq.palletQty = newSPQty / 12;
            newReq.skuQty = newSPQty * newReq.QuantityInCase;
            newReq.Qty = newSPQty;
            newReq.parcel = "SP";
            newReq.shipmentByName = newReq.shipmentByName.replace("PLT", "MC");
          }
          newReOrderRequest.push(newReq);
        }
      }
    }
  }
  return newReOrderRequest;
}

function checkHomeWarehouse(list, eventSheet) {
    var sheet = SpreadsheetApp.openByUrl(SHIPPINGTREE_SHEET_URL).getSheetByName("Inventory");
    var picpakSheet = SpreadsheetApp.getActive().getSheetByName("Inventory");
    var data = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues()
    var picpakData = picpakSheet.getRange(2, 1, picpakSheet.getLastRow() - 1, picpakSheet.getLastColumn()).getValues()
    var lastCol = sheet.getLastColumn() - 1;
    var picpakLastCol = picpakSheet.getLastColumn() - 1;
    var emailAddress = eventSheet.getRange("B1").getValue();
    var replyToEmail = eventSheet.getRange("B21").getValue();

    var array = [];
    list.forEach(function (item) {
        var available = false;
        if (item[8] == "US-AMZ" || item[8] == "CA-AMZ") {
            data.forEach(function (row) {
                if (item[3] == row[0]) {
                    if (item[6] <= row[lastCol]) {
                        array.push(item);
                        available = true;
                    } else {
                        var msg = "The following Amazon order is failed \n" + item[6] + " x " + item[3] + " \nThis SKU is out of stock in shipping tree \n" + amazonCreateMessage(item[3], [[item[3], item[4], item[6], row[lastCol]]], item[8])
                        sendNotification(emailAddress, ' ACTION NEEDED - Out of Stock ', "", replyToEmail, msg, "slack")
                        available = true;
                    }
                }
            });
        } else {
            picpakData.forEach(function (row) {
                if (item[3] == row[0]) {
                    if (item[6] <= row[picpakLastCol]) {
                        array.push(item);
                        available = true;
                    } else {
                        var msg = "The following Amazon order is failed \n" + item[6] + " x " + item[3] + " \nThis SKU is out of stock in Pikpak \n" + amazonCreateMessage(item[3], [[item[3], item[4], item[6], row[lastCol]]], item[8])
                        sendNotification(emailAddress, ' ACTION NEEDED - Out of Stock ', "", replyToEmail, msg, "slack")
                        available = true;
                    }
                }
            });
        }
        //amazonCreateMessage
        if (!available) {
            var msg = "The following Amazon order is failed \n" + item[6] + " x " + item[3] + " \nThis SKU is missing in shipping tree SKU list"
            sendNotification(emailAddress, ' ACTION NEEDED - Missing SKU ', "", replyToEmail, msg, "slack")
        }
    });
    array = ChooseSPDorLTL(array);
    return array;
}

function ChooseSPDorLTL(finalList) {
    var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Amazon Choose SPD or LTL");
    var data = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues()
    finalList.forEach(function (row) {
        data.forEach(function (item) {
            if (row[8] == item[0]) {
                if (row[2] >= item[1]) {
                    row.push("LTL");
                } else {
                    row.push("SP");
                }
            }
        });
    });
    return finalList;
}

function getDimentions(rows, eventSheet) {
    var sheet = SpreadsheetApp.openByUrl(SKU_DETAILS_URL).getSheetByName("SKU Master list");
    var sheetHeader = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];

    var WeightKG = sheetHeader.indexOf("Dimensions - Weight (kg)");
    var LengthCM = sheetHeader.indexOf("Dimensions - Length (cm)");
    var WidthCM = sheetHeader.indexOf("Dimensions - Width (cm)");
    var HeightCM = sheetHeader.indexOf("Dimensions - Height (cm)");
    var WeightLBS = sheetHeader.indexOf("Dimensions - Weight (lbs)");
    var LengthIN = sheetHeader.indexOf("Dimensions - Length (inches)");
    var WidthIN = sheetHeader.indexOf("Dimensions - Width (inches)");
    var HeightIN = sheetHeader.indexOf("Dimensions - Height (inches)");

    var data = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues();
    var emailAddress = eventSheet.getRange("B1").getValue();
    var replyToEmail = eventSheet.getRange("B21").getValue();

    rows.forEach(function (item) {
        data.forEach(function (row) {
            if (row[0] == item[0]) {
                if (item[8] == "US-AMZ") {
                    item.push(row[WeightLBS], row[LengthIN], row[WidthIN], row[HeightIN]);
                } else {
                    item.push(row[WeightKG], row[LengthCM], row[WidthCM], row[HeightCM]);
                }
            }
        });
    });
    rows = rows.filter(function (row) {
        if (!row[11] || row[11] == "" || !row[12] || row[12] == "" || !row[13] || row[13] == "" || !row[14] || row[14] == "") {
            var msg = "The following Amazon order is failed \n" + row[6] + " x " + row[3] + " \nThis SKU of box dimensions is Missing";
            sendNotification(emailAddress, ' ACTION NEEDED - Box Dimension Missing', "", replyToEmail, msg, "slack");
        } else {
            return row;
        }
    });
    return rows;
}

function checkExpireDate(finalList, eventSheet) {
    var sheet = SpreadsheetApp.openByUrl(EXPIREDAETSHEET_URL).getSheetByName("Data");
    var data = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues();
    var picpakSheet = SpreadsheetApp.openByUrl(PICPAK_EXPIREDAETSHEET_URL).getSheetByName("Data");
    var picpakData = picpakSheet.getRange(2, 1, picpakSheet.getLastRow() - 1, picpakSheet.getLastColumn()).getValues();

    var emailAddress = eventSheet.getRange("B1").getValue();
    var replyToEmail = eventSheet.getRange("B21").getValue();

    finalList.forEach(function (item) {
        var dateArray = []
        if (item[8] == "AU-AMZ") {
            picpakData.forEach(function (row) {
                if (row[1] == item[3] && row[3] && row[3].getTime()) {
                    dateArray.push(row[3]);
                }
            });
        } else {
            data.forEach(function (row) {
                if (row[1] == item[3] && row[3] && row[3].getTime()) {
                    dateArray.push(row[3]);
                }
            });
        }
        var min;
        if (dateArray.length > 0) {
            min = dateArray.reduce(function (a, b) { return a < b ? a : b; });
            var date = Utilities.formatDate(new Date(min), "GMT+10", "yyyy-MM-dd")
            var expire = new Date(min).getTime();
            var now = new Date().getTime();
            var diff = Math.floor((expire - now) / (24 * 3600 * 1000));
            console.log(diff);
            if (diff > 100) {
                item.push(date);
            } else {
                item.push("");
                var msg = "The following Amazon order is failed \n" + item[6] + " x " + item[3] + "\nThis SKU is expired."
                sendNotification(emailAddress, ' ACTION NEEDED - Expiration Date out of date', "", replyToEmail, msg, "slack")
            }
        } else {
            item.push("");
            var msg = "The following Amazon order is failed \n" + item[6] + " x " + item[3] + "\n Missing expiration date."
            sendNotification(emailAddress, ' ACTION NEEDED - Expiration Date Missing', "", replyToEmail, msg, "slack")
        }
    });
    finalList = finalList.filter(function (row) {
        if (row[15] != "") {
            return row;
        }
    });
    return finalList;
}

function FreightReadyDate(date) {
    //var date = new Date(2020,0,25);
    var day = date.getDay();
    var datee = date.getDate();
    if (day == 0) {
        date.setDate(datee + 3);
    } else if (day == 6 || day == 5 || day == 4) {
        date.setDate(datee + 4);
    } else {
        date.setDate(datee + 2);
    }
    return Utilities.formatDate(date, "GMT+10", "yyyy-MM-dd");
}

function amazonCreateMessage(sku, arr, Warehouse) {
    var message = ">*SKU*: " + sku + " (" + Warehouse.replace("AMZ", "1") + ") \n";
    arr.forEach(function (val) {
        message += '>*Current Qty*: ' + val[3] + '\n>*Required Qty*: ' + val[2] + '\n>*AMZ Current Qty*: ' + val[1] + '\n>*AMZ Required Qty*: ' + val[2];
    });

    Logger.log(message);
    return message;
}

function storeAmazonOrder(data) {
    var sheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Created Order");
    sheet.appendRow(data);
}