CONFIG_SHEET_URL = "https://docs.google.com/spreadsheets/d/1Fc5jv27mC2M-LxDjlbajrxGqhIEReDJ2ZCm50qCQnCI/";
SHIPPINGTREE_SHEET_URL = 'https://docs.google.com/spreadsheets/d/1oCx9ltq4sn1v9ycV6G6mDzxO4V_B75LxmUc7WTOvlQA/edit#gid=0';
LOGSHEET_URL = "https://docs.google.com/spreadsheets/d/1CKXQEHXO5vP8hDkUe50zCabS5Ft3PpuQk8YvdYGrTv0/edit#gid=0";
AMAZON_INVENTORY_SHEET = "https://docs.google.com/spreadsheets/d/1TfAEcRYbXpV89SyYzG-2q99F9m4PFgBuitYSkZUrhmA/edit#gid=0";
SKU_DETAILS_URL = "https://docs.google.com/spreadsheets/d/1neK6yGBdO6JwD_2Z-DQ4vgxq_JFLImwugJ3kBjNfYzQ/";
EXPIREDAETSHEET_URL = "https://docs.google.com/spreadsheets/d/1HG1aW1tTN5sDAHhkNuWDlUUi45YbfyQDB68fPPp4_cE/";
PICPAK_EXPIREDAETSHEET_URL = "https://docs.google.com/spreadsheets/d/1s8DWzrdg-Oqbm3AtB9gAQp1MpmxFwAal9ksOiH4vsUk/edit#gid=0";
AMAZONINBOUND_SHEET_URL = "https://docs.google.com/spreadsheets/d/1V3Nyrw7mTsJdPFJffr0zSF1ZxTFNUxFDlV7vHrHW_-o/edit#gid=0";
projectId = "bunsters";

//Get SKUsfunction
function doPost(e) {
    var data = JSON.parse(e.postData.contents);
    try {
        if (data.stock) {
            storeSmartTurnInventory(data.stock);
        } else if (data.so) {
            storeSmartTurnSO(data.so);
        } else if (data.ShippingStocks) {
            storeShippingTreeInventory(data.ShippingStocks);
        } else if (data.comments) {
            sendEmailOrderId(data.comments, data.orderId);
        } else if (data.inventory) {
            return storeInventory(data.data);
        } else if (data.inbound) {
            return storeInboundDetails(data.data);
        } else if (data.dailyCSV) {
            changeData(data.dailyCSV);
        } else if (data.sendMail) {
            emailSend(data.sendMail);
        } else if (data.sendErrorMail) {
            sendErrorMail(data.sendErrorMail);
        } else if (data.picpakExpireDate) {
            picpakExpireDate(data.picpakExpireDate);
        } else if (data.amazonS0alesOrders) {
            storeSalesOrder(data.amazonS0alesOrders);
        } else if (data.skuBundleStatus) {
            skuBundleCheck(data.skuBundleStatus);
        }
    } catch (e) {
        sendErrNorification(e.stack);
    }
}


function doGet(e) {
    var JsonList;
    try {
        if (e.parameter["action"] == "shippingTree") {
            JsonList = getOutOfStock("shippingTree");
        } else if (e.parameter["action"] == "smartturn") {
            JsonList = getOutOfStock("Pikpak");
        } else if (e.parameter["action"] == "basicDetails") {
            JsonList = getBasicDetails();
        } else {
            JsonList = getOutOfStockAmazon();
        }
        return ContentService.createTextOutput(JSON.stringify(JsonList))
            .setMimeType(ContentService.MimeType.JSON);
    } catch (e) {
        sendErrNorification(e.stack);
    }
}