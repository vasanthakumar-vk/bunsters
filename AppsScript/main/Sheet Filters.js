
function showPastMonthData() {
  var SS = SpreadsheetApp.getActiveSpreadsheet();
  var sh = SS.getSheetByName('Created SO');
  var ra = sh.getRange(2, 1, sh.getLastRow() - 1, sh.getLastColumn());
  ra.sort([{ column: 2, ascending: true }]);

  sh.showRows(2, sh.getMaxRows() - 2);
  var mk = SS.getSheetByName('marker');
  var num = mk.getRange(2, 3).getValue();
  sh.hideRows(2, num);

}
