function sendBigquery(datas, typeOfSite) {
    try {
        var newData = [];
        datas.forEach(function (row) {
            newData.push(createJson(row, typeOfSite));
        });

        var query;
        if (newData.length > 0) {
            var table = generateCustomTable(newData, typeOfSite);

            query = "MERGE `bunsters.master.Expiration_Date` T ";
            query += " USING (SELECT SKU, Lot_Number, MIN(Expire_Date) Expire_Date, Warehouse, Updated_Timestamp FROM (" + table + ") GROUP BY SKU, Lot_Number, Warehouse, Updated_Timestamp ) S";
            query += ' ON  T.SKU = S.SKU AND T.Warehouse = S.Warehouse AND IFNULL(T.Lot_Number,"-") = IFNULL(S.Lot_Number,"-") ';
            query += " WHEN MATCHED THEN UPDATE SET Expire_Date = S.Expire_Date, Updated_Timestamp = S.Updated_Timestamp";
            query += " WHEN NOT MATCHED";
            query += " THEN INSERT ( SKU, Lot_Number, Expire_Date, Warehouse, Updated_Timestamp)";
            query += " VALUES (S.SKU, S.Lot_Number, S.Expire_Date, S.Warehouse, S.Updated_Timestamp)";

            runQuery(query);
        }
    } catch (e) {
        sendErrNorification(e.stack);
    }
}

function runQuery(query) {
    //DriveApp.getFolders()
    var queryResults = BigQuery.Jobs.query({
        "query": query,
        "useLegacySql": false
    }, projectId);

    var jobId = queryResults.jobReference.jobId;
    var sleepTimeMs = 500;
    while (!queryResults.jobComplete) {
        Utilities.sleep(sleepTimeMs);
        queryResults = BigQuery.Jobs.getQueryResults(projectId, jobId);
    }
    return {
        jobId: jobId,
        bytes: queryResults.totalBytesProcessed
    };
}

function generateCustomTable(newVals, typeOfSite) {
    var schema = getSchema();
    var textTable = "";
    var count = 0;

    for (var index = 0; index < newVals.length; index++) {
        var row = newVals[index];
        var tempText = "SELECT\n";
        for (var key in row) {
            if (schema[key] == "NUMERIC" || schema[key] == "FLOAT") {
                tempText += ((row[key] && Number(row[key]).toString() != "NaN") ? Number(row[key]) : "NULL") + ' ' + key + ",";
            } else if (schema[key] == "TIMESTAMP") {
                tempText += ' TIMESTAMP(' + ((row[key]) ? '"' + row[key] + '"' : "NULL") + ') ' + key + ",";
            } else if (schema[key] == "DATE" && typeOfSite == "shippingTree") {
                tempText += ' DATE(' + ((row[key]) ? '"' + row[key] + '"' : "NULL") + ') ' + key + ",";
            } else if (schema[key] == "DATE" && typeOfSite == "Pikpak") {
                tempText += ' DATE(' + ((row[key]) ? 'PARSE_TIMESTAMP("%m/%d/%Y","' + row[key] + '")' : "NULL") + ') ' + key + ",";
            } else {
                tempText += ' ' + ((row[key]) ? ('"' + row[key] + '"') : "STRING(NULL)") + ' ' + key + ",";
            }
        }
        textTable += tempText.slice(0, tempText.length - 1) + "\n UNION ALL \n";
        count++;
    }
    return textTable.slice(0, textTable.length - 11);
}

function createJson(data, typeOfSite) {
    var Warehouse = "AU-1";
    var flag = true;
    if (typeOfSite == "shippingTree") {
        Warehouse = "US-1";
        flag = false;
    }
    return {
        "SKU": flag ? data[1] : data.SKU,
        "Lot_Number": flag ? data[2] : undefined,
        "Expire_Date": flag ? data[3] : data["Lot Number"],
        "Warehouse": Warehouse,
        "Updated_Timestamp": Utilities.formatDate(new Date(), "GMT", "YYYY-MM-dd HH:MM:ss")
    };
}

function getSchema() {
    return {
        "SKU": "STRING",
        "Lot_Number": "STRING",
        "Expire_Date": "DATE",
        "Warehouse": "STRING",
        "Updated_Timestamp": "TIMESTAMP"
    };
}