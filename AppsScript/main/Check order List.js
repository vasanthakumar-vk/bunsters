function checkOrderNum() {
    var sheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL);
    var eventSheet = sheet.getSheetByName("Events");
    var dateSheet = sheet.getSheetByName("Date bound Inventory");
    var emailSheet = sheet.getSheetByName("Email Config");
    emailSheet.getRange(24, 2, 3, 1).setValue(false);
    var dateSheetHeader = dateSheet.getRange(1, 1, 1, dateSheet.getLastColumn()).getValues()[0];
    var dateSheetDatas = dateSheet.getRange(2, 1, dateSheet.getLastRow() - 1, dateSheet.getLastColumn()).getValues();

    var WarehouseIndex = dateSheetHeader.indexOf("Warehouse")
    var SKUIndex = dateSheetHeader.indexOf("SKU")
    var eventIndex = dateSheetHeader.indexOf("Event")
    var flagIndex = dateSheetHeader.indexOf("Auto Reorder Flag")
    var reorderAtIndex = dateSheetHeader.indexOf("Min Stock Levels")//Re-order when inventory falls below
    var reorderQtyIndex = dateSheetHeader.indexOf("Order Quantity")//Re-order Quantity
    var rateIndex = dateSheetHeader.indexOf("Rate")
    var instructionIndex = dateSheetHeader.indexOf("Link to Instructions")
    var QtyOfMasterIndex = dateSheetHeader.indexOf("MOQ (Minimum Order Quantity)")//Qty in Master Carton    

    var dateSheetData = [];
    dateSheetDatas.forEach(function (row) {
        if ((Number(row[reorderAtIndex]).toString() == 'NaN' || Number(row[reorderQtyIndex]).toString() == 'NaN') && row[flagIndex]) {
            dateSheetData.push([row[WarehouseIndex], row[SKUIndex], row[eventIndex], row[flagIndex], row[reorderAtIndex], row[reorderQtyIndex], row[rateIndex], row[instructionIndex], row[QtyOfMasterIndex]])
        }
    });

    var eventDateRange = eventSheet.getRange(2, 1, eventSheet.getLastRow() - 1, 3).getValues()
    var DefaultEventName = ""
    eventDateRange.forEach(function (row) {
        if (row[1] == "Default") {
            DefaultEventName = row[0]
        }
    });

    var val = [];
    var startingDate;
    var endingDate;
    var selectedArray = [];
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    for (i = 0; i < dateSheetData.length; i++) {
        var sku = dateSheetData[i];
        var event = sku[2];
        for (j = 0; j < eventDateRange.length; j++) {
            if (event == eventDateRange[j][0]) {
                var theDate = [eventDateRange[j][1], eventDateRange[j][2]]
            } else {
                continue;
            }
            if (theDate[0] != "Default") {
                startingDate = new Date(currentYear, theDate[0].getMonth(), theDate[0].getDate());//.getTime();
                endingDate = new Date(currentYear, theDate[1].getMonth(), theDate[1].getDate());//.getTime(); 
                if (currentDate >= startingDate) {
                    if (currentDate <= endingDate) {
                        val.push([sku[1], sku[2], sku[3], sku[4], sku[5], sku[6], sku[7]])
                        selectedArray.push(sku[1])
                    }
                }
            }
        }
    }
    var ss = dateSheetData.filter(function (row) {
        if (row[2] == DefaultEventName) {
            return row;
        }
    }).forEach(function (row) {
        if (selectedArray.indexOf(row[0]) == -1) {
            val.push([row[1], row[2], row[3], row[4], row[5], row[6], row[7]])
        }
    });
    if (val.length != 0) {
        var reOrder = 0;
        var reOrderAt = 0;
        val.forEach(function (row) {
            if (Number(row[3]).toString() == "NaN") {
                reOrderAt++;
            }
            if (Number(row[4]).toString() == "NaN") {
                reOrder++;
            }
        });
        var msg = "";
        if (reOrder > 0) {
            msg += reOrder + " Reorder Quantity \n";
        }
        if (reOrderAt > 0) {
            msg += reOrderAt + " Reorder when inventory falls below Quantity";
        }
        sendNotification("", '#NA Errors in Sheet', "", "", msg, "slack", sheet.getUrl() + "#gid=" + dateSheet.getSheetId())
    }
}