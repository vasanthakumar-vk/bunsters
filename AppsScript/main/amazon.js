function storeInventory(data) {
    var sheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Inventory");

    //Get existing SKUs
    var skuList = sheet.getRange("B2:B").getDisplayValues().filter(String);
    var skus = skuList.map(function (row) { return row[0] });
    var uniqueSKUs = skus.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
    });

    var lastCol = sheet.getLastColumn();

    var newSKUs = [];
    var newQtys = [];
    var qtys = skuList.map(function (row) { return [""] });
    data.forEach(function (item) {
        if (uniqueSKUs.indexOf(item.sku) > -1) {
            qtys.splice(skus.indexOf(item.sku), 3, [item.available], [item.reserved], [item.inbound])
        } else {
            newSKUs.push([item.sku], [item.sku], [item.sku]);
            newQtys.push([item.available], [item.reserved], [item.inbound]);
        }
    });

    sheet.getRange(1, lastCol + 1).setValue(Utilities.formatDate(new Date(), "GMT+10", "YYYY-MM-dd"));

    if (qtys.length > 0) {
        sheet.getRange(2, lastCol + 1, qtys.length).setValues(qtys);
    }

    if (newSKUs.length > 0) {
        sheet
            .getRange(skuList.length + 2, 1, newSKUs.length)
            .setValues(newSKUs
                .map(function (item, index) {
                    switch (index % 3) {
                        case 0:
                            return ["Available"];
                        case 1:
                            return ["Reserved"];
                        case 2:
                            return ["Inbound"]
                    }
                }));
        sheet.getRange(skuList.length + 2, 2, newSKUs.length).setValues(newSKUs);
        sheet.getRange(skuList.length + 2, lastCol + 1, newSKUs.length).setValues(newQtys);
    }
    amazonUpdateFulfilmentStatus()
    return ContentService.createTextOutput("Success")
}

function storeInboundDetails(data) {
    var sheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Inbound Details");

    //Get existing SKUs
    var skuList = sheet.getRange("B2:B").getDisplayValues().filter(String);
    var skus = skuList.map(function (row) { return row[0] });
    var uniqueSKUs = skus.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
    });

    var lastCol = sheet.getLastColumn();
    //If last column has no values - overwrite it..
    if (sheet.getLastRow() > 2 && sheet.getRange(2, lastCol, sheet.getLastRow() - 1).getValues().filter(String).length == 0 && lastCol > 2) {
        lastCol = lastCol - 1;
    }

    var newSKUs = [];
    var newQtys = [];
    var qtys = skuList.map(function (row) { return [""] });
    data.forEach(function (item) {
        if (uniqueSKUs.indexOf(item.sku) > -1) {
            qtys.splice(skus.indexOf(item.sku), 10,
                [item.WORKING], [item.SHIPPED], [item.IN_TRANSIT],
                [item.DELIVERED], [item.CHECKED_IN], [item.RECEIVING],
                [item.CLOSED], [item.CANCELLED], [item.DELETED],
                [item.ERROR]
            )
        } else {
            newSKUs.push([item.sku], [item.sku], [item.sku], [item.sku], [item.sku], [item.sku],
                [item.sku], [item.sku], [item.sku], [item.sku]);
            newQtys.push([item.WORKING], [item.SHIPPED], [item.IN_TRANSIT],
                [item.DELIVERED], [item.CHECKED_IN], [item.RECEIVING],
                [item.CLOSED], [item.CANCELLED], [item.DELETED],
                [item.ERROR]);
        }
    });

    sheet.getRange(1, lastCol + 1).setValue(Utilities.formatDate(new Date(), "GMT+10", "YYYY-MM-dd"))

    if (qtys.length > 0) {
        sheet.getRange(2, lastCol + 1, qtys.length).setValues(qtys);
    }

    if (newSKUs.length > 0) {
        sheet
            .getRange(skuList.length + 2, 1, newSKUs.length)
            .setValues(newSKUs
                .map(function (item, index) {
                    switch (index % 10) {
                        case 0:
                            return ["WORKING"];
                        case 1:
                            return ["SHIPPED"];
                        case 2:
                            return ["IN_TRANSIT"];
                        case 3:
                            return ["DELIVERED"];
                        case 4:
                            return ["CHECKED_IN"];
                        case 5:
                            return ["RECEIVING"];
                        case 6:
                            return ["CLOSED"];
                        case 7:
                            return ["CANCELLED"];
                        case 8:
                            return ["DELETED"];
                        case 9:
                            return ["ERROR"];
                    }
                }));
        sheet.getRange(skuList.length + 2, 2, newSKUs.length).setValues(newSKUs);
        sheet.getRange(skuList.length + 2, lastCol + 1, newSKUs.length).setValues(newQtys);
    }

    return ContentService.createTextOutput("Success")
}

function getBasicDetails() {
    var sheet = SpreadsheetApp.openByUrl(AMAZONINBOUND_SHEET_URL).getSheetByName("Config");
    var json = {
        "ContactName": sheet.getRange("B2").getValue(),
        "ContactPhone": sheet.getRange("B3").getValue(),
        "ContactEmail": sheet.getRange("B4").getValue(),
        "ContactFax": sheet.getRange("B5").getValue(),
    }
    return json
}

function emailSend(data) {
    var eventSheet = SpreadsheetApp.openByUrl(CONFIG_SHEET_URL).getSheetByName("Email Config");
    var emailAddress = eventSheet.getRange("B1").getValue();
    var res = data;
    var subject

    var message = "Created the following order for Amazon :  " + '\n>FBA : ' + res.FBAOrderId + ""

    if (res.CountryCode == "AU") {
        message += '\n>Picpak SO : ' + res.orderId + ""
    } else {
        message += '\n>Shipping Tree : ' + res.orderId + "" + '\n>Shipping Tree Link : https://app.shippingtree.co/order/view/' + res.orderId + "/"
    }
    message += '\n>' + res.Qty + ' x ' + res.SKU + ""

    if (res.parcel == "SP") {
        subject = '\nOUTBOUND ORDER ' + res.FBAOrderId + ' - ' + res.orderId
        if (res.CountryCode == "AU") {
            message += "\nLabels are emailed to service@pikpak.com.au"
        } else {
            message += "\nLabels are attached to the order."
        }
    } else {
        subject = '\nOUTBOUND ORDER - Pallet ' + res.FBAOrderId + ' - ' + res.orderId
        if (res.CountryCode == "AU") {
            message += "\nLTL pickup by Amazon scheduled for " + res.pickupDate + ". \nPallet and Carton Labels are emailed to service@pikpak.com.au"
        } else {
            message += "\nLTL pickup by Amazon scheduled for " + res.pickupDate + ". \nPallet and Carton Labels are attached to the order."
        }
    }
    storeAmazonOrder([res.SKU, Utilities.formatDate(new Date(), "GMT+10", "YYYY-MM-dd"), res.Qty, false, res.FBAOrderId, res.orderId])
    sendNotification(emailAddress, subject, "", "", message, "slack")
}

function amazonUpdateFulfilmentStatus() {
    var soSheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Created Order");
    var sheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Inventory");
    var Header = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues();
    var lastColumn = sheet.getLastColumn() - 1;

    var uniqSku = [];
    var listItems = {};
    Header.forEach(function (row) {
        if (uniqSku.indexOf(row[1]) == -1) {
            listItems[row[1]] = {};
            listItems[row[1]][row[0]] = row[lastColumn];
            uniqSku.push(row[1]);
        }
        else {
            listItems[row[1]][row[0]] = row[lastColumn];
        }
    });

    var stock = [];
    for (var sku in listItems) {
        var listDetails = listItems[sku];
        var qty = 0;
        for (var status in listDetails) {
            var q = (Number(listDetails[status]).toString() != 'NaN') ? Number(listDetails[status]) : 0
            qty = qty + q;
        }
        qty = Number(qty);
        stock.push([sku, qty]);
    }


    if (soSheet.getLastRow() < 2) {
        return;
    }

    soSheet.getRange(2, 1, soSheet.getLastRow() - 1, 6).getValues().forEach(function (row, index) {
        var now = new Date();
        var notFilfilled = !row[3];
        if (notFilfilled) {
            for (var i = 0; i < stock.length; i++) {
                if (row[0] == stock[i][0]
                    && stock[i][1] >= row[2]) {
                    soSheet.getRange(index + 2, 4).setValue(true);
                }
            }
        }
    });
}

function storeSalesOrder(data) {
    data = [
        ['113-1194276-6845040', 'BU-HS-12-US', '1', 'US'],
        ['701-4985083-1565819', 'BU-HS-12-CA', '1', 'CA']
    ]

    var sheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Sales Orders");
    sheet.getRange(sheet.getLastRow() + 1, 1, data.length, data[0].length).setValues(data)



}