CONFIG_SHEET_URL = "https://docs.google.com/spreadsheets/d/1Fc5jv27mC2M-LxDjlbajrxGqhIEReDJ2ZCm50qCQnCI/";
SHIPPINGTREE_SHEET_URL = 'https://docs.google.com/spreadsheets/d/1oCx9ltq4sn1v9ycV6G6mDzxO4V_B75LxmUc7WTOvlQA/edit#gid=0';
LOGSHEET_URL = "https://docs.google.com/spreadsheets/d/1CKXQEHXO5vP8hDkUe50zCabS5Ft3PpuQk8YvdYGrTv0/edit#gid=0";
AMAZON_INVENTORY_SHEET = "https://docs.google.com/spreadsheets/d/1TfAEcRYbXpV89SyYzG-2q99F9m4PFgBuitYSkZUrhmA/edit#gid=0";
SKU_DETAILS_URL = "https://docs.google.com/spreadsheets/d/1neK6yGBdO6JwD_2Z-DQ4vgxq_JFLImwugJ3kBjNfYzQ/";
EXPIREDAETSHEET_URL = "https://docs.google.com/spreadsheets/d/1HG1aW1tTN5sDAHhkNuWDlUUi45YbfyQDB68fPPp4_cE/";
PICPAK_EXPIREDAETSHEET_URL = "https://docs.google.com/spreadsheets/d/1s8DWzrdg-Oqbm3AtB9gAQp1MpmxFwAal9ksOiH4vsUk/edit#gid=0";
AMAZONINBOUND_SHEET_URL = "https://docs.google.com/spreadsheets/d/1V3Nyrw7mTsJdPFJffr0zSF1ZxTFNUxFDlV7vHrHW_-o/edit#gid=0";

projectId = "bunsters";

function myFunction() {
  var sheet = SpreadsheetApp.openByUrl(AMAZON_INVENTORY_SHEET).getSheetByName("Inventory");
  var Header = sheet.getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn()).getValues();
  var Dates = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()[0];
  var lastColumn = 2;//sheet.getLastColumn() - 1;

  var listItems = {};
  for (var i = 2; i <= lastColumn; i++) {
    var uniqSku = [];
    //var listItems = {};
    Header.forEach(function (row) {
      if (row[1].indexOf('CA') == -1) {
        var key;
        if (row[0] == "Available") {
          key = "Available_qty";
        } else if (row[0] == "Reserved") {
          key = "Reserved_qty";
        } else if (row[0] == "Inbound") {
          key = "Inbound_qty";
        }

        if (uniqSku.indexOf(row[1]) == -1) {
          listItems[row[1]] = {};
          listItems[row[1]][key] = row[i];
          listItems[row[1]]["SKU"] = row[1];
          listItems[row[1]]["Timestamp"] = Dates[i];
          listItems[row[1]]["Warehoues"] = (row[1].indexOf("-US") > -1) ? "US-AMZ" : (row[1].indexOf("-AU") > -1) ? "AU-AMZ" : "CA-AMZ"
          uniqSku.push(row[1]);
        }
        else {
          listItems[row[1]][key] = row[i];
        }
      }
    });
  }

  var rows = [];
  for (var key in listItems) {
    rows.push(listItems[key]);
  }

  var datasetId = 'master';
  var tableId = 'inventory';

  var resource = {
    "rows": rows,
    "datasetId": datasetId,
    "tableId": tableId,
    "tableReference": {
      "projectId": projectId,
      "tableId": tableId,
      "datasetId": datasetId
    },
  };

  var queryResults = BigQuery.Jobs.insert(resource, projectId);
  var jobId = queryResults.jobReference.jobId;

  var sleepTimeMs = 500;
  while (!queryResults.jobComplete) {
    Utilities.sleep(sleepTimeMs);
    sleepTimeMs *= 2;
    queryResults = BigQuery.Jobs.getQueryResults(projectId, jobId);
  }
}
