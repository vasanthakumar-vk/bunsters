module.paths.push('/usr/local/lib/node_modules');

const axios = require('axios');

const accessKey = process.env.AWS_ACCESS_KEY_ID || '';
const accessSecret = process.env.AWS_SECRET_ACCESS_KEY || '';

var amazonMws = require('amazon-mws')(accessKey, accessSecret);

//For AU
var amazonMwsAU = require('amazon-mws')('', '');
amazonMwsAU.setHost('mws.amazonservices.com.au');

var SellerIds = {
    CA: "",
    US: "",
    AU: ""
};

var getIncoming = async function (status, MarketplaceId) {
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 2);
    var fulfillmentInventory = {
        'Version': '2010-10-01',
        'Action': 'ListInboundShipments',
        'SellerId': SellerIds[MarketplaceId[1]],
        'MarketplaceId': MarketplaceId[0],
        'ShipmentStatusList.member.1': status,
        'LastUpdatedAfter': yesterday,
        'LastUpdatedBefore': new Date()
    }
    let response;
    if (MarketplaceId[1] == "AU") {
        response = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory);
    } else {
        response = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory);
    }

    let shipments = [];
    if (response.ShipmentData && response.ShipmentData.member) {
        shipments = shipments.concat(response.ShipmentData.member);
    }
    while (response.NextToken) {
        var fulfillmentInventory2 = {
            'Version': '2010-10-01',
            'Action': 'ListInboundShipmentsByNextToken',
            'SellerId': SellerIds[MarketplaceId[1]],
            'MarketplaceId': MarketplaceId[0],
            'NextToken': response.NextToken
        }

        if (MarketplaceId[1] == "AU") {
            response = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory2);
        } else {
            response = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory2);
        }

        if (response.ShipmentData && response.ShipmentData.member) {
            shipments = shipments.concat(response.ShipmentData.member);
        }
    }

    const items = [];
    for (const shipment of shipments) {
        var fulfillmentInventory3 = {
            'Version': '2010-10-01',
            'Action': 'ListInboundShipmentItems',
            'SellerId': SellerIds[MarketplaceId[1]],
            'MarketplaceId': MarketplaceId[0],
            'ShipmentId': shipment.ShipmentId
        }
        let itemsResponse;
        if (MarketplaceId[1] == "AU") {
            itemsResponse = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory3);
        } else {
            itemsResponse = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory3);
        }

        if (itemsResponse.ItemData.member && itemsResponse.ItemData.member.SellerSKU) {
            items.push({
                SellerSKU: itemsResponse.ItemData.member.SellerSKU,
                QuantityShipped: itemsResponse.ItemData.member.QuantityShipped
            });
        }

        while (itemsResponse.ItemData.NextToken) {
            var fulfillmentInventory4 = {
                'Version': '2010-10-01',
                'Action': 'ListInboundShipmentItemsByNextToken',
                'SellerId': SellerIds[MarketplaceId[1]],
                'MarketplaceId': MarketplaceId[0],
                'NextToken': itemsResponse.ItemData.NextToken
            };
            if (MarketplaceId[1] == "AU") {
                itemsResponse = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory4);
            } else {
                itemsResponse = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory4);
            }
            if (itemsResponse.ItemData.member && itemsResponse.ItemData.member.SellerSKU) {
                items.push({
                    SellerSKU: itemsResponse.ItemData.member.SellerSKU,
                    QuantityShipped: itemsResponse.ItemData.member.QuantityShipped,
                    QuantityReceived: itemsResponse.ItemData.member.QuantityReceived,
                });
            }
        }

    }

    let inbounds = items.map(item => ({
        sku: item["SellerSKU"],
        qty: (item["QuantityReceived"] && item["QuantityReceived"] > 0) ?
            item["QuantityReceived"] :
            item["QuantityShipped"],
        status: status
    }));

    return inbounds
}

var run = async function () {
    const states = ["WORKING", "SHIPPED", "IN_TRANSIT", "DELIVERED", "CHECKED_IN", "RECEIVING", "CLOSED", "CANCELLED", "DELETED", "ERROR"];
    var MarketplaceIdList = [["A39IBJ37TRP1C6", "AU"], ["A2EUQ1WTGCTBG2", "CA"], ["ATVPDKIKX0DER", "US"]];

    let items = [];
    for (const MarketplaceId of MarketplaceIdList) {
        for (const state of states) {
            const inbounds = await getIncoming(state, MarketplaceId);
            console.log(`Receiving - ${state} \n ${inbounds}`);
            items = items.concat(inbounds);
        }
    }

    items = items.reduce((agg, curr) => {
        var sku = curr["sku"];
        if (!agg[sku]) {
            agg[sku] = {};
        }
        agg[sku][curr["status"]] = curr["qty"];
        return agg;
    }, {})

    let res = Object.keys(items).map(item => {
        return states.reduce((agg, state) => {
            agg[state] = (items[item][state]) ? items[item][state] : "";
            return agg;
        }, { sku: item });
    });

    console.log(res);
    let postResponse = await axios.post(
        'https://script.google.com/macros/s/AKfycbx_ujssQv8Dv49LV-pcakkXnGlk2UUTSE1s0giOxpiXtMVk_FbL/exec',
        { data: res, inbound: true });
    console.log(postResponse.data);
    axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "amzInbound" });
}

run();