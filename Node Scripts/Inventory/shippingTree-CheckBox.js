const Url = 'https://script.google.com/macros/s/AKfycbx_ujssQv8Dv49LV-pcakkXnGlk2UUTSE1s0giOxpiXtMVk_FbL/exec';
const axios = require('axios');
const puppeteer = require('puppeteer');
const emailAddress = "";
const passWord = "";

(async function main() {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.goto('https://app.shippingtree.co/product/', { timeout: 0 });
    await page.type('input[name="username"]', emailAddress);
    await page.type('input[name="password"]', passWord);
    const button = await page.$('button.sc-htpNat.hMOLWg.royal');
    await button.click();
    await page.waitFor(1500);
    console.log(new Date() + ": Login Successfull");
    var item = [];
    var nextButtonNull = "";
    console.log("start loop to get data");
    do {
        await page.waitForSelector('table tbody tr td:nth-child(3)');
        const sections = await page.$$('table tbody tr td:nth-child(3)');
        let items = [];
        for (i = 1; i <= sections.length; i++) {
            let json = {};
            json.sku = await page.$eval(`table tbody tr:nth-child(${i}) td:nth-child(3)`, a => a.innerText);
            json.link = await page.$eval(`table tbody tr:nth-child(${i}) td:nth-child(2) a`, a => a.href);
            items.push(json);
        }
        var res = await bundleStatus(items, browser);
        res.forEach(function (row) {
            item.push(row);
        });
        nextButtonNull = await page.$('button[title="Next"][disabled]');
        if (nextButtonNull == null) {
            await page.click('button[title="Next"]');
            await page.waitForSelector('table tbody tr td:nth-child(3)');
        }
    } while (nextButtonNull == null);
    console.log("loop end");
    console.log(new Date() + ": " + item.length + " Skus are found");
    if (item.length > 0) {
        await axios.post(Url, { skuBundleStatus: item });
        console.log("data Send to sheet");
        await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Shipping Checkbox" });
    }
    await browser.close();
})();

async function bundleStatus(links, browser) {
    const promises = [];
    const items = [];
    for (let k = 0; k < links.length; k++) {
        promises.push(browser.newPage().then(async page => {
            const link = links[k].link;
            await page.goto(link, { timeout: 0 });
            await page.waitForSelector('.form-group div:nth-child(10) label span');
            await page.waitFor(2000);
            const checkedOrNot = await page.$eval('.form-group div:nth-child(10) label span', a => a.className);
            console.log(checkedOrNot);
            items.push({
                "sku": links[k].sku,
                "Checked": checkedOrNot == "active"
            });
            await page.close();
        }));
    }
    await Promise.all(promises);
    return items;
}