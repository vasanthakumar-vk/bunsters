const Url = 'https://script.google.com/macros/s/AKfycbx_ujssQv8Dv49LV-pcakkXnGlk2UUTSE1s0giOxpiXtMVk_FbL/exec';
const { BigQuery } = require('@google-cloud/bigquery');
const util = require('util');
const axios = require('axios');
const puppeteer = require('puppeteer');
const shippingtreeConfig = require('./shippingtree_config.json');
const emailAddress = shippingtreeConfig.emailAddress;
const passWord = shippingtreeConfig.passWord;

(async function main() {
    const browser = await puppeteer.launch({ headless: true });
    var item = [];
    try {
        const page = await browser.newPage();
        await page.goto('https://app.shippingtree.co/login/', { timeout: 0 });
        await page.type('input[name="username"]', emailAddress);
        await page.type('input[name="password"]', passWord);
        const button = await page.$('button.sc-htpNat.hMOLWg.royal');
        await button.click();
        await page.waitFor(1500);
        console.log(new Date() + ": Login Successfull");
        await page.goto('https://app.shippingtree.co/product/', { waitUntil: 'domcontentloaded' });
        var nextButtonNull;
        console.log("start loop to get data");
        do {
            await page.waitForSelector('table tbody tr td:nth-child(3)');
            const sections = await page.$$('table tbody tr td:nth-child(3)');
            for (i = 1; i <= sections.length; i++) {
                let json = {};
                json.sku = await page.$eval(`table tbody tr:nth-child(${i}) td:nth-child(3)`, a => a.innerText);
                json.inStock = await page.$eval(`table tbody tr:nth-child(${i}) td:nth-child(8)`, a => a.innerText);
                json.inBound = await page.$eval(`table tbody tr:nth-child(${i}) td:nth-child(9)`, a => a.innerText);
                item.push([json]);
            }
            nextButtonNull = await page.$('button[title="Next"][disabled]');
            if (nextButtonNull == null) {
                await page.click('button[title="Next"]');
                await page.waitForSelector('table tbody tr td:nth-child(3)');
            }
        } while (nextButtonNull == null);
        console.log("loop end");

        var len = item.length;
        console.log(new Date() + ": " + len + " Skus are found");

        //send bigquery
        var now = new Date();
        var rows = item.map(function (row) {
            var vals = row[0];
            return {
                SKU: vals.sku,
                Available_qty: vals.inStock,
                Timestamp: now.getTime() / 1000,
                Warehouse: "US-1",
                Date: now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate()
            };
        });
        await sendToBigQuery(rows);
        await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Shipping Inventory - BQ" });

        //send sheet
        await axios.post(Url, { ShippingStocks: item });
        console.log(new Date() + ": stored in google sheet");
        console.log(new Date() + ": updated filfillment status");

        var response = await axios.get(Url + '?action=shippingTree');
        var reOrderRequest = response.data;
        console.log({ "Received Out of Stock items": reOrderRequest });
        await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Shipping Inventory" });
        await browser.close();
    } catch (e) {
        await browser.close();
        console.log(e);
    }
})();

async function sendToBigQuery(rows) {
    const bigquery = new BigQuery();
    var datasetId = "master";
    var tableId = "Inventory";
    await bigquery.dataset(datasetId).table(tableId).insert(rows).then(function (data) {
        console.log("\n API Response ", util.inspect(data[0], false, null, true));
        console.log("\nStored to Bigquery Table and updated filfillment status");
    }).catch(function (err) {
        console.log("\n API Error " + err.name);
        console.log("inventory: ", util.inspect(err, false, null, true));
    });
}