var Url = "https://script.google.com/macros/s/AKfycbx_ujssQv8Dv49LV-pcakkXnGlk2UUTSE1s0giOxpiXtMVk_FbL/exec";
const { BigQuery } = require('@google-cloud/bigquery');
const util = require('util');
const puppeteer = require('puppeteer');
const axios = require('axios');
const pikpakConfig = require('./pikpak_config.json');
const emailAddress = pikpakConfig.emailAddress;
const passWord = pikpakConfig.passWord;
const ZapURL = "https://hooks.zapier.com/hooks/catch/2337419/v6bmqf/";

(async function main() {
    const browser = await puppeteer.launch({ headless: true });
    try {

        const page = await browser.newPage();
        await page.goto('https://app3.smartturn.com/', { timeout: 0 });
        await page.type('#EmailAddress', emailAddress, { delay: 10 });
        await page.type('#Password', passWord);
        const button = await page.$('button.btn');
        await button.click();
        console.log(new Date() + ": Login Successfull");
        await page.waitForSelector('ul.sidebar-menu');
        await page.goto('https://app3.smartturn.com/AvailableToPromise/Index', { waitUntil: "networkidle0" });
        await page.waitForSelector('#AvailableToPromiseListPage');
        var item = [];
        var availabletopromise = [];
        var merge = [];
        var table = [];
        var pageInfo = [];
        var pageNumCount = 1;
        //await fs.writeFile('out.csv', 'Item,Available to promise\n');
        do {
            const sections = await page.$$('#AvailableToPromiseListPage tbody tr td:nth-child(2)');
            for (i = 0; i < sections.length; i++) {
                const td = sections[i];
                item[i] = await td.$eval('a', a => a.innerText);
            }
            const sections2 = await page.$$('#AvailableToPromiseListPage tbody tr td:nth-child(10)');
            for (i = 0; i < sections2.length; i++) {
                const td = sections2[i];
                availabletopromise[i] = await td.$eval('span', a => a.innerText.replace('ea', "").trim());
            }
            for (i = 0; i < sections.length; i++) {
                merge[i] = [item[i], availabletopromise[i]];
                //await fs.appendFile('out.csv', `"${item[i]}","${availabletopromise[i]}"\n`);
            }

            for (i = 0; i < sections.length; i++) {
                table.push(merge[i]);
            }

            var disabledBtn = await page.$('li.paginate_button.next.disabled');
            if (disabledBtn === null) {
                await page.waitForSelector('ul.pagination');
                do {
                    const buttonForNext = await page.$('li.paginate_button.next');
                    await buttonForNext.click();
                    await page.waitFor(5000);
                    var info = await page.$eval('#AvailableToPromiseListPage_info', a => a.innerText);
                    console.log(info);
                    if (pageInfo.indexOf(info) == -1) {
                        pageInfo.push(info);
                        break;
                    }
                } while (1)
                pageNumCount++;
                console.log("Clicked Page " + pageNumCount);
                await page.waitFor(5000);
            }
            else {
                break;
            }
        } while (1);

        var tableRange = table.length + 1;
        var x = 5;
        console.log(new Date() + ": " + tableRange + " Skus and qty are found");

        //send Bigquery
        var now = new Date();
        var rows = table.map(function (row) {
            return {
                SKU: row[0],
                Available_qty: Number(row[1].replace(",", "")),
                Timestamp: now.getTime() / 1000,
                Warehouse: "AU-1",
                Date: now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate()
            };
        });
        await sendToBigQuery(rows);
        await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Picpak Inventry - BQ" });

        //send sheet
        await axios.post(Url, { stock: table });
        console.log(new Date() + ": stored in google sheet");
        console.log(new Date() + ": updated filfillment status");
        await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Picpak Inventry" });

        //create SO
        var response = await axios.get(Url + '?action=smartturn');
        var qty = response.data;
        console.log({ "Received Out of Stock items": response.data });
        if (!qty.map) {
            throw "Invalid response";
        }
        if (qty.length > 0) {
            //await page.waitForNavigation();
            for (i = 0; i < qty.length; i++) {
                var sku = qty[i].sku;
                var reOrder = qty[i].reorderQty;
                await page.goto('https://app3.smartturn.com/SalesOrders/EditSO', { waitUntil: "networkidle0" });
                await page.type('#Priority', 'REW', { delay: 1 });
                await page.type('#Owner', 'Bunsters');
                await page.waitForSelector('a.ui-corner-all');
                await page.click('a.ui-corner-all');
                await page.waitFor(4000);
                await page.click('.table-links-group.clearfix a.add-btn');

                await page.waitForSelector('#ItemNumber');
                await page.type('#ItemNumber', 'Product Rework');
                const searchBtn = await page.$('button.btn.btn-default2');
                await page.waitFor(2000);
                await searchBtn.click();
                await page.waitForSelector('td.dt-body-left.noprint');
                await page.click('td.dt-body-left.noprint input.property');
                await page.click('#btnSave');
                await page.waitForSelector('#SalesOrders_ProfilePage_ItemGrid0-6');
                await page.type('#SalesOrders_ProfilePage_ItemGrid0-6', '1');
                await page.waitFor(2000);
                await page.click('.nav.nav-tabs li:nth-child(3)');
                await page.waitForSelector('#Comments');

                var mergjson = {};
                mergjson.sku = reOrder + " X " + sku;
                mergjson.componets = qty[i].component;
                mergjson.instruction = qty[i].instruction;

                var merg2 = "Create : " + reOrder + " X " + sku + "\n";
                merg2 += "Instructions to create project & SKU quantities used in this project : emailed to service@pikpak.com.au";
                console.log(new Date() + ": " + merg2);
                await page.evaluate((merg2) => {
                    document.querySelector('#Comments').value = merg2;
                }, merg2);

                await page.click('#btnSave');
                await page.waitFor(5000);
                var orderId;
                try {
                    orderId = await page.$eval('section.content-header.clearfix li.active', active => active.innerText.split("#:")[1]);
                } catch (e) {
                    try {
                        var errorAlert = await page.$(".divAlert");
                        if (errorAlert) {
                            var errorAlertText = await page.$eval('.divAlert', a => a.innerText);
                            console.log(errorAlertText);
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }
                if (orderId) {
                    var orderDetails = [];
                    orderDetails = [qty[i].sku, qty[i].date, qty[i].currentQty, qty[i].reorderQty, qty[i].fulFillment, orderId];
                    await axios.post(Url, { so: orderDetails });
                    console.log(new Date() + ": ordered successfully.., Sales Order No" + orderId);
                    var arrayOfEmail = [{ SKU: qty[i].sku, reorder_quantity: qty[i].reorderQty, current_Qty: qty[i].reorderQty + qty[i].currentQty, viewSO: orderId, SO: orderId.replace(/[^0-9\.]/g, '') }];
                    // axios.post(ZapURL, arrayOfEmail).then(orderDetail => {
                    //     console.log("Email sent!");
                    // });
                    await axios.post(Url, { comments: mergjson, orderId: orderId });
                } else {
                    try {
                        var errorAlert = await page.$(".divAlert");
                        if (errorAlert) {
                            var errorAlertText = await page.$eval('.divAlert', a => a.innerText);
                            console.log(errorAlertText);
                        }
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        }
        await browser.close();
    } catch (e) {
        await browser.close();
        console.log(e);
    }
})();

async function sendToBigQuery(rows) {
    const bigquery = new BigQuery();
    var datasetId = "master";
    var tableId = "Inventory";
    console.log(rows);
    await bigquery.dataset(datasetId).table(tableId).insert(rows).then(function (data) {
        console.log("\n API Response ", util.inspect(data[0], false, null, true));
        console.log("\nStored to Bigquery Table and updated filfillment status");
    }).catch(function (err) {
        console.log("\n API Error " + err.name);
        console.log("inventory: ", util.inspect(err, false, null, true));
    });
}