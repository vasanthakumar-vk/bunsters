const Url = 'https://script.google.com/a/bunstersworldwide.com/macros/s/AKfycbwNL3OTrywB1PP-Cvpken4BJ4zJNG3DZD3-3cmrJV2DjgHJD_r5/exec';
const puppeteer = require('puppeteer');
const axios = require('axios');
const emailAddress = "";
const passWord = "";

(async function main() {
    const browser = await puppeteer.launch({ headless: true });
    var item = [];
    try {
        const page = await browser.newPage();
        await page.goto('https://app.shippingtree.co/login/', { timeout: 90000 });
        await page.type('input[name="username"]', emailAddress, { delay: 10 });
        await page.type('input[name="password"]', passWord, { delay: 10 });
        const button = await page.$('button.sc-htpNat.hMOLWg.royal');
        await button.click();
        await page.waitFor(1500);
        console.log(new Date() + ": Login Successfull");
        await page.goto('https://app.shippingtree.co/order/', { waitUntil: 'domcontentloaded' });
        await page.waitForSelector('table tbody tr td:nth-child(3)');

        var nextButtonNull = "";
        var array = [];
        var sections = [];
        var flag = false;
        const page2 = await browser.newPage();
        await page2.goto('https://app.shippingtree.co/order/?status=200', { waitUntil: 'domcontentloaded' });
        await page2.waitForSelector('table tbody tr td:nth-child(3)');
        do {
            if (!flag) {
                await page2.click('.datatable__dropdown');
                await page2.click('.datatable__dropdown ul li:nth-child(3)');
                await page2.waitForSelector('table tbody tr td:nth-child(3)');
                flag = true;
            }
            let response = await page2.$$eval('.sc-gzVnrw.lmWoWP table tbody td:nth-child(1) a', els => els.map(el => el.href));
            for (var key of response) {
                sections.push(key);
            }
            nextButtonNull = await page2.$('button[title="Next"][disabled]');
            if (nextButtonNull == null) {
                await page2.click('button[title="Next"]');
                await page2.waitForSelector('table tbody tr td:nth-child(3)');
            }
        } while (nextButtonNull == null);
        await page2.close();

        flag = false;
        const page3 = await browser.newPage();
        await page3.goto('https://app.shippingtree.co/order/?status=201', { waitUntil: 'domcontentloaded' });
        await page3.waitForSelector('table tbody tr td:nth-child(3)');
        do {
            if (!flag) {
                await page3.click('.datatable__dropdown');
                await page3.click('.datatable__dropdown ul li:nth-child(3)');
                await page3.waitForSelector('table tbody tr td:nth-child(3)');
                flag = true;
            }
            let response = await page3.$$eval('.sc-gzVnrw.lmWoWP table tbody td:nth-child(1) a', els => els.map(el => el.href));
            for (var key of response) {
                sections.push(key);
            }
            nextButtonNull = await page3.$('button[title="Next"][disabled]');
            if (nextButtonNull == null) {
                await page3.click('button[title="Next"]');
                await page3.waitForSelector('table tbody tr td:nth-child(3)');
            }
        } while (nextButtonNull == null);

        var position = 0;
        var factor = 25;
        do {
            var datas = await gatherData(browser, sections.slice(position, (factor + position)));
            position = position + factor;
            datas.forEach(function (row) {
                array.push(row);
            });
            let pages = await browser.pages();
            await pages.forEach(function (row, index) {
                if (index >= 2) {
                    pages[index].close();
                }
            });
        } while (position <= sections.length);
        console.log(array.length + ": Orders are found");
        console.log(new Date() + ": stored in google sheet");
        await axios.post(Url, { ShippingOrders: array });
        await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Shipping Order" });
        await browser.close();
        console.log('finish');
    } catch (e) {
        console.log(e);
        await browser.close();
    }
})();

async function gatherData(browser, list) {
    const promises = [];
    const items = [];
    for (let k = 0; k < list.length; k++) {
        promises.push(browser.newPage().then(async page => {
            const link = list[k];
            try {
                const page = await browser.newPage();
                await page.goto(link, { timeout: 30000 });
                console.log("eee")
                // await page.waitForSelector('.sc-ifAKCX.hBINv td');
                // console.log("eee23e")
                var orderNum = await page.$eval('.order-section h3 span', a => a.innerText);
                console.log("eee1234")

                var shopifyID = "";
                var totalCost = "";
                var divCount = await page.$$('.order-section:first-child div');
                for (var i = 1; i <= divCount.length; i++) {
                    var innerText = await page.$eval(`.order-section:first-child div:nth-child(${i + 1})`, a => a.innerText);
                    if (innerText.toString().indexOf("Total Charges") > -1) {
                        totalCost = await page.$eval(`.order-section:first-child div:nth-child(${i + 1}) span`, a => a.innerText);
                    } else if (innerText.indexOf("Remote Number") > -1) {
                        shopifyID = await page.$eval(`.order-section:first-child div:nth-child(${i + 1}) span`, a => a.innerText);
                    }
                }
                items.push([orderNum, shopifyID, totalCost]);
                await page.close();
            } catch (e) {
                console.log(e);
                console.log("Failed scrapping - " + link);
            }
        }));

    }
    await Promise.all(promises);
    console.log(items)
    return items;
}