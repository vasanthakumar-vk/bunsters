var Url = 'https://script.google.com/a/bunstersworldwide.com/macros/s/AKfycbwNL3OTrywB1PP-Cvpken4BJ4zJNG3DZD3-3cmrJV2DjgHJD_r5/exec';
const puppeteer = require('puppeteer');
const axios = require('axios');
const emailAddress = "";
const passWord = "";

(async function main() {
    const browser = await puppeteer.launch();
    try {
        const page = await browser.newPage();
        await page.setViewport({ width: 1000, height: 897, deviceScaleFactor: 1 })
        await page.goto('https://app3.smartturn.com/', { timeout: 0 });
        await page.type('#EmailAddress', emailAddress, { delay: 10 });
        await page.type('#Password', passWord);
        const button = await page.$('button.btn');
        await button.click();
        console.log(new Date() + ": Login Successfull");
        await page.waitForSelector('ul.sidebar-menu');
        await page.goto('https://app3.smartturn.com/SalesOrders', { waitUntil: "networkidle0" });
        var scrapeData = []
        do {
            await page.waitForSelector('#SalesOrdersListPage');
            var rowData = await page.$$('#SalesOrdersListPage tbody tr');
            for (var i = 1; i < rowData.length; i++) {
                var so = await page.$eval(`#SalesOrdersListPage tbody tr:nth-child(${i}) td:nth-child(1)`, a => a.innerText)
                var external = await page.$eval(`#SalesOrdersListPage tbody tr:nth-child(${i}) td:nth-child(12)`, a => a.innerText)
                scrapeData.push([so, external, ""])
            }
            var disabledBtn = await page.$('li.paginate_button.next.disabled');
            if (disabledBtn === null) {
                await page.waitForSelector('ul.pagination');
                await page.waitFor(2000);
                const buttonForNext = await page.$('li.paginate_button.next');
                await buttonForNext.click();
                await page.waitFor(500);
            }
            else {
                break;
            }
        } while (1);
        console.log(new Date() + ": " + scrapeData.length + "item are found")
        await axios.post(Url, { "scrapeData": scrapeData });
        console.log(new Date() + ": Store in google sheet...")
        await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Picpak Order" });
        await browser.close();
    } catch (e) {
        await browser.close();
        console.log(e);
    }
})();
