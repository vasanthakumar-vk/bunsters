const URL = "https://script.google.com/macros/s/AKfycbwzplxpll5_2PtkAhwWyn3ttiNSd95DPmdRgutEPodjauzN_DRW/exec";
const ZapURL = "";

const puppeteer = require('puppeteer');
const axios = require('axios');

(async () => {
    console.log("Starting script " + new Date());
    const list = (await axios.get(URL)).data;
    if(list.length < 1) {
        console.log("No entries to process");
        return;
    }
    console.log({listToProcess: list});

    const browser = await puppeteer.launch({ headless: false});
    const page = await browser.newPage();
    page.setViewport({ width: 1200, height: 800 });

    await page.goto("https://app3.smartturn.com", { waitUntil: "networkidle2" });

    //Login
    await page.type('#EmailAddress', "");
    await page.type('#Password', "");
    await Promise.all([
        page.click('button[type="submit"]'),
        page.waitForNavigation({ waitUntil: 'networkidle0' })
    ]);

    const status = [];

    for (const row of list) {
        await page.goto("https://app3.smartturn.com/SalesOrders/EditSO", {
            waitUntil: "networkidle0"
        });
        console.log("Loaded Sales Orders Page");

        await page.type('#Priority', "PU");
        await page.type('#Owner', "Bunsters Fresh::BUN");
        
        await Promise.all([
            page.evaluate(() => {
                document.querySelector('#lnkLookupCustomer').click();
            }),
            page.waitForNavigation({ waitUntil: "networkidle0" })
        ]);

        await page.type('#Name', "G&K Fine Foods - PICKUP c/o James Nulliah");
        await Promise.all([
            page.click("#btnSearch"),
            page.waitForNavigation({ waitUntil: "networkidle0" })
        ]);
        await page.click("#BusinessAffilatesGridProfilePage0-0");
        await Promise.all([
            page.click("#btnOK"),
            page.waitForNavigation({ waitUntil: "networkidle0" })
        ]);

        for (const item of row.items) {
            console.log("Adding item " + item.name);

            await Promise.all([
                page.click('a[actionname="SalesOrders_ProfilePage_ItemGrid_Add"]'),
                page.waitForNavigation({ waitUntil: "networkidle0" })
            ]);

            await page.type("#ItemNumber", item.name);
            await Promise.all([
                page.click("#btnSearch"),
                page.waitForNavigation({ waitUntil: "networkidle0" })
            ]);
            await page.click("#ItemMasterGridviewId0-0");
            await Promise.all([
                page.click("#btnSave"),
                page.waitForNavigation({ waitUntil: "networkidle0" })
            ]);
        }

        console.log("Adding quantities ");
        for (let index = 0; index < row.items.length; index++) {
            const item = row.items[index];
            await page.type(`#SalesOrders_ProfilePage_ItemGrid${index}-6`, item.qty.toString())
        }
        console.log("Added quantities");

        await Promise.all([
            page.click("#btnSave"),
            page.waitForNavigation({ waitUntil: "networkidle0" })
        ]);

        try {
           await page.waitForNavigation();
        } catch(e) {
            console.log("Waited some time..");
        }

        console.log("Getting SO number");
        const so = await page.$eval(".content-header li.active", el => el.textContent);

        status.push({rowNum: row.rowNum, so: so.split("#:")[1].trim()});
        console.log({status: status});
    }

    await browser.close();

    axios.post(URL, status).then(data => {
        console.log("Posted Status to sheet");

        status.forEach(state => {
            sendEmail(state.rowNum);
        });
    })
})();

function sendEmail(rowNum) {
    axios.get(URL + "?getDetails=true&rowNum=" + rowNum).then(data => {
        const row = data.data[0];
        var table = "";
        var index = 1;
        for (const item of row.items) {
            table += `<tr>
                <td style="border:1px solid #737373; padding: .3em;">${index}.</td>
                <td style="border:1px solid #737373; padding: .3em;">${item.name}</td>
                <td style="border:1px solid #737373; padding: .3em;">${item.qty}</td></tr>`;
            index++;
        }

        const emailBody = {
            "invoiceID": row.invoice,
            "so": row.so,
            "poNumber": row.po,
            "orderDetails": `<table style=\"border:1px solid #737373; border-collapse: collapse\" >${table}</table>`
        };
        axios.post(ZapURL, emailBody).then(data => {
            console.log("Email sent!");
        });
    });
}