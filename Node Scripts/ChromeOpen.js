const puppeteer = require('puppeteer');
(async function main() {
    try {
        const browser = await puppeteer.launch({
            executablePath: 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe',
            userDataDir: 'C:/Users/Administrator/AppData/Local/Google/Chrome/User Data/',
            headless: false
        });
        const page = await browser.newPage();
        await page.goto("https://www.amazon.com/");
    } catch (e) {
        console.log(e);
    }
})();