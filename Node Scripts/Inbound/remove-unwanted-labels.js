const fs = require('fs');

(async function run() {

    fs.readdir("C:\\Users\\Administrator\\node_scripts\\Inbound/", (err, files) => {
        files.forEach(file => {
            if ((file.indexOf("labels") > -1 || file.indexOf("unlink") > -1) && file.indexOf("remove") == -1) {
                fs.unlink("C:\\Users\\Administrator\\node_scripts\\Inbound/" + file, (err) => {
                    if (err) {
                        console.log("failed to delete local image:" + err);
                    } else {
                        console.log('successfully deleted');
                    }
                });
            }
        });
    });

})();