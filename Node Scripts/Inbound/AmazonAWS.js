const { compose } = require("underscore");

var request = require("request")
    , crypto = require("crypto")
    , querystring = require("querystring")
    , assert = require("assert")
    , parseString = require("xml2js").parseString
    ;

var amazonMws = require('amazon-mws')('', '');
var amazonMwsAU = require('amazon-mws')('', '');
amazonMwsAU.setHost('mws.amazonservices.com.au');

var SellerIds = {
    CA: "",
    US: "",
    AU: ""
};

var MarketplaceIds = {
    CA: "",
    US: "",
    AU: ""
};

function Client(options) {
    var requiredKeys = [
        "AWSAccessKeyId",
        "SellerId",
        "Secret"
    ];
    for (var i = 0, len = requiredKeys.length; i < len; i++) {
        var key = requiredKeys[i];
        this[key] = options[key];
        assert(this[key], "options[\"" + key + "\"] required");
    }
}

function hmacSHA256(method, domain, path, secret, payload) {
    var hmac = crypto.createHmac("sha256", secret);
    hmac.update(method + "\n");
    hmac.update(domain + "\n");
    hmac.update(path + "\n");

    var keys = Object.keys(payload).sort();
    var qs = '';
    for (var i = 0, len = keys.length; i < len; i++) {
        qs += keys[i] + "=" + querystring.escape(payload[keys[i]]);
        if (i !== (len - 1)) {
            qs += "&";
        }
    }
    hmac.update(qs);

    return hmac.digest('base64');
}

// http://docs.developer.amazonservices.com/en_US/orders/2013-09-01/Orders_ListOrders.html
Client.prototype.ListOrders = function (cond, Host, callback) {
    this.invoke("POST",
        "ListOrders",
        "2013-09-01",
        "/Orders/2013-09-01",
        cond,
        Host,
        function (responseJSON) {
            var orders = responseJSON.ListOrdersResponse.ListOrdersResult[0].Orders[0].Order
                , items = 0
                , amount = 0;
            if (!orders) {
                return {
                    orders: 0,
                    items: 0,
                    amount: 0
                };
            }
            for (var i = 0, len = orders.length; i < len; i++) {
                var order = orders[i];
                items += parseInt(order.NumberOfItemsUnshipped[0], 10) +
                    parseInt(order.NumberOfItemsShipped[0], 10);
                amount += (order.OrderTotal ? parseInt(order.OrderTotal[0].Amount[0], 10)
                    : 0);
            }
            return {
                orders: orders.length,
                items: items,
                amount: amount
            };
        },
        callback);
};

// http://docs.developer.amazonservices.com/en_US/fba_inventory/FBAInventory_ListInventorySupply.html
Client.prototype.ListInventorySupply = function (cond, Host, callback) {
    this.invoke("GET",
        "ListInventorySupply",
        "2010-10-01",
        "/FulfillmentInventory/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var member = responseJSON.ListInventorySupplyResponse.ListInventorySupplyResult[0].InventorySupplyList[0].member[0];
            return {
                InStockSupplyQuantity: parseInt(member.InStockSupplyQuantity[0], 10),
                TotalSupplyQuantity: parseInt(member.TotalSupplyQuantity[0], 10),
            };
        },
        callback);
};

Client.prototype.CreateInboundShipmentPlan = function (cond, Host, callback) {
    this.invoke("POST",
        "CreateInboundShipmentPlan",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var result = responseJSON;
            return {
                response: result
            };
        },
        callback);
};

Client.prototype.CreateInboundShipment = function (cond, Host, callback) {
    this.invoke("POST",
        "CreateInboundShipment",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};


Client.prototype.updateInboundShipment = function (cond, Host, callback) {
    this.invoke("POST",
        "UpdateInboundShipment",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.ListInboundShipments = function (cond, Host, callback) {
    this.invoke("POST",
        "ListInboundShipmentItems",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.GetPrepInstructionsForSKU = function (cond, Host, callback) {
    this.invoke("POST",
        "GetPrepInstructionsForSKU",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.estimateTransportRequest = function (cond, Host, callback) {
    this.invoke("POST",
        "EstimateTransportRequest",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.putTransportContent = function (cond, Host, callback) {
    this.invoke("POST",
        "PutTransportContent",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.getTransportContent = function (cond, Host, callback) {
    this.invoke("POST",
        "GetTransportContent",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.confirmTransportRequest = function (cond, Host, callback) {
    this.invoke("POST",
        "ConfirmTransportRequest",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.voidTransportRequest = function (cond, Host, callback) {
    this.invoke("POST",
        "VoidTransportRequest",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.getUniquePackageLabels = function (cond, Host, callback) {
    this.invoke("POST",
        "GetUniquePackageLabels",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.getPalletLabels = function (cond, Host, callback) {
    this.invoke("POST",
        "GetPalletLabels",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.getPrepInstructionsForASIN = function (cond, Host, callback) {
    this.invoke("POST",
        "GetPrepInstructionsForASIN",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.getServiceStatus = function (cond, Host, callback) {
    this.invoke("POST",
        "GetServiceStatus",
        "2010-10-01",
        "/FulfillmentInboundShipment/2010-10-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.invoke = async function (method, action, version, path, cond, Host, parser, callback) {
    var timestamp = (new Date()).toISOString();
    var payload = {
        "Action": action,
        "AWSAccessKeyId": this["AWSAccessKeyId"],
        "SellerId": this["SellerId"],
        "SignatureMethod": "HmacSHA256",
        "SignatureVersion": "2",
        "Timestamp": timestamp,
        "Version": version
    };
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var signature = hmacSHA256(method.toUpperCase(),
        Host,
        path,
        this.Secret,
        payload);
    payload.Signature = signature;

    await request[method.toLowerCase()]({
        url: "https://" + Host + path,
        qs: payload
    }, function (err, response, body) {
        if (err || (response.statusCode !== 200)) {
            parseString(body, function (error, result) {
                if (error) {
                    console.log("error: ", error);
                    callback(error, 0);
                    return;
                }
                console.log("error: ", result);
                callback(parser(result), null);
            });
            return;
        }
        parseString(body, function (err, result) {
            if (err) {
                callback(err, 0);
                return;
            }
            callback(null, parser(result));
        });
    });
};

Client.prototype.getFeedSubmissionList = function (cond, Host, callback) {
    this.feed("POST",
        "getFeedSubmissionList",
        "2009-01-01",
        "/Feeds/2009-01-01",
        cond,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.feed = function (method, action, version, path, cond, Host, parser, callback) {
    var timestamp2 = (new Date()).toISOString();
    var payload = {
        "Action": action,
        "AWSAccessKeyId": this["AWSAccessKeyId"],
        "SellerId": this["SellerId"],
        "SignatureMethod": "HmacSHA256",
        "SignatureVersion": "2",
        "Timestamp": timestamp2,
        "Version": version,
    };
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var signature = hmacSHA256(method.toUpperCase(),
        Host,
        path,
        this.Secret,
        payload);
    payload.Signature = signature;
    request[method.toLowerCase()]({
        url: "https://" + Host + path,
        qs: payload
    }, function (err, response, body) {
        if (err || (response.statusCode !== 200)) {
            parseString(body, function (error, result) {
                if (error) {
                    console.log("error: ", error);
                    callback(error, 0);
                    return;
                }
                console.log("error: ", result);
                callback(parser(result), null);
            });
            return;
        }
        parseString(body, function (err, result) {
            if (err) {
                callback(err, 0);
                return;
            }
            callback(null, parser(result));
        });
    });
};

Client.prototype.submitFeed = function (cond, Buffers, Host, callback) {
    this.submitedFeed("POST",
        "SubmitFeed",
        "2009-01-01",
        "/Feeds/2009-01-01",
        cond,
        Buffers,
        Host,
        function (responseJSON) {
            var res = responseJSON;
            return {
                response: res
            };
        },
        callback);
};

Client.prototype.submitedFeed = function (method, action, version, path, cond, Buffers, Host, parser, callback) {
    var timestamp2 = (new Date()).toISOString();
    var payload = {
        "Action": action,
        "AWSAccessKeyId": this["AWSAccessKeyId"],
        "SellerId": this["SellerId"],
        "SignatureMethod": "HmacSHA256",
        "SignatureVersion": "2",
        "Timestamp": timestamp2,
        "Version": version,
    };
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var signature = hmacSHA256(method.toUpperCase(),
        Host,
        path,
        this.Secret,
        payload);
    payload.Signature = signature;
    request[method.toLowerCase()]({
        url: "https://" + Host + path,
        qs: payload,
        formData: { FeedContent: Buffers },
    }, function (err, response, body) {
        if (err || (response.statusCode !== 200)) {
            parseString(body, function (error, result) {
                if (error) {
                    console.log("error: ", error);
                    callback(error, 0);
                    return;
                }
                console.log("error: ", result);
                callback(parser(result), null);
            });
            return;
        }
        parseString(body, function (err, result) {
            if (err) {
                callback(err, 0);
                return;
            }
            callback(null, parser(result));
        });
    });
};

var getIncoming = async function (status, MarketplaceId) {
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 2);
    var fulfillmentInventory = {
        'Version': '2010-10-01',
        'Action': 'ListInboundShipments',
        'SellerId': SellerIds[MarketplaceId[1]],
        'MarketplaceId': MarketplaceId[0],
        'ShipmentStatusList.member.1': status,
        'LastUpdatedAfter': yesterday,
        'LastUpdatedBefore': new Date()
    }
    let response;
    if (MarketplaceId[1] == "AU") {
        response = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory);
    } else {
        response = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory);
    }

    let shipments = [];
    if (response.ShipmentData && response.ShipmentData.member) {
        shipments = shipments.concat(response.ShipmentData.member);
    }
    while (response.NextToken) {
        var fulfillmentInventory2 = {
            'Version': '2010-10-01',
            'Action': 'ListInboundShipmentsByNextToken',
            'SellerId': SellerIds[MarketplaceId[1]],
            'MarketplaceId': MarketplaceId[0],
            'NextToken': response.NextToken
        }
        if (MarketplaceId[1] == "AU") {
            response = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory2);
        } else {
            response = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory2);
        }
        if (response.ShipmentData && response.ShipmentData.member) {
            shipments = shipments.concat(response.ShipmentData.member);
        }
    }

    const items = [];
    for (const shipment of shipments) {
        var fulfillmentInventory3 = {
            'Version': '2010-10-01',
            'Action': 'ListInboundShipmentItems',
            'SellerId': SellerIds[MarketplaceId[1]],
            'MarketplaceId': MarketplaceId[0],
            'ShipmentId': shipment.ShipmentId
        }
        let itemsResponse;
        if (MarketplaceId[1] == "AU") {
            itemsResponse = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory3);
        } else {
            itemsResponse = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory3);
        }

        if (itemsResponse.ItemData.member && itemsResponse.ItemData.member.SellerSKU) {
            items.push({
                SellerSKU: itemsResponse.ItemData.member.SellerSKU,
                QuantityShipped: itemsResponse.ItemData.member.QuantityShipped
            });
        }

        while (itemsResponse.ItemData.NextToken) {
            var fulfillmentInventory4 = {
                'Version': '2010-10-01',
                'Action': 'ListInboundShipmentItemsByNextToken',
                'SellerId': SellerIds[MarketplaceId[1]],
                'MarketplaceId': MarketplaceId[0],
                'NextToken': itemsResponse.ItemData.NextToken
            }

            if (MarketplaceId[1] == "AU") {
                itemsResponse = await amazonMwsAU.fulfillmentInboundShipment.search(fulfillmentInventory4);
            } else {
                itemsResponse = await amazonMws.fulfillmentInboundShipment.search(fulfillmentInventory4);
            }
            if (itemsResponse.ItemData.member && itemsResponse.ItemData.member.SellerSKU) {
                items.push({
                    SellerSKU: itemsResponse.ItemData.member.SellerSKU,
                    QuantityShipped: itemsResponse.ItemData.member.QuantityShipped,
                    QuantityReceived: itemsResponse.ItemData.member.QuantityReceived,
                });
            }
        }

    }

    let inbounds = items.map(item => ({
        sku: item["SellerSKU"],
        qty: (item["QuantityReceived"] && item["QuantityReceived"] > 0) ?
            item["QuantityReceived"] :
            item["QuantityShipped"],
        status: status
    }));

    return inbounds;
};

async function getDiscardSku(condition) {
    console.log(condition)
    var MarketplaceIdList = [["A39IBJ37TRP1C6", "AU"], ["A2EUQ1WTGCTBG2", "CA"], ["ATVPDKIKX0DER", "US"]];
    let items = [];
    for (const MarketplaceId of MarketplaceIdList) {
        const inbounds = await getIncoming("WORKING", MarketplaceId);
        items = items.concat(inbounds);
    }
    items = items.reduce((agg, curr) => {
        var sku = curr["sku"];
        if (!agg[sku]) {
            agg[sku] = {};
        }
        agg[sku][curr["status"]] = curr["qty"];
        return agg;
    }, {});
    var discardSku = [];
    for (var sku in items) {
        discardSku.push(sku);
    }
    return discardSku;
}

async function getPrepInstructionsForSKU(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'GetPrepInstructionsForSKU',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload);

    var PrepInstructionList = [];
    if (response.SKUPrepInstructionsList.SKUPrepInstructions) {
        PrepInstructionList = response.SKUPrepInstructionsList.SKUPrepInstructions.PrepInstructionList.PrepInstruction;
        if (!Array.isArray(PrepInstructionList)) {
            PrepInstructionList = [PrepInstructionList];
        }
    }
    return PrepInstructionList;
}

async function createInboundShipmentPlan(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'CreateInboundShipmentPlan',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload);

    var plans = [];
    if (response.InboundShipmentPlans && response.InboundShipmentPlans.member) {
        plans = response.InboundShipmentPlans.member;
        if (!Array.isArray(plans)) {
            plans = [plans];
        }
    }
    return plans;
}

async function createInboundShipment(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'CreateInboundShipment',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload);
    return response;
}

async function PutTransportContent(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'PutTransportContent',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload);
    return response;
}

async function getUniquePackageLabels(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'GetUniquePackageLabels',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.search(payload)
    return response;
}

async function cartonContentsRequest(cond, id) {
    var payload = {
        'Version': '2009-01-01',
        'Action': 'SubmitFeed',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.feeds.submit(payload);
    return response;
}

async function getPalletLabels(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'GetPalletLabels',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.search(payload)
    inventory.response.GetPalletLabelsResponse.GetPalletLabelsResult[0].TransportDocument[0].PdfDocument[0]
    return response;
}

async function updateInboundShipment(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'UpdateInboundShipment',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload)
    return response;
}

async function voidTransportRequest(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'VoidTransportRequest',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload);
    return response;
}

async function confirmTransportRequest(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'ConfirmTransportRequest',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload);
    return response;
}
async function estimateTransportRequest(cond, id) {
    var payload = {
        'Version': '2010-10-01',
        'Action': 'EstimateTransportRequest',
        'SellerId': SellerIds[id],
        'MarketplaceId': MarketplaceIds[id],
    }
    for (var k in cond) {
        payload[k] = cond[k];
    }
    var client = amazonMws;
    if (id == "AU") {
        client = amazonMwsAU;
    }
    let response = await client.fulfillmentInboundShipment.submit(payload);
    return response;
}

module.exports = {
    Client, getDiscardSku, getUniquePackageLabels, PutTransportContent, getPrepInstructionsForSKU, createInboundShipmentPlan, createInboundShipment, voidTransportRequest, updateInboundShipment, getPalletLabels,
    confirmTransportRequest, estimateTransportRequest, cartonContentsRequest
};