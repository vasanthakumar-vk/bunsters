const Url = 'https://script.google.com/macros/s/AKfycbx_ujssQv8Dv49LV-pcakkXnGlk2UUTSE1s0giOxpiXtMVk_FbL/exec';
const puppeteer = require('puppeteer');
const axios = require('axios');
const { Client, getDiscardSku, getUniquePackageLabels, PutTransportContent, getPrepInstructionsForSKU, createInboundShipmentPlan, createInboundShipment, voidTransportRequest, updateInboundShipment,
    getPalletLabels, confirmTransportRequest, estimateTransportRequest, cartonContentsRequest } = require('./AmazonAWS.js');

const util = require('util');
const crypto = require("crypto");
const CryptoJS = require("crypto-js");
const mwsExtract = require('mws-extract-document');
const AdmZip = require('adm-zip');
const fs = require('fs-extra');
const xmlParser = require('xml-js');
const { google } = require('googleapis');
const readline = require('readline');

const promisify = util.promisify;
const getFileData = promisify(fs.readFile);
const fileRename = promisify(fs.rename);

const MailComposer = require('nodemailer/lib/mail-composer');
const { isArray } = require('lodash');
const credentials = "";
const SCRIPT_LOCATION = "C:\\Users\\Administrator\\node_scripts\\Inbound";
const TOKEN_PATH = SCRIPT_LOCATION + '/token.json';
const SCOPES = ['https://mail.google.com', 'https://www.googleapis.com/auth/gmail.readonly'];

const emailAddress = "";
const passWord = "";

const ShipFromAddressNameUS = {
    "ShipFromAddressName": "Bunsters c/o Shipping Tree",
    "ShipFromAddressL1": "12258 Orden Drive",
    "ShipFromAddressCity": "Santa Fe Springs",
    "ShipFromAddressState": "CA",
    "ShipFromAddressPostal": "90670-6326",
    "ShipFromAddressCountry": "US",
};

const ShipFromAddressNameAU = {
    "ShipFromAddressName": "Bunsters c/o Pikpak",
    "ShipFromAddressL1": "14 Awun Ct",
    "ShipFromAddressCity": "Springvale",
    "ShipFromAddressState": "VIC",
    "ShipFromAddressPostal": "3171",
    "ShipFromAddressCountry": "AU",
};

var clientForAu = new Client({
    "AWSAccessKeyId": '',
    "SellerId": "",
    "Secret": ''
});

var client = new Client({
    "AWSAccessKeyId": "",
    "SellerId": "",
    "Secret": ""
});

var Hosts = {
    CA: "mws.amazonservices.ca",
    US: "mws.amazonservices.com",
    AU: "mws.amazonservices.com.au"
};

var SellerIds = {
    CA: "",
    US: "",
    AU: ""
};

var MarketplaceIds = {
    CA: "",
    US: "",
    AU: ""
};

(async function main() {
    // const browser = await puppeteer.launch({ headless: false });
    // await loginallSites(browser);
    // var skuarr = ["BU-BUNDLE-35-AU", "BU-BUNDLE-19-AU"];
    // var skuqarr = [6, 3];
    // var fbaarr = ["FBA15CMYZJ3M", "FBA15CMZ07X2"];
    // var pdfarr = [["./FBA15CMYZJ3M-03e7-PackageLabels.pdf"], ["./FBA15CMZ07X2-ff75-PackageLabels.pdf"]];
    // for (var i = 0; i < 2; i++) {
    //     var page = await browser.newPage();
    //     var ShipmentId = fbaarr[i];
    //     var allDatas = {};
    //     allDatas.sku = skuarr[i];
    //     allDatas.skuQty = skuqarr[i];
    //     allDatas.addName = "MEL1";
    //     allDatas.addL1 = "29 National Drive";
    //     var pdfNameList = pdfarr[i];
    //     var id = await OutBoundShippmentAU(ShipmentId, allDatas, pdfNameList, page);
    //     console.log(id);
    // }

    var discardSku = await getDiscardSku("start");
    console.log("discardSku", discardSku);

    const browser = await puppeteer.launch({ headless: true });
    await loginallSites(browser);

    /*****  get Outof stock of Shipping tree   ********/
    var response = await axios.get(Url);
    var reOrderRequest = response.data;
    console.log(reOrderRequest);
    console.log({ "Received Out of Stock items": reOrderRequest.length });
    var basicDetails = await axios.get(Url + '?action=basicDetails');

    for (var i = 0; i < reOrderRequest.length; i++) {
        var newAllDatas = reOrderRequest[i];
        var CountryCode = newAllDatas.warehouse.split("-")[0].trim();
        var ShipmentId;

        if (discardSku.indexOf(newAllDatas.sku) > -1 || CountryCode == "CA") {
            continue;
        }
        try {
            // Get Label
            var labelJson = {
                'SellerSKUList.Id.1': newAllDatas.sku,
                "ShipToCountryCode": CountryCode
            };
            var PrepInstructions = await getPrepInstructionsForSKU(labelJson, CountryCode);
            console.log("get PrepInstructions");
            console.log(PrepInstructions);
            if (!PrepInstructions[0] || PrepInstructions[0] == "") {
                throw "Missing Prep Instructions";
            }

            //Create Inbound Plan
            var planJson = await getInboundShipmentPlanJSON(PrepInstructions, newAllDatas, CountryCode);
            console.log(planJson)
            var planResponse = await createInboundShipmentPlan(planJson, CountryCode);
            //console.log(util.inspect(planResponse, false, null, true));
            console.log("get Plan");

            // Spreate Plan
            var planCount = planResponse.length;
            for (var j = 0; j < planCount; j++) {
                var allDatas = JSON.parse(JSON.stringify(newAllDatas));
                var plan = planResponse[j];
                allDatas.addName = plan.ShipToAddress.Name;
                allDatas.addL1 = plan.ShipToAddress.AddressLine1;
                ShipmentId = plan.ShipmentId;
                try {
                    if (planCount > 1) {
                        var newQty = Number(plan.Items.member.Quantity);
                        var QuantityInCase = allDatas.QuantityInCase;
                        var CortonCount = newQty / QuantityInCase;
                        var palletCount = CortonCount / 12;

                        if (allDatas.parcel == "SP") {
                            allDatas.Qty = CortonCount;
                        } else {
                            if (palletCount >= 1 && palletCount % 1 == 0) {
                                var minPallet = allDatas.minPallet;
                                if (palletCount >= minPallet) {
                                    allDatas.parcel = "LTL";
                                    allDatas.Qty = palletCount;
                                } else {
                                    allDatas.parcel = "SP";
                                    allDatas.Qty = CortonCount;
                                    allDatas.shipmentByName = allDatas.shipmentByName.replace("PLT", "MC");
                                }
                            } else if (palletCount < 1) {
                                allDatas.parcel = "SP";
                                allDatas.Qty = CortonCount;
                                allDatas.shipmentByName = allDatas.shipmentByName.replace("PLT", "MC");
                            } else {
                                console.log("Skip This Item");
                                console.log(allDatas);
                                continue;
                            }
                        }
                        allDatas.skuQty = newQty;
                        allDatas.palletQty = palletCount;
                    }

                    //Create Inbound
                    var inboundjson = await getInboundShipmentJSON(plan, PrepInstructions, allDatas, CountryCode);
                    var inboundreponse = await createInboundShipment(inboundjson, CountryCode);


                    //Put Transport content
                    if (CountryCode != "AU") {
                        await new Promise(resolve => setTimeout(resolve, 10000));
                        var transportJson = await getTransportContentJson(ShipmentId, allDatas, basicDetails, CountryCode);
                        console.log(transportJson)
                        var putTransportContentResponse = await PutTransportContent(transportJson, CountryCode);
                        console.log("putTransportContentResponse is done");
                        await new Promise(resolve => setTimeout(resolve, 20000));
                        var estimateResponse = await estimateTransportRequest({ "ShipmentId": ShipmentId, }, CountryCode);
                        console.log("estimateResponse is done");
                        await new Promise(resolve => setTimeout(resolve, 30000));
                        var conconfirmResponse = await confirmTransportRequest({ "ShipmentId": ShipmentId, }, CountryCode);
                        console.log("conconfirmResponse is done");
                    }

                    //Create carton Contents Request
                    await new Promise(resolve => setTimeout(resolve, 20000));
                    var CartonId = await createCartonContentsRequest(ShipmentId, allDatas, CountryCode);

                    //Get PDF
                    var palletPdfs;
                    if (allDatas.palletQty >= 1) {
                        await new Promise(resolve => setTimeout(resolve, 10000));
                        palletPdfs = await getPalletLabelPdfs(ShipmentId, allDatas, CountryCode);
                    }
                    await new Promise(resolve => setTimeout(resolve, 10000));
                    var pdfNameList = await getUniquePackageLabelPdfs(ShipmentId, CartonId, palletPdfs, CountryCode);
                    console.log(pdfNameList);
                    if (pdfNameList.length > 0) {
                        //Create Outbound
                        console.log("all data are ready...");
                        var page = await browser.newPage();
                        var orderId;
                        if (CountryCode == "AU") {
                            orderId = await OutBoundShippmentAU(ShipmentId, allDatas, pdfNameList, page);
                        } else {
                            orderId = await OutBoundShippment(ShipmentId, allDatas, pdfNameList, page);
                        }
                        await OutBoundShippmentConfirmMail(ShipmentId, allDatas, orderId, CountryCode);
                    }
                } catch (e) {
                    await errmailsend(e, allDatas, ShipmentId);
                }
            }
        } catch (e) {
            await errmailsend(e, newAllDatas, ShipmentId);
        }
    }
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "createAmazonInbound" });
})();

async function loginallSites(browser) {
    const page = await browser.newPage();
    try {
        await page.goto('https://app.shippingtree.co/login/', { waitUntil: 'domcontentloaded' });
    } catch (e) {
        await page.goto('https://app.shippingtree.co/login/', { timeout: 90000 });
    }
    await page.type('input[name="username"]', emailAddress, { delay: 10 });
    await page.type('input[name="password"]', passWord, { delay: 10 });
    const button = await page.$('button.sc-htpNat.hMOLWg.royal');
    await button.click();
    await page.waitFor(1500);
    console.log(new Date() + ":Shipping Tree Login Successfull");

    const page2 = await browser.newPage();
    page2.setViewport({ width: 1900, height: 850 });
    await page2.goto('https://app3.smartturn.com/', { timeout: 0 });
    await page2.type('#EmailAddress', emailAddress, { delay: 10 });
    await page2.type('#Password', passWord);
    await page2.click('button.btn');
    console.log(new Date() + ":Picpak Login Successfull");
    return;
}

async function getInboundShipmentPlanJSON(PrepInstructions, allDatas, CountryCode) {
    var Fromaddress;
    if (CountryCode == "US") {
        Fromaddress = ShipFromAddressNameUS;
    } else {
        Fromaddress = ShipFromAddressNameAU;
    }
    var json = {
        "ShipFromAddress.Name": Fromaddress.ShipFromAddressName,
        "ShipFromAddress.AddressLine1": Fromaddress.ShipFromAddressL1,
        "ShipFromAddress.City": Fromaddress.ShipFromAddressCity,
        "ShipFromAddress.StateOrProvinceCode": Fromaddress.ShipFromAddressState,
        "ShipFromAddress.PostalCode": Fromaddress.ShipFromAddressPostal,
        "ShipFromAddress.CountryCode": Fromaddress.ShipFromAddressCountry,
        "InboundShipmentPlanRequestItems.member.1.SellerSKU": allDatas.sku,
        "InboundShipmentPlanRequestItems.member.1.Quantity": allDatas.skuQty,
        "InboundShipmentPlanRequestItems.member.1.QuantityInCase": allDatas.QuantityInCase,
    };
    for (var i = 0; i < PrepInstructions.length; i++) {
        var PrepInstruction = PrepInstructions[i];
        json["InboundShipmentPlanRequestItems.member.1.PrepDetailsList.member." + (i + 1) + ".PrepInstruction"] = PrepInstruction;
        json["InboundShipmentPlanRequestItems.member.1.PrepDetailsList.member." + (i + 1) + ".PrepOwner"] = "SELLER";
    }
    return json;
}

async function getInboundShipmentJSON(res, PrepInstructions, allDatas, CountryCode) {
    var Fromaddress;
    if (CountryCode == "US") {
        Fromaddress = ShipFromAddressNameUS;
    } else {
        Fromaddress = ShipFromAddressNameAU;
    }
    var json = {
        "ShipmentId": res.ShipmentId,
        "InboundShipmentHeader.ShipmentName": "FBA " + allDatas.Qty + "x" + allDatas.shipmentByName,
        "InboundShipmentHeader.ShipFromAddress.Name": Fromaddress.ShipFromAddressName,
        "InboundShipmentHeader.ShipFromAddress.AddressLine1": Fromaddress.ShipFromAddressL1,
        "InboundShipmentHeader.ShipFromAddress.City": Fromaddress.ShipFromAddressCity,
        "InboundShipmentHeader.ShipFromAddress.StateOrProvinceCode": Fromaddress.ShipFromAddressState,
        "InboundShipmentHeader.ShipFromAddress.PostalCode": Fromaddress.ShipFromAddressPostal,
        "InboundShipmentHeader.ShipFromAddress.CountryCode": Fromaddress.ShipFromAddressCountry,
        "InboundShipmentHeader.DestinationFulfillmentCenterId": res.DestinationFulfillmentCenterId,
        "InboundShipmentHeader.LabelPrepPreference": res.LabelPrepType,
        "InboundShipmentHeader.AreCasesRequired": true,
        "InboundShipmentHeader.ShipmentStatus": 'WORKING',
        "InboundShipmentHeader.IntendedBoxContentsSource": 'FEED',
        "InboundShipmentItems.member.1.QuantityShipped": Number(res.Items.member.Quantity),
        "InboundShipmentItems.member.1.QuantityInCase": allDatas.QuantityInCase,
        "InboundShipmentItems.member.1.SellerSKU": res.Items.member.SellerSKU,
    };

    for (var i = 0; i < PrepInstructions.length; i++) {
        var PrepInstruction = PrepInstructions[i];
        json["InboundShipmentPlanRequestItems.member.1.PrepDetailsList.member." + (i + 1) + ".PrepInstruction"] = PrepInstruction;
        json["InboundShipmentPlanRequestItems.member.1.PrepDetailsList.member." + (i + 1) + ".PrepOwner"] = "SELLER";
    }
    return json;
}

async function getTransportContentJson(ShipmentId, allDatas, basicDetails, CountryCode) {
    var contactList = basicDetails.data;
    var WeightUnit;
    var DimensionsUnit;
    var CarrierName = "";
    var json = {
        "ShipmentId": ShipmentId,
        "ShipmentType": allDatas.parcel,
    };

    if (CountryCode == "US") {
        WeightUnit = "pounds";
        DimensionsUnit = "inches";
        json["IsPartnered"] = true;
        CarrierName = "UNITED_PARCEL_SERVICE_INC";
    } else {
        WeightUnit = "kilograms";
        DimensionsUnit = "centimeters";
        json["IsPartnered"] = false;
        CarrierName = "OTHER";
    }

    if (allDatas.parcel == "SP") {
        json["TransportDetails.PartneredSmallParcelData.CarrierName"] = CarrierName;
        for (var i = 1; i <= allDatas.Qty; i++) {
            json["TransportDetails.PartneredSmallParcelData.PackageList.member." + i + ".Dimensions.Unit"] = DimensionsUnit;
            json["TransportDetails.PartneredSmallParcelData.PackageList.member." + i + ".Dimensions.Length"] = Math.round(Number(allDatas.Boxlength.toFixed(2)));
            json["TransportDetails.PartneredSmallParcelData.PackageList.member." + i + ".Dimensions.Width"] = Math.round(Number(allDatas.width.toFixed(2)));
            json["TransportDetails.PartneredSmallParcelData.PackageList.member." + i + ".Dimensions.Height"] = Math.round(Number(allDatas.heigth.toFixed(2)));
            json["TransportDetails.PartneredSmallParcelData.PackageList.member." + i + ".Weight.Unit"] = WeightUnit;
            json["TransportDetails.PartneredSmallParcelData.PackageList.member." + i + ".Weight.Value"] = Math.round(Number(allDatas.weight.toFixed(2)));
        }
    } else {
        json["TransportDetails.PartneredLtlData.Contact.Name"] = contactList.ContactName;
        json["TransportDetails.PartneredLtlData.Contact.Phone"] = contactList.ContactPhone;
        json["TransportDetails.PartneredLtlData.Contact.Email"] = contactList.ContactEmail;
        json["TransportDetails.PartneredLtlData.Contact.Fax"] = contactList.ContactFax;
        json["TransportDetails.PartneredLtlData.BoxCount"] = allDatas.Qty * 12;
        json["TransportDetails.PartneredLtlData.FreightReadyDate"] = allDatas.FreightReadyDate;

        for (var j = 1; j <= allDatas.Qty; j++) {
            json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".Dimensions.Unit"] = DimensionsUnit;
            json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".Dimensions.Length"] = Math.round(Number(allDatas.Boxlength.toFixed(2)));
            json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".Dimensions.Width"] = Math.round(Number(allDatas.width.toFixed(2)));
            json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".Dimensions.Height"] = Math.round(Number(allDatas.heigth.toFixed(0)));
            json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".Weight.Unit"] = WeightUnit;
            json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".Weight.Value"] = Math.round(Number(allDatas.weight.toFixed(2)));
            if (allDatas.heigth <= 50 && allDatas.weight <= 750) {
                json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".IsStacked"] = true;
            } else {
                json["TransportDetails.PartneredLtlData.PalletList.member." + j + ".IsStacked"] = false;
            }
        }
    }
    //console.log(json);
    return json;
}

async function createCartonContentsRequest(ShipmentId, allDatas, CountryCode) {
    var CartonId = [];
    var MessageID = (Math.random() * 100).toFixed(0);
    var NumCartons = "";
    if (allDatas.shipmentByName.indexOf("MC") > -1) {
        NumCartons = allDatas.Qty;
    } else {
        NumCartons = allDatas.Qty * 12;
    }

    var itemHtml = "";
    var cartonIds = ShipmentId + "U000";
    for (var index = 1; index <= NumCartons; index++) {
        var CartonIdUnique;
        if (index <= 9) {
            CartonIdUnique = cartonIds + "00" + (index);
        } else if (index > 9) {
            CartonIdUnique = cartonIds + "0" + (index);
        } else {
            CartonIdUnique = cartonIds + (index);
        }
        CartonId.push(CartonIdUnique);
        itemHtml += `<Carton><CartonId>${CartonIdUnique}</CartonId><Item><SKU>${allDatas.sku}</SKU><QuantityShipped>${allDatas.QuantityInCase}</QuantityShipped><QuantityInCase>${allDatas.QuantityInCase}</QuantityInCase><ExpirationDate>${allDatas.expire}</ExpirationDate></Item></Carton>`;
    }
    var httpBody = `<?xml version="1.0" encoding="UTF-8"?>  
    <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">  
    <Header><DocumentVersion>1.01</DocumentVersion><MerchantIdentifier>${SellerIds[CountryCode]}</MerchantIdentifier></Header> 
    <MessageType>CartonContentsRequest</MessageType>
    <Message><MessageID>${MessageID}</MessageID>
    <CartonContentsRequest>
    <ShipmentId>${ShipmentId}</ShipmentId>
    <NumCartons>${NumCartons}</NumCartons>${itemHtml}
    </CartonContentsRequest>
    </Message> 
    </AmazonEnvelope>`;
    //console.log(httpBody)

    var response = await cartonContentsRequest({
        'FeedType': '_POST_FBA_INBOUND_CARTON_CONTENTS_',
        'FeedContent': httpBody,
        "MarketplaceIdList.Id.1": MarketplaceIds[CountryCode]
    }, CountryCode);

    var FeedSubmissionId = response.FeedSubmissionInfo.FeedSubmissionId;
    console.log("FeedSubmissionId : " + FeedSubmissionId);
    return CartonId;
}


async function getPalletLabelPdfs(ShipmentId, allDatas, CountryCode) {

    var pkJSON = {
        "ShipmentId": ShipmentId,
        "PageType": "PackageLabel_Letter_2",
        "NumberOfPallets": allDatas.palletQty
    };
    if (CountryCode == "AU") {
        pkJSON.PageType = "PackageLabel_Thermal";
    }
    console.log("Saving Pallet pdf files");
    var response = await GetPalletLabels(pkJSON, CountryCode);

    return response.TransportDocument.PdfDocument;
}

async function getUniquePackageLabelPdfs(ShipmentId, CartonId, palletLabel, CountryCode) {
    var pdfNameList = [];

    if (palletLabel) {
        pdfNameList.push(await savePdf(palletLabel, ShipmentId));
        console.log("Saved Pallet pdf files");
    }

    console.log("Saving... carton pdf files");

    var totalCarton = CartonId.length;
    var start = 1;
    do {
        var tempCartonIdCount = 50;
        if (totalCarton < 50) {
            tempCartonIdCount = totalCarton;
        }
        var tempTotal = (start + tempCartonIdCount);
        if (tempTotal > totalCarton) {
            tempTotal = totalCarton + 1;
        }
        var LabelIdjson = {
            "ShipmentId": ShipmentId,
            "PageType": "PackageLabel_Letter_2",
        };
        if (CountryCode == "AU") {
            LabelIdjson["PageType"] = "PackageLabel_Thermal";
        }
        for (var index = start; index < tempTotal; index++) {
            LabelIdjson[`PackageLabelsToPrint.member.${(index - start) + 1}`] = CartonId[index - 1];
        }
        start = tempTotal;
        //console.log(LabelIdjson);
        try {
            var res = await getUniquePackageLabels(LabelIdjson, CountryCode);
            pdfNameList.push(await savePdf(res.TransportDocument.PdfDocument, ShipmentId));
            console.log("saved package label pdf");
        } catch (e) {
            console.log(e);
            console.log("getUniquePackageLabels ERROR");
        }
    } while (start < totalCarton);
    console.log("Label retrive is end");
    return pdfNameList;
}

async function savePdf(base64String, ShipmentId) {
    var dateString = new Date().toISOString().substring(0, 10);
    var id = crypto.randomBytes(3).toString("hex");
    var docName = SCRIPT_LOCATION + '/labels(' + dateString + ')' + id + '.zip';
    var zipFile = await mwsExtract(base64String, docName);
    console.log(zipFile);
    var zip = new AdmZip(docName);
    var pdfNameList = [];
    var zipEntries = zip.getEntries();
    zipEntries.forEach(function (zipEntry) {
        pdfNameList.push(SCRIPT_LOCATION + '/' + ShipmentId + "-" + crypto.randomBytes(2).toString("hex") + "-" + zipEntry.entryName);
    });
    zip.extractAllTo(SCRIPT_LOCATION, true);
    fs.rename(SCRIPT_LOCATION + "/PackageLabels.pdf", pdfNameList[0], (err) => {
        if (err) {
            console.log("failed to rename pdf one" + err);
        } else {
            console.log('successfully Renamed Pdf files');
        }
    });
    return pdfNameList[0];
}

async function cancelOrder(updatingJson, CountryCode) {
    updatingJson["InboundShipmentHeader.ShipmentStatus"] = 'CANCELLED';
    return await updateInboundShipment(updatingJson, CountryCode);
}

async function OutBoundShippment(poNumber, allDatas, pdfNameList, page) {
    var parcel = allDatas.parcel;
    var fullName;
    var addressLine1;
    var addressLine2;

    await page.goto('https://app.shippingtree.co/order/new/', { waitUntil: 'domcontentloaded' });

    if (parcel == "SP") {
        fullName = "Amazon (Cartons for " + poNumber + ")";
    } else {
        fullName = "Amazon (Pallet Pickup for " + poNumber + ")";
    }
    if (parcel == "SP") {
        addressLine1 = "Amazon Shipping Labels Provided";
    } else {
        addressLine1 = "Amazon to Pickup Pallets";
    }
    if (parcel == "SP") {
        addressLine2 = "Amazon Shipping Labels Provided";
    } else {
        addressLine2 = "Amazon to Pickup Pallets";
    }

    await page.type('input[name="name"]', fullName);
    await page.type('input[name="email"]', "hello@bunstersworldwide.com");
    await page.type('input[placeholder="Remote ID"]', poNumber);
    await page.type('input[name="business"]', "Amazon");
    await page.type('input[name="line1"]', addressLine1);
    await page.type('input[name="line2"]', addressLine2);
    await page.type('input[name="city"]', "Santa Fe Springs");
    await page.type('input[name="state"]', "CA");
    await page.type('input[name="zip"]', "90670");
    await page.type('input[name="tel"]', "0");
    await page.select('select[name="country"]', "US");

    if (parcel !== "SP") {
        await page.click('#root > div > div > div > div.main-nav__wrapper > div.main-nav__content > div.page-content > form > div > div:nth-child(2) > div:nth-child(5) > label');
    }

    console.log("Shipping tree form Filled");
    await page.waitFor(5000);
    await page.click('button.sc-htpNat.hMOLWg');
    await page.waitFor(5000);
    await page.waitForSelector("#root > div > div > div > div.main-nav__wrapper > div.main-nav__content > div.page-content > div:nth-child(2) > div.col-lg-3.col-xl-2 > div li:nth-child(3)");
    var allLIs = await page.$$("#root > div > div > div > div.main-nav__wrapper > div.main-nav__content > div.page-content > div:nth-child(2) > div.col-lg-3.col-xl-2 > div li");
    for (var li = 1; li <= allLIs.length; li++) {
        let text = await page.$eval(`#root > div > div > div > div.main-nav__wrapper > div.main-nav__content > div.page-content > div:nth-child(2) > div.col-lg-3.col-xl-2 > div li:nth-child(${li})`, a => a.innerText);
        if (text.indexOf("Labels") > -1) {
            do {
                await page.click(`#root > div > div > div > div.main-nav__wrapper > div.main-nav__content > div.page-content > div:nth-child(2) > div.col-lg-3.col-xl-2 > div li:nth-child(${li}) .text`);
                try {
                    await page.waitFor(3000);
                    await page.waitForSelector('.modal-content input[type="file"]');
                } catch (e) { }
            } while (await page.$('.modal-content input[type="file"]') == null);
            break;
        }
    }
    const input = await page.$('.modal-content input[type="file"]');

    var files = pdfNameList;
    if (pdfNameList.length == 1) {
        await input.uploadFile(files[0]);
    } else {
        await input.uploadFile(...files);
    }
    page.on('dialog', async dialog => {
        console.log(dialog.message());
        await dialog.dismiss();
    });
    await page.click('.modal-footer .sc-htpNat.hMOLWg');
    console.log("Uploaded");
    await page.waitFor(6000);
    await page.waitForSelector('#root > div > div > div > div.main-nav__wrapper > div.main-nav__content > div.page-content > div:nth-child(2) > div.col-lg-6.col-xl-7 > div > div:nth-child(2) > div:nth-child(1) > div > div.datatable__header > div.datatable__toolbar > button');
    do {
        await page.click('#root > div > div > div > div.main-nav__wrapper > div.main-nav__content > div.page-content > div:nth-child(2) > div.col-lg-6.col-xl-7 > div > div:nth-child(2) > div:nth-child(1) > div > div.datatable__header > div.datatable__toolbar > button');
        try {
            await page.waitFor(3000);
            await page.waitForSelector('body > div.fade.modal.show > div > div > div.modal-body > form > div.search-bar > div > input');
        } catch (e) { }
    } while (await page.$('.modal-content') == null);
    await page.click('body > div.fade.modal.show > div > div > div.modal-body > form > div.search-bar > div > input');
    await page.type('body > div.fade.modal.show > div > div > div.modal-body > form > div.search-bar > div > input', allDatas.sku);
    await page.click('body > div.fade.modal.show > div > div > div.modal-body > form > div.search-bar > div > div > button');
    console.log(allDatas.sku + "search bth clicked");
    await page.waitFor(5000);
    await page.waitForSelector('.modal-content .scrolltable table tr');
    var tableRow = await page.$$('.modal-content .scrolltable table tr');
    for (var j = 1; j <= tableRow.length; j++) {
        var nameOfSku = await page.$eval(`.modal-content .scrolltable table tr:nth-child(${j}) td:nth-child(2) span a`, a => a.innerText);
        console.log(nameOfSku);
        if (nameOfSku.trim() == allDatas.sku) {
            console.log("match");
            await page.type(`.modal-content .scrolltable table tr:nth-child(${j}) td:nth-child(4) input`, allDatas.skuQty.toString(), { delay: 1000 });
            await page.click('.modal-content .sc-htpNat.hMOLWg.primary');
            break;
        }
    }
    var shippingTreeId = await page.$eval('.order-section h3 span', a => a.innerText);
    pdfNameList.forEach(function (row) {
        fs.rename(row, SCRIPT_LOCATION + "/unlink" + crypto.randomBytes(6).toString("hex") + ".pdf", (err) => {
            if (err) {
                console.log("failed to delete local image:" + err);
            } else {
                console.log('successfully deleted previous Datas');
            }
        });
    });
    console.log("shippingTreeId " + shippingTreeId);
    return shippingTreeId;
}

async function OutBoundShippmentAU(poNumber, allDatas, pdfNameList, page) {
    await page.setViewport({ width: 1500, height: 900 });
    await page.waitFor(3000);
    await page.goto('https://app3.smartturn.com/SalesOrders/EditSO', { waitUntil: "networkidle0" });
    await page.waitFor(2000);
    await page.click('.nav.nav-tabs li:nth-child(1)');
    await page.waitForSelector('#ShipToContact');
    await page.type('#ShipToContact', "FBA: Bunsters Worldwide Pty Ltd");
    await page.type('#ShipToAddressLine1', allDatas.addName);
    await page.type('#ShipToAddressLine2', allDatas.addL1);
    await page.type('#ShipToCity', 'Dandenong South');
    await page.type('#ShipToPostalCode', '3175');
    const allCountryValue = (await page.$x('//*[@id = "ShipToddlCountry"]/option[text() = "Australia"]'))[0];
    const countryValue = await (await allCountryValue.getProperty('value')).jsonValue();
    await page.select('#ShipToddlCountry', countryValue);
    await page.waitFor(2000);
    const allStateValue = (await page.$x('//*[@id = "ShipToddlState"]/option[text() = "Victoria"]'))[0];
    const stateValue = await (await allStateValue.getProperty('value')).jsonValue();
    await page.select('#ShipToddlState', stateValue);
    await page.waitFor(1000);
    await page.click('.table-links-group.clearfix a.add-btn');
    await page.waitForSelector('#ItemNumber');
    await page.type('#ItemNumber', allDatas.sku);
    const searchBtn = await page.$('button.btn.btn-default2');
    await page.waitFor(2000);
    await searchBtn.click();
    await page.waitForSelector('td.dt-body-left.noprint');
    await page.click('td.dt-body-left.noprint input.property');
    await page.click('#btnSave');
    await page.waitForSelector('#SalesOrders_ProfilePage_ItemGrid0-6');
    await page.type('#SalesOrders_ProfilePage_ItemGrid0-6', allDatas.skuQty.toString());
    await page.waitFor(2000);
    await page.click('.nav.nav-tabs li:nth-child(3)');
    await page.waitForSelector('#Comments');
    await page.evaluate((poNumber) => {
        document.querySelector('#Comments').value = "FBA Shipment ID: " + poNumber;
    }, poNumber);
    await page.click('#btnSave');
    await page.waitFor(5000);
    var orderId;
    try {
        orderId = await page.$eval('section.content-header.clearfix li.active', active => active.innerText.split("#:")[1]);
        if (!orderId) {
            throw "error";
        }
    } catch (e) {
        try {
            var errorAlert = await page.$(".divAlert");
            if (errorAlert) {
                var errorAlertText = await page.$eval('.divAlert', a => a.innerText);
                console.log(errorAlertText);
            }
        } catch (e) {
            console.log(e);
        }
    }
    console.log("PicpakID " + orderId);
    var emailResponse = await sendLabeltoEmail(pdfNameList, orderId)
    return orderId;
}

async function OutBoundShippmentConfirmMail(poNumber, allDatas, ID, CountryCode) {
    var resultJson = {
        "CountryCode": CountryCode,
        "parcel": allDatas.parcel,
        "Qty": allDatas.skuQty,
        "SKU": allDatas.sku,
        "FBAOrderId": poNumber,
        "orderId": ID,
        "pickupDate": allDatas.FreightReadyDate
    };
    await axios.post(Url, { sendMail: resultJson });
    console.log("email send for " + poNumber);
    return;
}

async function sendLabeltoEmail(pdfNameList, orderID) {
    //authorize(credentials, sendEmail);
    let { client_secret, client_id, redirect_uris } = credentials.installed;
    let oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

    var token = await getFileData(TOKEN_PATH);
    if (!token) {
        console.log("Token Error")
    } else {
        try {
            oAuth2Client.setCredentials(JSON.parse(token));
            authvals = oAuth2Client;
            await sendEmail(authvals, pdfNameList, orderID);
        } catch (error) {
            console.log(error)
        }
    }
}

async function sendEmail(auth, pdfNameList, orderID) {
    var attachments = [];
    pdfNameList.forEach(function (row) {
        attachments.push({ "path": row })
    })
    let mail = new MailComposer({
        to: "service@pikpak.com.au",
        html: "Find the attached the label",
        subject: "Amazon Box Label (Sales OrderID:" + orderID + ")",
        textEncoding: "base64",
        replyTo: "hello@bunstersworldwide.com",
        cc: "andrew@bunstersworldwide.com",
        attachments: attachments
    });
    var msg;
    try {
        msg = await mail.compile().build();
    } catch (e) {
        return console.log('Error compiling email ' + error);
    }

    const encodedMessage = Buffer.from(msg)
        .toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=+$/, '');

    const gmail = google.gmail({ version: 'v1', auth });
    var result = await gmail.users.messages.send({
        userId: 'me',
        resource: {
            raw: encodedMessage,
        }
    });
    console.log("NODEMAILER - Sending email reply from server:", result.data);
    for (var i = 0; i < pdfNameList.length; i++) {
        var row = pdfNameList[i];
        try {
            await fileRename(row, SCRIPT_LOCATION + "/unlink" + crypto.randomBytes(6).toString("hex") + ".pdf")
            console.log('successfully renamed pdf Datas');
        } catch (e) {
            console.log("failed to delete local image:" + err);
        }
    }
}

async function errmailsend(error, allDatas, ShipmentId) {
    let resultErrorJson = {
        "allData": allDatas,
        "shipmentID": ShipmentId,
        "CartonId": "",
        "palletLabel": "",
        "error": error.Message ? error.Message : error.message,
        "errors": error
    };
    console.log(resultErrorJson)
    await axios.post(Url, { sendErrorMail: resultErrorJson });
    console.log("Send Error Email");
}