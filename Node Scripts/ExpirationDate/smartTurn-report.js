var Url = "https://script.google.com/macros/s/AKfycbx_ujssQv8Dv49LV-pcakkXnGlk2UUTSE1s0giOxpiXtMVk_FbL/exec";
const puppeteer = require('puppeteer');
const axios = require('axios');
//const fs = require('fs-extra');
const emailAddress = "";
const passWord = "";

(async function main() {
    const browser = await puppeteer.launch({ headless: true });
    try {

        const page = await browser.newPage();
        await page.goto('https://app3.smartturn.com/', { timeout: 0 });
        await page.type('#EmailAddress', emailAddress, { delay: 10 });
        await page.type('#Password', passWord);
        const button = await page.$('button.btn');
        await button.click();
        console.log(new Date() + ": Login Successfull");
        await page.waitForSelector('ul.sidebar-menu');
        await page.goto('https://app3.smartturn.com/InventoryByItem/Index', { waitUntil: "networkidle0" });
        await page.waitForSelector('#InventoryByItemListPage');
        var item = [];
        var expireDate = [];
        var lot = [];
        var discription = [];
        var serialNum = [];
        var merge = [];
        var table = [];
        //await fs.writeFile('out.csv', 'Item,Available to promise\n');
        var now = new Date();
        var month = now.getMonth() + 1;
        var Year = now.getFullYear();
        var Day = now.getDate();
        do {
            const sections = await page.$$('#InventoryByItemListPage tbody tr td:nth-child(1)');
            const sections2 = await page.$$('#InventoryByItemListPage tbody tr td:nth-child(8)');
            const sections3 = await page.$$('#InventoryByItemListPage tbody tr td:nth-child(5)');
            const sections4 = await page.$$('#InventoryByItemListPage tbody tr td:nth-child(2)');

            for (i = 0; i < sections.length; i++) {
                merge[i] = [];
                try {
                    merge[i][0] = await sections4[i].$eval('span', a => a.innerText);
                } catch (e) {
                    merge[i][0] = "";
                }
                merge[i][1] = await sections[i].$eval('a', a => a.innerText);
                try {
                    merge[i][2] = await sections3[i].$eval('span', a => a.innerText);
                } catch (e) {
                    merge[i][2] = "";
                }
                try {
                    merge[i][3] = await sections2[i].$eval('span', a => a.innerText);
                } catch (e) {
                    merge[i][3] = "";
                }
                merge[i][4] = Year.toString() + "/" + month.toString() + "/" + (Day).toString()
                table.push(merge[i]);
            }

            var disabledBtn = await page.$('li.paginate_button.next.disabled');
            if (disabledBtn === null) {
                await page.waitForSelector('ul.pagination');
                const buttonForNext = await page.$('li.paginate_button.next');
                await buttonForNext.click();
                await page.waitFor(5000);
            }
            else {
                break;
            }
        } while (1);

        var tableRange = table.length + 1;
        console.log(new Date() + ": " + tableRange + " Skus and ExpireDate are found");
        await axios.post(Url, { picpakExpireDate: table });
        console.log(new Date() + ": stored in google sheet");
        axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Pikpak Expriation Date" });
        await browser.close();
    } catch (e) {
        await browser.close();
        console.log(e);
    }
})();