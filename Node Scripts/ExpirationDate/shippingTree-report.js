const puppeteer = require('puppeteer');
const axios = require('axios');
const emailAddress = "";
const passWord = "";

// function
(async function main() {
    var browser = await puppeteer.launch({ headless: true });
    try {
        var page = await browser.newPage();
        await page.setViewport({
            width: 1000,
            height: 1080
        });
        await page.goto('https://app.shippingtree.co/product/', { waitUntil: 'domcontentloaded' });
        await page.type('input[name="username"]', emailAddress, { delay: 10 });
        await page.type('input[name="password"]', passWord, { delay: 10 });
        const button = await page.$('button.sc-htpNat.hMOLWg.royal');
        await button.click();
        await page.waitFor(1500);
        console.log(new Date() + ": Login Successfull");
        await page.waitForSelector('table tbody tr td:nth-child(3)');
        await page.click('.sc-htpNat.hMOLWg');
        await page.waitFor(4000);
        await page.waitForSelector('.modal-content .rdrStaticRanges');
        await page.click('.modal-content .rdrStaticRanges .rdrStaticRange:first-child');
        await page.waitFor(1000);
        await page.click('.modal-content .sc-htpNat.hMOLWg');
        axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Shipping Expriation Date" });
        await browser.close();
    } catch (e) {
        await browser.close();
        console.log(e);
    }
})();