const { BigQuery } = require('@google-cloud/bigquery');
const uuid = require('uuid');
const util = require('util');
const axios = require('axios');
const amazonConfig = require('./amazon_config.json');
module.paths.push('/usr/local/lib/node_modules');

function getAmazonMws(config) {
    var amazonMws = require('amazon-mws')(config.AWS_ACCESS_KEY_ID, config.AWS_SECRET_ACCESS_KEY);
    amazonMws.setHost(config.HOST);
    return amazonMws;
}

var startDateTime = new Date();
startDateTime.setDate(startDateTime.getDate() - 500);

var getInventory = async function (country) {
    var config = amazonConfig[country];
    var amazonMws = getAmazonMws(config);

    var nextToken;
    var response;
    var inventory = [];
    do {
        var resource = {
            'Version': '2010-10-01',
            'Action': 'ListInventorySupply',
            'SellerId': config.SELLER_ID,
            'MarketplaceId': config.MARKETPLACE_ID
        };
        if (nextToken) {
            resource["NextToken"] = nextToken;
            resource["Action"] = 'ListInventorySupplyByNextToken';
        } else {
            resource["QueryStartDateTime"] = startDateTime;
        }

        response = await amazonMws.fulfillmentInventory.search(resource);
        nextToken = response.NextToken;

        let newDatas;
        if (Array.isArray(response.InventorySupplyList.member)) {
            newDatas = response.InventorySupplyList.member;
        } else {
            newDatas = [response.InventorySupplyList.member];
        }

        if (newDatas) {
            inventory = inventory.concat(newDatas.map(function (item) {
                return {
                    "SellerSKU": item.SellerSKU,
                    "InStockSupplyQuantity": item.InStockSupplyQuantity,
                    "TotalSupplyQuantity": item.TotalSupplyQuantity
                };
            }));
        }
    } while (nextToken);

    inventory = inventory.reduce((agg, curr) => {
        if (agg[curr["SellerSKU"]]) {
            agg[curr["SellerSKU"]] = {
                InStockSupplyQuantity: Number(agg[curr["SellerSKU"].InStockSupplyQuantity]) + Number(curr["InStockSupplyQuantity"]),
                TotalSupplyQuantity: Number(agg[curr["SellerSKU"].TotalSupplyQuantity]) + Number(curr["TotalSupplyQuantity"])
            };
        } else {
            agg[curr["SellerSKU"]] = {
                InStockSupplyQuantity: Number(curr["InStockSupplyQuantity"]),
                TotalSupplyQuantity: Number(curr["TotalSupplyQuantity"])
            };
        }
        return agg;
    }, {});

    return inventory;
};

var getIncoming = async function (country) {
    var config = amazonConfig[country];
    var amazonMws = getAmazonMws(config);

    var nextToken;
    var response;
    var shipments = [];
    do {
        var resource = {
            'Version': '2010-10-01',
            'Action': 'ListInboundShipments',
            'SellerId': config.SELLER_ID,
            'MarketplaceId': config.MARKETPLACE_ID
        };

        if (nextToken) {
            resource["NextToken"] = nextToken;
            resource["Action"] = 'ListInboundShipmentsByNextToken';
        } else {
            resource['ShipmentStatusList.member.1'] = 'SHIPPED';
            resource['ShipmentStatusList.member.2'] = 'IN_TRANSIT';
        }
        response = await amazonMws.fulfillmentInboundShipment.search(resource);
        nextToken = response.NextToken;

        let newDatas;
        if (response.ShipmentData && response.ShipmentData.member) {
            if (Array.isArray(response.ShipmentData.member)) {
                newDatas = response.ShipmentData.member;
            } else {
                newDatas = [response.ShipmentData.member];
            }
        }

        if (newDatas) {
            shipments = shipments.concat(newDatas);
        }

    } while (nextToken);

    const items = [];
    var nextToken;
    var itemsResponse;
    for (const shipment of shipments) {
        do {
            var resource = {
                'Version': '2010-10-01',
                'Action': 'ListInboundShipmentItems',
                'SellerId': config.SELLER_ID,
                'MarketplaceId': config.MARKETPLACE_ID
            };
            if (nextToken) {
                resource["NextToken"] = nextToken;
                resource["Action"] = 'ListInboundShipmentItemsByNextToken';
            } else {
                resource['ShipmentId'] = shipment.ShipmentId;
            }

            itemsResponse = await amazonMws.fulfillmentInboundShipment.search(resource);
            nextToken = itemsResponse.ItemData.NextToken;

            if (itemsResponse.ItemData.member && itemsResponse.ItemData.member.SellerSKU) {
                items.push({
                    SellerSKU: itemsResponse.ItemData.member.SellerSKU,
                    QuantityShipped: itemsResponse.ItemData.member.QuantityShipped
                });
            }
        } while (nextToken);
    }

    let inbounds = items.reduce((agg, curr) => {
        agg[curr["SellerSKU"]] = (agg[curr["SellerSKU"]]) ? Number(agg[curr["SellerSKU"]]) + Number(curr["QuantityShipped"]) : 0 + Number(curr["QuantityShipped"]);
        return agg;
    }, {});

    return inbounds;
};
var ReportJson = {};
var RequestReport = async function (country) {
    var config = amazonConfig[country];
    var amazonMws = getAmazonMws(config);

    let ReportId;
    if (country != "US") {
        var requestReport = {
            'Version': '2009-01-01',
            'Action': 'RequestReport',
            'SellerId': config.SELLER_ID,
            'Marketplace': config.MARKETPLACE_ID,
            'ReportType': "_GET_RESERVED_INVENTORY_DATA_",
            "StartDate": startDateTime,
            "EndDate": new Date()
        };

        let response = await amazonMws.reports.submit(requestReport);
        let ReportRequestId = response.ReportRequestInfo.ReportRequestId;
        var checkResponsewait = response.ReportRequestInfo.ReportProcessingStatus;
        console.log("\nReportRequestId - " + ReportRequestId);
        var count = 1;

        do {
            console.log("\nwait response for " + ReportRequestId);
            await new Promise(r => setTimeout(r, 60000 * count));
            var getReportRequestList = {
                'Version': '2009-01-01',
                'Action': 'GetReportRequestList',
                'SellerId': config.SELLER_ID,
                'Marketplace': config.MARKETPLACE_ID,
                'ReportTypeList.Type.1': "_GET_RESERVED_INVENTORY_DATA_",
                "ReportRequestIdList.Id.1": ReportRequestId
            };
            let checkResponse = await amazonMws.reports.submit(getReportRequestList);
            if (checkResponse.ReportRequestInfo.ReportProcessingStatus == "_DONE_")
                checkResponsewait = false;

            count++;
        } while (checkResponsewait && count < 6);

        if (count > 5) {
            throw "Unable to get report";
        }

        var getReportList = {
            'Version': '2009-01-01',
            'Action': 'GetReportList',
            'SellerId': config.SELLER_ID,
            'Marketplace': config.MARKETPLACE_ID,
            'ReportTypeList.Type.1': "_GET_RESERVED_INVENTORY_DATA_",
            "ReportRequestIdList.Id.1": ReportRequestId
        };
        let response2 = await amazonMws.reports.submit(getReportList);
        ReportId = response2.ReportInfo.ReportId;
    } else {
        ReportId = ReportJson.CA;
    }
    ReportJson[country] = ReportId;
    console.log("\nReportId - " + ReportId);

    var getReport = {
        'Version': '2009-01-01',
        'Action': 'GetReport',
        'SellerId': config.SELLER_ID,
        'Marketplace': config.MARKETPLACE_ID,
        'ReportId': ReportId
    };

    let response3 = await amazonMws.reports.submit(getReport);

    var resportDetails = {};
    response3.data.forEach(function (row) {
        resportDetails[row.sku] = row.reserved_qty;
    });

    return resportDetails;
};

(async function main() {
    var countries = ["AU", "CA", "US"];
    var inventory = {};
    var inbounds = {};
    var Warehouse = {};
    var reservedListJson = {};

    for (var i = 0; i < countries.length; i++) {
        var inventorylist = await getInventory(countries[i]);
        for (var key in inventorylist) {
            if (inventorylist[key].InStockSupplyQuantity > 0 || inventorylist[key].TotalSupplyQuantity > 0 || inventorylist[key] == undefined || i == 0) {
                inventory[key] = inventorylist[key];
                Warehouse[key] = countries[i] + "-AMZ";
            }
        }
        var inboundsList = await getIncoming(countries[i]);
        for (var key2 in inboundsList) {
            inbounds[key2] = inboundsList[key2];
            Warehouse[key2] = countries[i] + "-AMZ";
        }
        var reservedList = await RequestReport(countries[i]);
        for (var key3 in reservedList) {
            reservedListJson[key3] = reservedList[key3];
        }
    }

    const skus = [...new Set(Object.keys(inventory).concat(Object.keys(inbounds)))];

    const res = skus.map(sku => ({
        sku: sku,
        available: (inventory[sku]) ? inventory[sku].InStockSupplyQuantity : 0,
        reserved: (reservedListJson[sku]) ? reservedListJson[sku] : 0,
        inbound: (inbounds[sku]) ? inbounds[sku] : 0
    }));

    var now = new Date();
    const rows = skus.map(sku => ({
        SKU: sku,
        Available_qty: (inventory[sku]) ? inventory[sku].InStockSupplyQuantity : 0,
        Reserved_qty: (reservedListJson[sku]) ? reservedListJson[sku] : 0,
        Inbound_qty: (inbounds[sku]) ? inbounds[sku] : 0,
        Timestamp: now.getTime() / 1000,
        Warehouse: Warehouse[sku],
        Date: now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate()
    }));

    var postResponse = await axios.post(
        'https://script.google.com/macros/s/AKfycbx_ujssQv8Dv49LV-pcakkXnGlk2UUTSE1s0giOxpiXtMVk_FbL/exec',
        { data: res, inventory: true });
    console.log(postResponse.data);
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Amazon Inventory" });

    if (rows && rows.length > 0) {
        await sendToBigQuery(rows);
    }
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Amazon Inventory - BQ" });
})();

async function sendToBigQuery(rows) {
    const bigquery = new BigQuery();
    var datasetId = "master";
    var tableId = "Inventory";
    console.log(rows);
    await bigquery.dataset(datasetId).table(tableId).insert(rows).then(function (data) {
        console.log("\n API Response ", util.inspect(data[0], false, null, true));
        console.log("\nStored to Bigquery Table and updated filfillment status");
    }).catch(function (err) {
        console.log("\n API Error " + err.name);
        console.log("inventory: ", util.inspect(err, false, null, true));
    });
}