const { BigQuery } = require('@google-cloud/bigquery');
const uuid = require('uuid');
const util = require('util');
const axios = require('axios');
const amazonConfig = require('./amazon_config.json');
module.paths.push('/usr/local/lib/node_modules');

function getAmazonMws(config) {
    var amazonMws = require('amazon-mws')(config.AWS_ACCESS_KEY_ID, config.AWS_SECRET_ACCESS_KEY);
    amazonMws.setHost(config.HOST);
    return amazonMws;
}

var startDateTime = new Date();
startDateTime.setDate(startDateTime.getDate() - 10);

var RequestReport = async function (country) {
    var config = amazonConfig[country];
    var amazonMws = getAmazonMws(config);

    var requestReport = {
        'Version': '2009-01-01',
        'Action': 'RequestReport',
        'SellerId': config.SELLER_ID,
        'Marketplace': config.MARKETPLACE_ID,
        'ReportType': "_GET_FBA_FULFILLMENT_INVENTORY_RECEIPTS_DATA_",
        "StartDate": startDateTime,
        "EndDate": new Date()
    };

    let response = await amazonMws.reports.submit(requestReport);
    let ReportRequestId = response.ReportRequestInfo.ReportRequestId;
    var checkResponsewait = response.ReportRequestInfo.ReportProcessingStatus;
    console.log("\nReportRequestId - " + ReportRequestId);
    var count = 1;

    do {
        console.log("\nwait response for " + ReportRequestId);
        await new Promise(r => setTimeout(r, 6000 * count));
        var getReportRequestList = {
            'Version': '2009-01-01',
            'Action': 'GetReportRequestList',
            'SellerId': config.SELLER_ID,
            'Marketplace': config.MARKETPLACE_ID,
            'ReportTypeList.Type.1': "_GET_FBA_FULFILLMENT_INVENTORY_RECEIPTS_DATA_",
            "ReportRequestIdList.Id.1": ReportRequestId
        };
        let checkResponse = await amazonMws.reports.submit(getReportRequestList);

        if (checkResponse.ReportRequestInfo.ReportProcessingStatus == "_DONE_")
            checkResponsewait = false;

        if (checkResponse.ReportRequestInfo.ReportProcessingStatus == "_CANCELLED_")
            throw "Request Report Cancelled";

        count++;
    } while (checkResponsewait && count < 6);

    if (count > 5) {
        throw "Unable to get report";
    }

    var getReportList = {
        'Version': '2009-01-01',
        'Action': 'GetReportList',
        'SellerId': config.SELLER_ID,
        'Marketplace': config.MARKETPLACE_ID,
        'ReportTypeList.Type.1': "_GET_FBA_FULFILLMENT_INVENTORY_RECEIPTS_DATA_",
        "ReportRequestIdList.Id.1": ReportRequestId
    };
    let response2 = await amazonMws.reports.submit(getReportList);
    let ReportId = response2.ReportInfo.ReportId;
    console.log("\nReportId - " + ReportId);

    var getReport = {
        'Version': '2009-01-01',
        'Action': 'GetReport',
        'SellerId': config.SELLER_ID,
        'Marketplace': config.MARKETPLACE_ID,
        'ReportId': ReportId
    };

    let response3 = await amazonMws.reports.submit(getReport);
    return response3.data;
};

(async function main() {
    var countries = ["AU", "US"];
    var receiptsList = [];

    for (var i = 0; i < countries.length; i++) {
        console.log("Processing " + countries[i]);
        var reportResponse = await RequestReport(countries[i]);
        reportResponse.forEach(function (row) {
            receiptsList.push(createItemJson(row, countries[i]));
        });
        await sendData(receiptsList);
        console.log("Done " + countries[i]);
        receiptsList = [];
    }
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Amazon Inventory Receipts - BQ" });
})();

async function sendData(newVals) {
    const bigquery = new BigQuery();

    var schema = getSchema();
    var textTable = "";
    var count = 0;

    for (var index = 0; index < newVals.length; index++) {
        var row = newVals[index];
        var tempText = "SELECT\n";
        for (var key in row) {
            if (schema[key] == "NUMERIC" || schema[key] == "FLOAT" || schema[key] == "INTEGER") {
                tempText += ((row[key] && Number(row[key]).toString() != "NaN") ? Number(row[key]) : "NULL") + ' ' + key + ",";
            } else if (schema[key] == "TIMESTAMP") {
                tempText += ' TIMESTAMP("' + ((row[key]) ? row[key] : " TIMESTAMP(NULL)") + '") ' + key + ",";
            } else if (schema[key] == "DATE") {
                tempText += ' DATE("' + ((row[key]) ? row[key] : " DATE(NULL)") + '") ' + key + ",";
            } else {
                tempText += ' ' + ((row[key]) ? ('"' + row[key] + '"') : "STRING(NULL)") + ' ' + key + ",";
            }
        }
        textTable += tempText.slice(0, tempText.length - 1) + "\n UNION ALL \n";
        count++;
        if (count >= 500) {
            count = 0;
            await runQuery(bigquery, textTable);
            textTable = "";
        }
    }
    if (count <= 500 && textTable != "") {
        await runQuery(bigquery, textTable);
    }
    console.log("Bigquery data uploaded");
}

async function runQuery(bigquery, textTable) {

    var mainQuery = "MERGE\n`bunsters.master.Amazon_fulfilment_receipts` D\nUSING\n (" + textTable.slice(0, textTable.length - 11) + ") S"
        + " ON\n  D.Warehouse = S.Warehouse AND D.SKU = S.SKU AND D.FBA_shipment_id = S.FBA_shipment_id\nWHEN MATCHED\n"
        + " THEN UPDATE SET D.Warehouse = S.Warehouse, D.Received_date = S.Received_date, D.SKU = S.SKU, D.product_name = S.product_name, D.fnsku = S.fnsku, D.Quantity = S.Quantity, D.FBA_shipment_id = S.FBA_shipment_id, D.Fulfillment_center_id = S.Fulfillment_center_id"
        + " WHEN NOT MATCHED\nTHEN\nINSERT (Warehouse, Received_date, SKU, product_name, fnsku, Quantity, FBA_shipment_id, Fulfillment_center_id)"
        + " VALUES\n(S.Warehouse, S.Received_date, S.SKU, S.product_name, S.fnsku, S.Quantity, S.FBA_shipment_id, S.Fulfillment_center_id)";

    const options = {
        query: mainQuery,
        useLegacySql: false
    };

    const [job] = await bigquery.createQueryJob(options);
    console.log(`Job ${job.id} started.`);
    await job.promise();
    console.log(`Job ${job.id} ` + job.metadata.status.state);
}

function createItemJson(item, countries) {
    var json = {
        "Warehouse": countries + "-AMZ",
        "Received_date": item["received-date"],
        "SKU": item["sku"],
        "product_name": item["product-name"],
        "fnsku": item["fnsku"],
        "Quantity": item["quantity"],
        "FBA_shipment_id": item["fba-shipment-id"],
        "Fulfillment_center_id": item["fulfillment-center-id"],
    };
    return json;
}

function getSchema() {
    return {
        "Warehouse": "STRING",
        "Received_date": "DATE",
        "SKU": "STRING",
        "product_name": "STRING",
        "fnsku": "STRING",
        "Quantity": "INTEGER",
        "FBA_shipment_id": "STRING",
        "Fulfillment_center_id": "STRING",
    };
}