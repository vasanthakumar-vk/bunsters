const { BigQuery } = require('@google-cloud/bigquery');
const amazonConfig = require('./amazon_config.json');
const axios = require('axios');
var startDateTime = new Date();
startDateTime.setDate(startDateTime.getDate() - 5);

var errorOrders = [];

function getAmazonMws(config) {
    var amazonMws = require('amazon-mws')(config.AWS_ACCESS_KEY_ID, config.AWS_SECRET_ACCESS_KEY);
    amazonMws.setHost(config.HOST);
    return amazonMws;
}

var requestPromise = async function (amazonMws, resource) {
    return new Promise(async function (resolve) {
        await amazonMws.orders.search(resource, function (error, res) {
            if (error) {
                console.dir(error.Message);
                res = "ORDER ERROR :" + error.Message;
            }
            resolve(res);
        });
    });
};

var getOrderItem = function (order, config, amazonMws) {
    return new Promise(async function (resolve, reject) {
        var resource = {
            'Version': '2013-09-01',
            'Action': 'ListOrderItems',
            'SellerId': config.SELLER_ID,
            'MarketplaceId.Id.1': config.MARKETPLACE_ID,
            'AmazonOrderId': order.AmazonOrderId
        };
        var response;//= await amazonMws.orders.search(resource);
        var flag = false;

        let newResource = JSON.parse(JSON.stringify(resource));
        response = await requestPromise(amazonMws, newResource);
        if (typeof response == "string" && response.indexOf("ORDER ERROR :") > -1) {
            flag = true;
            await new Promise(resolve => setTimeout(resolve, 10000));
            errorOrders.push(order)
        }

        let newDatas;
        if (response.OrderItems && response.OrderItems.OrderItem && flag == false) {
            if (Array.isArray(response.OrderItems.OrderItem)) {
                newDatas = response.OrderItems.OrderItem;
            } else {
                newDatas = [response.OrderItems.OrderItem];
            }
        } else {
            newDatas = "error Item"
        }
        resolve({
            order: order,
            orderItem: newDatas
        });
    });
};

var getOrderItems = async function (list, config, amazonMws) {
    const promises = [];
    const items = [];
    for (let i = 0; i < list.length; i++) {
        promises.push(getOrderItem(list[i], config, amazonMws));
    }
    await Promise.all(promises).then(function (value) {
        value.forEach(function (row) {
            if (row.orderItem != "error Item") {
                row.orderItem.forEach(function (item) {
                    items.push({
                        order: row.order,
                        orderItem: item
                    });
                });
            }
        });
    });
    return items;
};

var getOrders = async function (config, amazonMws) {
    var nextToken;
    var response;
    var orders = [];
    do {
        var resource = {
            'Version': '2013-09-01',
            'Action': 'ListOrders',
            'SellerId': config.SELLER_ID,
            'MarketplaceId.Id.1': config.MARKETPLACE_ID
        };
        if (nextToken) {
            resource["NextToken"] = nextToken;
            resource["Action"] = 'ListOrdersByNextToken';
        } else {
            resource["LastUpdatedAfter"] = startDateTime;
        }
        var orderPromiseCount = 1;
        var flag = false;
        do {
            var errorFlag = false
            var newResource = JSON.parse(JSON.stringify(resource));
            try {
                response = response = await amazonMws.orders.search(newResource);
            } catch (e) {
                console.log(e.Message)
                errorFlag = true
            }

            if (errorFlag == true) {
                flag = true;
                console.log("Wait " + orderPromiseCount + " Minute");
                await new Promise(resolve => setTimeout(resolve, 61000 * orderPromiseCount));
                orderPromiseCount++;
            }

        } while (errorFlag && orderPromiseCount <= 5);
        await new Promise(resolve => setTimeout(resolve, 11000));

        nextToken = response.NextToken;
        let newDatas;
        if (response.Orders && response.Orders.Order) {
            if (Array.isArray(response.Orders.Order)) {
                newDatas = response.Orders.Order;
            } else {
                newDatas = [response.Orders.Order];
            }
        }
        if (newDatas) {
            orders = orders.concat(newDatas);
        }
    } while (nextToken);
    return orders;
};

(async function main() {
    var countries = ["AU", "CA", "US"];
    var completeditesCount = 0;
    for (var i = 0; i < countries.length; i++) {
        var config = amazonConfig[countries[i]];
        var amazonMws = getAmazonMws(config);
        var orders = await getOrders(config, amazonMws);
        console.log(orders.length + " Orders are Found in the " + countries[i]);

        var orderWithItem = [];
        var start = 0;
        var numberOf = 25;
        do {
            var splitedOrders = orders.slice(start, start + numberOf);
            await new Promise(resolve => setTimeout(resolve, 4000));
            var orderItems = await getOrderItems(splitedOrders, config, amazonMws);
            orderItems.forEach(function (item) {
                orderWithItem.push(createItemJson(countries[i], item));
            });
            start = start + numberOf;
            if (orderWithItem.length >= 50) {
                await sendData(orderWithItem);
                orderWithItem = []
                completeditesCount = completeditesCount + orderWithItem.length;
            }
        } while (start < orders.length);

        if (orderWithItem.length > 0) {
            await sendData(orderWithItem);
            orderWithItem = [];
            completeditesCount = completeditesCount + orderWithItem.length;
        }

        do {
            var copyErrorOrders = [...errorOrders];
            var errorDataWaitCount = 1
            errorOrders = []
            var startError = 0;
            var numberOfErrorItem = 25;
            do {
                var splitedOrders = copyErrorOrders.slice(startError, startError + numberOfErrorItem);
                await new Promise(resolve => setTimeout(resolve, 2000 * errorDataWaitCount));
                var orderItems = await getOrderItems(splitedOrders, config, amazonMws);
                orderItems.forEach(function (item) {
                    orderWithItem.push(createItemJson(countries[i], item));
                });
                startError = startError + numberOfErrorItem;
                errorDataWaitCount = errorDataWaitCount + 1
                if (orderWithItem.length >= 50) {
                    await sendData(orderWithItem);
                    orderWithItem = []
                    completeditesCount = completeditesCount + orderWithItem.length
                }
            } while (startError < copyErrorOrders.length);

            if (orderWithItem.length > 0) {
                await sendData(orderWithItem);
                orderWithItem = []
                completeditesCount = completeditesCount + orderWithItem.length
            }
            console.log("error items length----" + errorOrders.length);
        } while (errorOrders.length != 0);
        errorOrders = [];
    }
    console.log(completeditesCount + " no of item completed..")
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Amazon Sales" });
})();

async function sendData(newVals) {
    const bigquery = new BigQuery();
    var schema = getSchema();
    var textTable = "";
    var count = 0;
    for (var index = 0; index < newVals.length; index++) {
        var row = newVals[index];
        var tempText = "SELECT\n";
        for (var key in row) {
            if (schema[key] == "NUMERIC" || schema[key] == "FLOAT") {
                tempText += ((row[key] && Number(row[key]).toString() != "NaN") ? Number(row[key]) : "NULL") + ' ' + key + ",";
            } else if (schema[key] == "TIMESTAMP") {
                tempText += ' TIMESTAMP("' + ((row[key]) ? row[key] : "TIMESTAMP(NULL)") + '") ' + key + ",";
            } else {
                tempText += ' ' + ((row[key]) ? ('"' + row[key] + '"') : "STRING(NULL)") + ' ' + key + ",";
            }
        }
        textTable += tempText.slice(0, tempText.length - 1) + "\n UNION ALL \n";
        count++;
        if (count >= 500) {
            count = 0;
            var mainQuery = "MERGE\n`bunsters.master.sales` T\nUSING\n (" + textTable.slice(0, textTable.length - 11) + ") S "
                + "ON\n  T.Order_ID = S.Order_ID  AND T.SKU = S.SKU\nWHEN MATCHED\nTHEN UPDATE SET SKU = S.SKU, Channel = S.Channel, Units = S.Units, Currency = S.Currency, Value = S.Value, Timestamp = S.Timestamp, Order_ID = S.Order_ID, Order_Status = S.Order_Status, Updated_Timestamp = S.Updated_Timestamp, Region = S.Region"
                + " WHEN NOT MATCHED\nTHEN\nINSERT (SKU, Channel, Units, Currency, Value, Timestamp, Order_ID, Order_Status, Updated_Timestamp, Region)"
                + " VALUES\n(S.SKU, S.Channel, S.Units, S.Currency, S.Value, S.Timestamp, S.Order_ID, S.Order_Status, S.Updated_Timestamp, S.Region)";

            const options = {
                query: mainQuery,
                location: 'US',
            };
            const [job] = await bigquery.createQueryJob(options);
            console.log(`Job ${job.id} started.`);
            // const [rows] = await job.getQueryResults();
            // console.log('Rows:');
            // rows.forEach(row => console.log(row));
            textTable = "";
        }
    }
    if (count < 500 && textTable != "") {
        var mainQuery = "MERGE\n`bunsters.master.sales` T\nUSING\n (" + textTable.slice(0, textTable.length - 11) + ") S "
            + "ON\n  T.Order_ID = S.Order_ID  AND T.SKU = S.SKU\nWHEN MATCHED\nTHEN UPDATE SET SKU = S.SKU, Channel = S.Channel, Units = S.Units, Currency = S.Currency, Value = S.Value, Timestamp = S.Timestamp, Order_ID = S.Order_ID, Order_Status = S.Order_Status, Updated_Timestamp = S.Updated_Timestamp, Region = S.Region"
            + " WHEN NOT MATCHED\nTHEN\nINSERT (SKU, Channel, Units, Currency, Value, Timestamp, Order_ID, Order_Status, Updated_Timestamp, Region)"
            + " VALUES\n(S.SKU, S.Channel, S.Units, S.Currency, S.Value, S.Timestamp, S.Order_ID, S.Order_Status, S.Updated_Timestamp, S.Region)";
        const options = {
            query: mainQuery,
            location: 'US',
        };
        const [job] = await bigquery.createQueryJob(options);
        console.log(`Job ${job} started.`);
    }
    console.log("Bigquery data uploaded");
}

function createItemJson(countries, res) {
    let order = res.order;
    let orderItem = res.orderItem;
    var channel;
    if (order.SalesChannel == "Amazon.com") channel = "Amazon US"
    if (order.SalesChannel == "Amazon.com.au") channel = "Amazon AU"
    if (order.SalesChannel == "Amazon.ca") channel = "Amazon CA"
    if (order.SalesChannel == "Non-Amazon") channel = "Non Amazon"

    /*var PurchaseDate
    var dd = new Date(order.PurchaseDate)
    if (order.SalesChannel == "Amazon.com") dd.setHours(dd.getHours() - 7);
    if (order.SalesChannel == "Amazon.com.au") dd.setHours(dd.getHours() + 11);
    if (order.SalesChannel == "Amazon.ca") dd.setHours(dd.getHours() - 7);

    if (order.SalesChannel == "Non-Amazon") {
        let countryCode;
        if (order.ShippingAddress && order.ShippingAddress.CountryCode) {
            countryCode = order.ShippingAddress.CountryCode;
        } else {
            countryCode = countries;
        }
        if (countryCode == "AU") {
            dd.setHours(dd.getHours() + 11);
        } else {
            dd.setHours(dd.getHours() - 7);
        }
    }
    PurchaseDate = dd.toISOString();*/

    var tempObj = {
        "SKU": orderItem.SellerSKU,
        "Channel": channel,
        "Units": orderItem.QuantityOrdered,
        "Timestamp": order.PurchaseDate,
        "Order_ID": order.AmazonOrderId,
        "Order_Status": order.OrderStatus,
        "Updated_Timestamp": new Date().toISOString(),
        "Region": countries,
        "Currency": undefined,
        "Value": undefined
    };
    if (orderItem.ItemPrice) {
        tempObj.Currency = orderItem.ItemPrice.CurrencyCode;
        tempObj.Value = orderItem.ItemPrice.Amount;
    }
    return tempObj;
}

function getSchema() {
    return {
        "SKU": "STRING",
        "Channel": "STRING",
        "Units": "NUMERIC",
        "Currency": "STRING",
        "Value": "FLOAT",
        "Timestamp": "TIMESTAMP",
        "Order_ID": "STRING",
        "Order_Status": "STRING",
        "Updated_Timestamp": "TIMESTAMP",
        "Region": "STRING"
    };
}
