const { BigQuery } = require('@google-cloud/bigquery');
const puppeteer = require('puppeteer');
const axios = require('axios');
const csv = require('csv-parser');
const path = require('path');
const fs = require('fs-extra');
const util = require('util');
const keepspaceConfig = require('./keepspace_config.json');
const emailAddress = keepspaceConfig.emailAddress;
const passWord = keepspaceConfig.passWord;


var File_Name;

// function
(async function main() {
    var browser = await puppeteer.launch({ headless: true });
    var item = [];
    try {
        var page = await browser.newPage();
        await page.setViewport({
            width: 1000,
            height: 1080
        });
        await page.goto('https://app.keepspace.com.au/users/login', { timeout: 0 });
        await page.type('#email', emailAddress, { delay: 10 });
        await page.type('#password', passWord, { delay: 10 });
        const button = await page.$('button[type="submit"]');
        await button.click();
        await new Promise(function (resolve) { setTimeout(resolve, 1500); });
        console.log(new Date() + ": Login Successfull");
        await page.goto('https://platform.keepspace.com.au/user/orgtcsi6xzvya6sl/space/products', { timeout: 0 });
        await new Promise(function (resolve) { setTimeout(resolve, 1500); });
        const path = await download(page, async function () {
            //await page.click('#root > div > div.jss1 > div > main > div > div.jss42 > div.MuiToolbar-root.MuiToolbar-regular.MuiToolbar-gutters > button');
            await page.click('main div.MuiToolbar-root.MuiToolbar-regular.MuiToolbar-gutters > button');
        });
        const { size } = await util.promisify(fs.stat)(path.filePathd);
        console.log(path.filePathd, `${size}B`);
        await new Promise(function (resolve) { setTimeout(resolve, 10000); });
        console.log(path.filePathd, `${size}B`);
        File_Name = path.filePathd;
        await main2(File_Name, path.downloadPath);
        await browser.close();
    } catch (e) {
        console.log(e);
    }
})();

// function 
async function main2(File_Name, downloadPath) {
    var array = [];
    fs.createReadStream(File_Name)
        .pipe(csv())
        .on('data', (row) => {
            array.push(row);
        })
        .on('end', async function () {
            console.log('CSV file successfully processed');
            var len = array.length;
            console.log(new Date() + ": " + len + " Skus are found");
            var now = new Date();
            var rows = array.map(function (row) {
                return {
                    SKU: row.sku,
                    Available_qty: row.quantity_available,
                    Timestamp: new Date().getTime() / 1000,
                    Warehouse: "AU-2",
                    Date: now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate()
                };
            });
            await sendToBigQuery(rows);
            fs.remove(downloadPath);
        });
}

// set up, invoke the function, wait for the download to complete
async function download(page, f) {
    const downloadPath = path.resolve(
        process.cwd(),
        `download-${Math.random()
            .toString(36)
            .substr(2, 8)}`,
    );
    await util.promisify(fs.mkdir)(downloadPath);
    console.error('Download directory:', downloadPath);
    await page._client.send('Page.setDownloadBehavior', {
        behavior: 'allow',
        downloadPath: downloadPath,
    });
    await f();
    console.error('Downloading...');
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Keepspace" });
    let fileName;
    while (!fileName || fileName.endsWith('.crdownload')) {
        await f();
        await new Promise(resolve => setTimeout(resolve, 10000));
        [fileName] = await util.promisify(fs.readdir)(downloadPath);
    }

    const filePath = path.resolve(downloadPath, fileName);
    console.log('Downloaded file:', filePath);
    return {
        filePathd: filePath,
        downloadPath: downloadPath
    };
}

async function sendToBigQuery(rows) {
    const bigquery = new BigQuery();
    var datasetId = "master";
    var tableId = "Inventory";

    await bigquery.dataset(datasetId).table(tableId).insert(rows).then(function (data) {
        console.log("\n API Response ", util.inspect(data[0], false, null, true));
        console.log("\nStored to Bigquery Table and updated filfillment status");
    }).catch(function (err) {
        console.log("\n API Error " + err.name);
        console.log("inventory: ", util.inspect(err, false, null, true));
    });
}