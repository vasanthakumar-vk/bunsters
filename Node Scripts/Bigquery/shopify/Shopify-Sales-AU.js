const Shopify = require('shopify-api-node');
const { BigQuery } = require('@google-cloud/bigquery');
const axios = require('axios');
const util = require('util');

const shopify = new Shopify({
    shopName: 'Bunsters',
    apiKey: '',
    password: ''
});

var startDateTime = new Date();
startDateTime.setDate(startDateTime.getDate() - 20);

(async () => {
    let params = { limit: 100, created_at_min: startDateTime.toISOString(), status: "any" };
    var data = [];
    var years = [];
    do {
        const orders = await shopify.order.list(params);

        orders.forEach(async function (order) {
            //console.log(util.inspect(order, false, null, true));
            order.line_items.forEach(async function (singleItems) {
                var dd = new Date(order.created_at);
                var year = dd.getFullYear();
                if (years.indexOf(year) == -1) {
                    console.log(year + " processing");
                    years.push(year);
                }
                var send = {
                    "SKU": singleItems.sku,
                    "Channel": "Shopify AU",
                    "Units": singleItems.quantity,
                    "Currency": singleItems.price_set.presentment_money.currency_code,
                    "Value": singleItems.price,
                    "Timestamp": order.created_at,
                    "Order_ID": order.id,
                    "Order_Status": order.financial_status,
                    "Updated_Timestamp": new Date().toISOString(),
                    "Region": "AU"
                };
                data.push(send);
            });
        });
        params = orders.nextPageParameters;
    } while (params !== undefined);

    await sendToBigQuery(data);
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Shopify AU Sales" });
})().catch(console.error);

async function sendToBigQuery(newVals) {
    console.log(newVals.length + " data found");
    const bigquery = new BigQuery();
    var schema = getSchema();
    var textTable = "";
    var count = 0;
    for (var index = 0; index < newVals.length; index++) {
        var row = newVals[index];
        var tempText = "SELECT\n";
        for (var key in row) {
            if (schema[key] == "NUMERIC" || schema[key] == "FLOAT") {
                tempText += ((row[key] && Number(row[key]).toString() != "NaN") ? Number(row[key]) : "NULL") + ' ' + key + ",";
            } else if (schema[key] == "TIMESTAMP") {
                tempText += ' TIMESTAMP("' + ((row[key]) ? row[key] : "TIMESTAMP(NULL)") + '") ' + key + ",";
            } else {
                tempText += ' ' + ((row[key]) ? ('"' + row[key] + '"') : "STRING(NULL)") + ' ' + key + ",";
            }
        }
        textTable += tempText.slice(0, tempText.length - 1) + "\n UNION ALL \n";
        count++;
        if (count >= 300) {
            count = 0;
            await runQuery(textTable, bigquery);
            textTable = "";
        }
    }
    if (count < 300 && textTable != "") {
        await runQuery(textTable, bigquery);
    }
    console.log("Bigquery data uploaded");
}

async function runQuery(textTable, bigquery) {
    var mainQuery = "MERGE\n`bunsters.master.sales` T\nUSING\n (" + textTable.slice(0, textTable.length - 11) + ") S "
        + "ON\n  T.Order_ID = S.Order_ID  AND T.SKU = S.SKU\nWHEN MATCHED\nTHEN UPDATE SET SKU = S.SKU, Channel = S.Channel, Units = S.Units, Currency = S.Currency, Value = S.Value, Timestamp = S.Timestamp, Order_ID = S.Order_ID, Order_Status = S.Order_Status, Updated_Timestamp = S.Updated_Timestamp, Region = S.Region"
        + " WHEN NOT MATCHED\nTHEN\nINSERT (SKU, Channel, Units, Currency, Value, Timestamp, Order_ID, Order_Status, Updated_Timestamp, Region)"
        + " VALUES\n(S.SKU, S.Channel, S.Units, S.Currency, S.Value, S.Timestamp, S.Order_ID, S.Order_Status, S.Updated_Timestamp, S.Region)";
    const options = {
        query: mainQuery,
        location: 'US',
    };
    const [job] = await bigquery.createQueryJob(options);
    try {
        console.log(`Job ${job} started.`);
        await job.promise();
        console.log(`Job ${job.id} Done.`);
    } catch (e) {
        console.log(e);
    }
}

function getSchema() {
    return {
        "SKU": "STRING",
        "Channel": "STRING",
        "Units": "NUMERIC",
        "Currency": "STRING",
        "Value": "FLOAT",
        "Timestamp": "TIMESTAMP",
        "Order_ID": "STRING",
        "Order_Status": "STRING",
        "Updated_Timestamp": "TIMESTAMP",
        "Region": "STRING"
    };
}