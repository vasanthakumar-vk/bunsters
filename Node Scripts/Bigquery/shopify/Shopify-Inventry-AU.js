const Shopify = require('shopify-api-node');
const { BigQuery } = require('@google-cloud/bigquery');
const axios = require('axios');
const util = require('util');
const shopify = new Shopify({
    shopName: 'Bunsters',
    apiKey: '',
    password: ''
});

(async () => {
    let params = { limit: 10 };
    var data = [];
    do {
        const products = await shopify.product.list(params);
        products.forEach(function (items) {
            var now = new Date();
            items["variants"].forEach(async function (pro) {
                var send = {
                    SKU: pro.sku,
                    Available_qty: pro.inventory_quantity,
                    Timestamp: now.getTime() / 1000,
                    Warehouse: "Shopify AU",
                    Date: now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate()
                };
                data.push(send)
            });
        });
        params = products.nextPageParameters;
    } while (params !== undefined);
    await sendToBigQuery(data);
    await axios.post("https://script.google.com/macros/s/AKfycbwI-57aYdowuC_ZbcozqTGQOfcx-VlM3efNukV7Tt6MD9BL1XM/exec", { data: "Shopify AU Inventory" });
})().catch(console.error);

async function sendToBigQuery(rows) {
    const bigquery = new BigQuery();
    var datasetId = "master";
    var tableId = "Inventory";
    await bigquery.dataset(datasetId).table(tableId).insert(rows).then(function (data) {
        console.log("\n API Response ", util.inspect(data[0], false, null, true));
        console.log("\nStored to Bigquery Table and updated filfillment status");
    }).catch(function (err) {
        console.log("\n API Error " + err.name);
        console.log("inventory: ", util.inspect(err, false, null, true));
    });
}